/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { ReligiousInstituteDetailComponent } from 'app/entities/religious-institute/religious-institute-detail.component';
import { ReligiousInstitute } from 'app/shared/model/religious-institute.model';

describe('Component Tests', () => {
  describe('ReligiousInstitute Management Detail Component', () => {
    let comp: ReligiousInstituteDetailComponent;
    let fixture: ComponentFixture<ReligiousInstituteDetailComponent>;
    const route = ({ data: of({ religiousInstitute: new ReligiousInstitute(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [ReligiousInstituteDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ReligiousInstituteDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ReligiousInstituteDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.religiousInstitute).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
