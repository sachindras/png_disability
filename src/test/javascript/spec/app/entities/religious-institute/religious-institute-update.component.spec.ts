/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { ReligiousInstituteUpdateComponent } from 'app/entities/religious-institute/religious-institute-update.component';
import { ReligiousInstituteService } from 'app/entities/religious-institute/religious-institute.service';
import { ReligiousInstitute } from 'app/shared/model/religious-institute.model';

describe('Component Tests', () => {
  describe('ReligiousInstitute Management Update Component', () => {
    let comp: ReligiousInstituteUpdateComponent;
    let fixture: ComponentFixture<ReligiousInstituteUpdateComponent>;
    let service: ReligiousInstituteService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [ReligiousInstituteUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ReligiousInstituteUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ReligiousInstituteUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ReligiousInstituteService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ReligiousInstitute(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ReligiousInstitute();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
