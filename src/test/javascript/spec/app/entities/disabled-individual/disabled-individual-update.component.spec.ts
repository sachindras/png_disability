/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { DisabledIndividualUpdateComponent } from 'app/entities/disabled-individual/disabled-individual-update.component';
import { DisabledIndividualService } from 'app/entities/disabled-individual/disabled-individual.service';
import { DisabledIndividual } from 'app/shared/model/disabled-individual.model';

describe('Component Tests', () => {
  describe('DisabledIndividual Management Update Component', () => {
    let comp: DisabledIndividualUpdateComponent;
    let fixture: ComponentFixture<DisabledIndividualUpdateComponent>;
    let service: DisabledIndividualService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [DisabledIndividualUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DisabledIndividualUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DisabledIndividualUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DisabledIndividualService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DisabledIndividual(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DisabledIndividual();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
