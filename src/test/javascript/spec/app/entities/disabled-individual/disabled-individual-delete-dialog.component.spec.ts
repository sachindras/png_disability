/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { DisabledIndividualDeleteDialogComponent } from 'app/entities/disabled-individual/disabled-individual-delete-dialog.component';
import { DisabledIndividualService } from 'app/entities/disabled-individual/disabled-individual.service';

describe('Component Tests', () => {
  describe('DisabledIndividual Management Delete Component', () => {
    let comp: DisabledIndividualDeleteDialogComponent;
    let fixture: ComponentFixture<DisabledIndividualDeleteDialogComponent>;
    let service: DisabledIndividualService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [DisabledIndividualDeleteDialogComponent]
      })
        .overrideTemplate(DisabledIndividualDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DisabledIndividualDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DisabledIndividualService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
