/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { DisabledIndividualService } from 'app/entities/disabled-individual/disabled-individual.service';
import {
  IDisabledIndividual,
  DisabledIndividual,
  Gender,
  EstimateAge,
  MartialStatus,
  CarerRelationship,
  Difficulty,
  CauseOfDisability,
  ServiceAvailibility,
  ServiceAccessibility,
  Livelihood,
  Talents
} from 'app/shared/model/disabled-individual.model';

describe('Service Tests', () => {
  describe('DisabledIndividual Service', () => {
    let injector: TestBed;
    let service: DisabledIndividualService;
    let httpMock: HttpTestingController;
    let elemDefault: IDisabledIndividual;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(DisabledIndividualService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new DisabledIndividual(
        0,
        'AAAAAAA',
        'AAAAAAA',
        Gender.MALE,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        EstimateAge.LESS_THAN_18,
        MartialStatus.MARRIED,
        0,
        0,
        0,
        'AAAAAAA',
        'AAAAAAA',
        CarerRelationship.FAMILY_MEMBER,
        Difficulty.NO,
        Difficulty.NO,
        Difficulty.NO,
        Difficulty.NO,
        Difficulty.NO,
        Difficulty.NO,
        CauseOfDisability.DISEASE,
        ServiceAvailibility.YES_AVAILABLE,
        ServiceAccessibility.YES_ACCESSIBLE,
        ServiceAvailibility.YES_AVAILABLE,
        ServiceAccessibility.YES_ACCESSIBLE,
        ServiceAvailibility.YES_AVAILABLE,
        ServiceAccessibility.YES_ACCESSIBLE,
        ServiceAvailibility.YES_AVAILABLE,
        ServiceAccessibility.YES_ACCESSIBLE,
        ServiceAvailibility.YES_AVAILABLE,
        ServiceAccessibility.YES_ACCESSIBLE,
        ServiceAvailibility.YES_AVAILABLE,
        ServiceAccessibility.YES_ACCESSIBLE,
        ServiceAvailibility.YES_AVAILABLE,
        ServiceAccessibility.YES_ACCESSIBLE,
        ServiceAvailibility.YES_AVAILABLE,
        ServiceAccessibility.YES_ACCESSIBLE,
        ServiceAvailibility.YES_AVAILABLE,
        ServiceAccessibility.YES_ACCESSIBLE,
        ServiceAvailibility.YES_AVAILABLE,
        ServiceAccessibility.YES_ACCESSIBLE,
        false,
        'AAAAAAA',
        Livelihood.EMPLOYED_FORM,
        'AAAAAAA',
        Talents.LEADERSHIP
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            dateOfBirth: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a DisabledIndividual', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateOfBirth: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateOfBirth: currentDate
          },
          returnedFromService
        );
        service
          .create(new DisabledIndividual(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a DisabledIndividual', async () => {
        const returnedFromService = Object.assign(
          {
            civilRegistrationNo: 'BBBBBB',
            name: 'BBBBBB',
            gender: 'BBBBBB',
            religion: 'BBBBBB',
            occupation: 'BBBBBB',
            employer: 'BBBBBB',
            dateOfBirth: currentDate.format(DATE_TIME_FORMAT),
            estimatedAge: 'BBBBBB',
            martialStatus: 'BBBBBB',
            noOfChildrenAlive: 1,
            noOfChildrenDeceased: 1,
            noOfChildrenCongenital: 1,
            educationFormal: 'BBBBBB',
            educationTrade: 'BBBBBB',
            carerRelationship: 'BBBBBB',
            diffcultySeeing: 'BBBBBB',
            diffcultyHearing: 'BBBBBB',
            diffcultyWalking: 'BBBBBB',
            diffcultyMemory: 'BBBBBB',
            diffcultySelfCare: 'BBBBBB',
            diffcultyCommunicating: 'BBBBBB',
            causeOfDisability: 'BBBBBB',
            healthServiceAvailibility: 'BBBBBB',
            healthServiceAccessibility: 'BBBBBB',
            educationServiceAvailibility: 'BBBBBB',
            educationServiceAccessibility: 'BBBBBB',
            lawServiceAvailibility: 'BBBBBB',
            lawServiceAccessibility: 'BBBBBB',
            welfareServiceAvailibility: 'BBBBBB',
            welfareServiceAccessibility: 'BBBBBB',
            sportsServiceAvailibility: 'BBBBBB',
            sportsServiceAccessibility: 'BBBBBB',
            roadServiceAvailibility: 'BBBBBB',
            roadServiceAccessibility: 'BBBBBB',
            marketServiceAvailibility: 'BBBBBB',
            marketServiceAccessibility: 'BBBBBB',
            bankingServiceAvailibility: 'BBBBBB',
            bankingServiceAccessibility: 'BBBBBB',
            teleCommunicationServiceAvailibility: 'BBBBBB',
            teleCommunicationServiceAccessibility: 'BBBBBB',
            churchServiceAvailibility: 'BBBBBB',
            churchServiceAccessibility: 'BBBBBB',
            memberDisabledPersonsOrganisation: true,
            awarenessAdvocacyProgrammeDetails: 'BBBBBB',
            livelihood: 'BBBBBB',
            livelihoodDetails: 'BBBBBB',
            talents: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateOfBirth: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of DisabledIndividual', async () => {
        const returnedFromService = Object.assign(
          {
            civilRegistrationNo: 'BBBBBB',
            name: 'BBBBBB',
            gender: 'BBBBBB',
            religion: 'BBBBBB',
            occupation: 'BBBBBB',
            employer: 'BBBBBB',
            dateOfBirth: currentDate.format(DATE_TIME_FORMAT),
            estimatedAge: 'BBBBBB',
            martialStatus: 'BBBBBB',
            noOfChildrenAlive: 1,
            noOfChildrenDeceased: 1,
            noOfChildrenCongenital: 1,
            educationFormal: 'BBBBBB',
            educationTrade: 'BBBBBB',
            carerRelationship: 'BBBBBB',
            diffcultySeeing: 'BBBBBB',
            diffcultyHearing: 'BBBBBB',
            diffcultyWalking: 'BBBBBB',
            diffcultyMemory: 'BBBBBB',
            diffcultySelfCare: 'BBBBBB',
            diffcultyCommunicating: 'BBBBBB',
            causeOfDisability: 'BBBBBB',
            healthServiceAvailibility: 'BBBBBB',
            healthServiceAccessibility: 'BBBBBB',
            educationServiceAvailibility: 'BBBBBB',
            educationServiceAccessibility: 'BBBBBB',
            lawServiceAvailibility: 'BBBBBB',
            lawServiceAccessibility: 'BBBBBB',
            welfareServiceAvailibility: 'BBBBBB',
            welfareServiceAccessibility: 'BBBBBB',
            sportsServiceAvailibility: 'BBBBBB',
            sportsServiceAccessibility: 'BBBBBB',
            roadServiceAvailibility: 'BBBBBB',
            roadServiceAccessibility: 'BBBBBB',
            marketServiceAvailibility: 'BBBBBB',
            marketServiceAccessibility: 'BBBBBB',
            bankingServiceAvailibility: 'BBBBBB',
            bankingServiceAccessibility: 'BBBBBB',
            teleCommunicationServiceAvailibility: 'BBBBBB',
            teleCommunicationServiceAccessibility: 'BBBBBB',
            churchServiceAvailibility: 'BBBBBB',
            churchServiceAccessibility: 'BBBBBB',
            memberDisabledPersonsOrganisation: true,
            awarenessAdvocacyProgrammeDetails: 'BBBBBB',
            livelihood: 'BBBBBB',
            livelihoodDetails: 'BBBBBB',
            talents: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateOfBirth: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DisabledIndividual', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
