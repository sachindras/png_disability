/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { DisabledIndividualDetailComponent } from 'app/entities/disabled-individual/disabled-individual-detail.component';
import { DisabledIndividual } from 'app/shared/model/disabled-individual.model';

describe('Component Tests', () => {
  describe('DisabledIndividual Management Detail Component', () => {
    let comp: DisabledIndividualDetailComponent;
    let fixture: ComponentFixture<DisabledIndividualDetailComponent>;
    const route = ({ data: of({ disabledIndividual: new DisabledIndividual(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [DisabledIndividualDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DisabledIndividualDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DisabledIndividualDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.disabledIndividual).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
