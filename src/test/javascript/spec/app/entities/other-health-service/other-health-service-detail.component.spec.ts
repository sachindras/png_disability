/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { OtherHealthServiceDetailComponent } from 'app/entities/other-health-service/other-health-service-detail.component';
import { OtherHealthService } from 'app/shared/model/other-health-service.model';

describe('Component Tests', () => {
  describe('OtherHealthService Management Detail Component', () => {
    let comp: OtherHealthServiceDetailComponent;
    let fixture: ComponentFixture<OtherHealthServiceDetailComponent>;
    const route = ({ data: of({ otherHealthService: new OtherHealthService(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [OtherHealthServiceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OtherHealthServiceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OtherHealthServiceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.otherHealthService).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
