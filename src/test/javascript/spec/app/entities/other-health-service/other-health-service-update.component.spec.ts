/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { OtherHealthServiceUpdateComponent } from 'app/entities/other-health-service/other-health-service-update.component';
import { OtherHealthServiceService } from 'app/entities/other-health-service/other-health-service.service';
import { OtherHealthService } from 'app/shared/model/other-health-service.model';

describe('Component Tests', () => {
  describe('OtherHealthService Management Update Component', () => {
    let comp: OtherHealthServiceUpdateComponent;
    let fixture: ComponentFixture<OtherHealthServiceUpdateComponent>;
    let service: OtherHealthServiceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [OtherHealthServiceUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OtherHealthServiceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OtherHealthServiceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OtherHealthServiceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OtherHealthService(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OtherHealthService();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
