/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { OtherHealthServiceDeleteDialogComponent } from 'app/entities/other-health-service/other-health-service-delete-dialog.component';
import { OtherHealthServiceService } from 'app/entities/other-health-service/other-health-service.service';

describe('Component Tests', () => {
  describe('OtherHealthService Management Delete Component', () => {
    let comp: OtherHealthServiceDeleteDialogComponent;
    let fixture: ComponentFixture<OtherHealthServiceDeleteDialogComponent>;
    let service: OtherHealthServiceService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [OtherHealthServiceDeleteDialogComponent]
      })
        .overrideTemplate(OtherHealthServiceDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OtherHealthServiceDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OtherHealthServiceService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
