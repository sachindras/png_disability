/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { PrimarySchoolUpdateComponent } from 'app/entities/primary-school/primary-school-update.component';
import { PrimarySchoolService } from 'app/entities/primary-school/primary-school.service';
import { PrimarySchool } from 'app/shared/model/primary-school.model';

describe('Component Tests', () => {
  describe('PrimarySchool Management Update Component', () => {
    let comp: PrimarySchoolUpdateComponent;
    let fixture: ComponentFixture<PrimarySchoolUpdateComponent>;
    let service: PrimarySchoolService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [PrimarySchoolUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PrimarySchoolUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PrimarySchoolUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PrimarySchoolService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PrimarySchool(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PrimarySchool();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
