/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { PrimarySchoolDetailComponent } from 'app/entities/primary-school/primary-school-detail.component';
import { PrimarySchool } from 'app/shared/model/primary-school.model';

describe('Component Tests', () => {
  describe('PrimarySchool Management Detail Component', () => {
    let comp: PrimarySchoolDetailComponent;
    let fixture: ComponentFixture<PrimarySchoolDetailComponent>;
    const route = ({ data: of({ primarySchool: new PrimarySchool(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [PrimarySchoolDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PrimarySchoolDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PrimarySchoolDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.primarySchool).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
