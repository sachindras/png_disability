/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { PrimarySchoolDeleteDialogComponent } from 'app/entities/primary-school/primary-school-delete-dialog.component';
import { PrimarySchoolService } from 'app/entities/primary-school/primary-school.service';

describe('Component Tests', () => {
  describe('PrimarySchool Management Delete Component', () => {
    let comp: PrimarySchoolDeleteDialogComponent;
    let fixture: ComponentFixture<PrimarySchoolDeleteDialogComponent>;
    let service: PrimarySchoolService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [PrimarySchoolDeleteDialogComponent]
      })
        .overrideTemplate(PrimarySchoolDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PrimarySchoolDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PrimarySchoolService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
