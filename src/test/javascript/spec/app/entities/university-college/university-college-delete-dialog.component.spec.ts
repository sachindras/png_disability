/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { UniversityCollegeDeleteDialogComponent } from 'app/entities/university-college/university-college-delete-dialog.component';
import { UniversityCollegeService } from 'app/entities/university-college/university-college.service';

describe('Component Tests', () => {
  describe('UniversityCollege Management Delete Component', () => {
    let comp: UniversityCollegeDeleteDialogComponent;
    let fixture: ComponentFixture<UniversityCollegeDeleteDialogComponent>;
    let service: UniversityCollegeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [UniversityCollegeDeleteDialogComponent]
      })
        .overrideTemplate(UniversityCollegeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UniversityCollegeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UniversityCollegeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
