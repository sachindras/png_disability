/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { UniversityCollegeUpdateComponent } from 'app/entities/university-college/university-college-update.component';
import { UniversityCollegeService } from 'app/entities/university-college/university-college.service';
import { UniversityCollege } from 'app/shared/model/university-college.model';

describe('Component Tests', () => {
  describe('UniversityCollege Management Update Component', () => {
    let comp: UniversityCollegeUpdateComponent;
    let fixture: ComponentFixture<UniversityCollegeUpdateComponent>;
    let service: UniversityCollegeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [UniversityCollegeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UniversityCollegeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UniversityCollegeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UniversityCollegeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UniversityCollege(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UniversityCollege();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
