/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { UniversityCollegeDetailComponent } from 'app/entities/university-college/university-college-detail.component';
import { UniversityCollege } from 'app/shared/model/university-college.model';

describe('Component Tests', () => {
  describe('UniversityCollege Management Detail Component', () => {
    let comp: UniversityCollegeDetailComponent;
    let fixture: ComponentFixture<UniversityCollegeDetailComponent>;
    const route = ({ data: of({ universityCollege: new UniversityCollege(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [UniversityCollegeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UniversityCollegeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UniversityCollegeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.universityCollege).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
