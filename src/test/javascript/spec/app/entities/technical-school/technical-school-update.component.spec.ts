/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { TechnicalSchoolUpdateComponent } from 'app/entities/technical-school/technical-school-update.component';
import { TechnicalSchoolService } from 'app/entities/technical-school/technical-school.service';
import { TechnicalSchool } from 'app/shared/model/technical-school.model';

describe('Component Tests', () => {
  describe('TechnicalSchool Management Update Component', () => {
    let comp: TechnicalSchoolUpdateComponent;
    let fixture: ComponentFixture<TechnicalSchoolUpdateComponent>;
    let service: TechnicalSchoolService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [TechnicalSchoolUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TechnicalSchoolUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TechnicalSchoolUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TechnicalSchoolService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TechnicalSchool(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TechnicalSchool();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
