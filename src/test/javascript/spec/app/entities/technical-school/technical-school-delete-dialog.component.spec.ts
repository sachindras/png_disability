/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { TechnicalSchoolDeleteDialogComponent } from 'app/entities/technical-school/technical-school-delete-dialog.component';
import { TechnicalSchoolService } from 'app/entities/technical-school/technical-school.service';

describe('Component Tests', () => {
  describe('TechnicalSchool Management Delete Component', () => {
    let comp: TechnicalSchoolDeleteDialogComponent;
    let fixture: ComponentFixture<TechnicalSchoolDeleteDialogComponent>;
    let service: TechnicalSchoolService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [TechnicalSchoolDeleteDialogComponent]
      })
        .overrideTemplate(TechnicalSchoolDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TechnicalSchoolDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TechnicalSchoolService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
