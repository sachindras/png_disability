/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { TechnicalSchoolDetailComponent } from 'app/entities/technical-school/technical-school-detail.component';
import { TechnicalSchool } from 'app/shared/model/technical-school.model';

describe('Component Tests', () => {
  describe('TechnicalSchool Management Detail Component', () => {
    let comp: TechnicalSchoolDetailComponent;
    let fixture: ComponentFixture<TechnicalSchoolDetailComponent>;
    const route = ({ data: of({ technicalSchool: new TechnicalSchool(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [TechnicalSchoolDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TechnicalSchoolDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TechnicalSchoolDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.technicalSchool).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
