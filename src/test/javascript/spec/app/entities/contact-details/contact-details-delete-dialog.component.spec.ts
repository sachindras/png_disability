/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { ContactDetailsDeleteDialogComponent } from 'app/entities/contact-details/contact-details-delete-dialog.component';
import { ContactDetailsService } from 'app/entities/contact-details/contact-details.service';

describe('Component Tests', () => {
  describe('ContactDetails Management Delete Component', () => {
    let comp: ContactDetailsDeleteDialogComponent;
    let fixture: ComponentFixture<ContactDetailsDeleteDialogComponent>;
    let service: ContactDetailsService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [ContactDetailsDeleteDialogComponent]
      })
        .overrideTemplate(ContactDetailsDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ContactDetailsDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContactDetailsService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
