/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { SecondarySchoolDeleteDialogComponent } from 'app/entities/secondary-school/secondary-school-delete-dialog.component';
import { SecondarySchoolService } from 'app/entities/secondary-school/secondary-school.service';

describe('Component Tests', () => {
  describe('SecondarySchool Management Delete Component', () => {
    let comp: SecondarySchoolDeleteDialogComponent;
    let fixture: ComponentFixture<SecondarySchoolDeleteDialogComponent>;
    let service: SecondarySchoolService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [SecondarySchoolDeleteDialogComponent]
      })
        .overrideTemplate(SecondarySchoolDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SecondarySchoolDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SecondarySchoolService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
