/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { SecondarySchoolDetailComponent } from 'app/entities/secondary-school/secondary-school-detail.component';
import { SecondarySchool } from 'app/shared/model/secondary-school.model';

describe('Component Tests', () => {
  describe('SecondarySchool Management Detail Component', () => {
    let comp: SecondarySchoolDetailComponent;
    let fixture: ComponentFixture<SecondarySchoolDetailComponent>;
    const route = ({ data: of({ secondarySchool: new SecondarySchool(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [SecondarySchoolDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SecondarySchoolDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SecondarySchoolDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.secondarySchool).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
