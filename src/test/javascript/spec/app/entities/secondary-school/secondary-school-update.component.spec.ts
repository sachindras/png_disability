/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { SecondarySchoolUpdateComponent } from 'app/entities/secondary-school/secondary-school-update.component';
import { SecondarySchoolService } from 'app/entities/secondary-school/secondary-school.service';
import { SecondarySchool } from 'app/shared/model/secondary-school.model';

describe('Component Tests', () => {
  describe('SecondarySchool Management Update Component', () => {
    let comp: SecondarySchoolUpdateComponent;
    let fixture: ComponentFixture<SecondarySchoolUpdateComponent>;
    let service: SecondarySchoolService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [SecondarySchoolUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SecondarySchoolUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SecondarySchoolUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SecondarySchoolService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SecondarySchool(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SecondarySchool();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
