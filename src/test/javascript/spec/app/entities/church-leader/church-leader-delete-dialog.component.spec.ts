/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { ChurchLeaderDeleteDialogComponent } from 'app/entities/church-leader/church-leader-delete-dialog.component';
import { ChurchLeaderService } from 'app/entities/church-leader/church-leader.service';

describe('Component Tests', () => {
  describe('ChurchLeader Management Delete Component', () => {
    let comp: ChurchLeaderDeleteDialogComponent;
    let fixture: ComponentFixture<ChurchLeaderDeleteDialogComponent>;
    let service: ChurchLeaderService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [ChurchLeaderDeleteDialogComponent]
      })
        .overrideTemplate(ChurchLeaderDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ChurchLeaderDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ChurchLeaderService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
