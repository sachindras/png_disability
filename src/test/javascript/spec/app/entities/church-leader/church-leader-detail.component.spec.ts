/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { ChurchLeaderDetailComponent } from 'app/entities/church-leader/church-leader-detail.component';
import { ChurchLeader } from 'app/shared/model/church-leader.model';

describe('Component Tests', () => {
  describe('ChurchLeader Management Detail Component', () => {
    let comp: ChurchLeaderDetailComponent;
    let fixture: ComponentFixture<ChurchLeaderDetailComponent>;
    const route = ({ data: of({ churchLeader: new ChurchLeader(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [ChurchLeaderDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ChurchLeaderDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ChurchLeaderDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.churchLeader).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
