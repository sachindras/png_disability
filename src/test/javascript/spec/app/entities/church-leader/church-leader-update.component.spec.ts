/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { ChurchLeaderUpdateComponent } from 'app/entities/church-leader/church-leader-update.component';
import { ChurchLeaderService } from 'app/entities/church-leader/church-leader.service';
import { ChurchLeader } from 'app/shared/model/church-leader.model';

describe('Component Tests', () => {
  describe('ChurchLeader Management Update Component', () => {
    let comp: ChurchLeaderUpdateComponent;
    let fixture: ComponentFixture<ChurchLeaderUpdateComponent>;
    let service: ChurchLeaderService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [ChurchLeaderUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ChurchLeaderUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ChurchLeaderUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ChurchLeaderService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ChurchLeader(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ChurchLeader();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
