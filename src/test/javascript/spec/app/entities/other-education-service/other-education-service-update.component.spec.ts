/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { OtherEducationServiceUpdateComponent } from 'app/entities/other-education-service/other-education-service-update.component';
import { OtherEducationServiceService } from 'app/entities/other-education-service/other-education-service.service';
import { OtherEducationService } from 'app/shared/model/other-education-service.model';

describe('Component Tests', () => {
  describe('OtherEducationService Management Update Component', () => {
    let comp: OtherEducationServiceUpdateComponent;
    let fixture: ComponentFixture<OtherEducationServiceUpdateComponent>;
    let service: OtherEducationServiceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [OtherEducationServiceUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OtherEducationServiceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OtherEducationServiceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OtherEducationServiceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OtherEducationService(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OtherEducationService();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
