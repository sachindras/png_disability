/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { OtherEducationServiceDeleteDialogComponent } from 'app/entities/other-education-service/other-education-service-delete-dialog.component';
import { OtherEducationServiceService } from 'app/entities/other-education-service/other-education-service.service';

describe('Component Tests', () => {
  describe('OtherEducationService Management Delete Component', () => {
    let comp: OtherEducationServiceDeleteDialogComponent;
    let fixture: ComponentFixture<OtherEducationServiceDeleteDialogComponent>;
    let service: OtherEducationServiceService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [OtherEducationServiceDeleteDialogComponent]
      })
        .overrideTemplate(OtherEducationServiceDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OtherEducationServiceDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OtherEducationServiceService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
