/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PngDisabilityReligionTestModule } from '../../../test.module';
import { OtherEducationServiceDetailComponent } from 'app/entities/other-education-service/other-education-service-detail.component';
import { OtherEducationService } from 'app/shared/model/other-education-service.model';

describe('Component Tests', () => {
  describe('OtherEducationService Management Detail Component', () => {
    let comp: OtherEducationServiceDetailComponent;
    let fixture: ComponentFixture<OtherEducationServiceDetailComponent>;
    const route = ({ data: of({ otherEducationService: new OtherEducationService(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PngDisabilityReligionTestModule],
        declarations: [OtherEducationServiceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OtherEducationServiceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OtherEducationServiceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.otherEducationService).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
