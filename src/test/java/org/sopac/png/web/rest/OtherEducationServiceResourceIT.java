package org.sopac.png.web.rest;

import org.sopac.png.PngDisabilityReligionApp;
import org.sopac.png.domain.OtherEducationService;
import org.sopac.png.repository.OtherEducationServiceRepository;
import org.sopac.png.repository.search.OtherEducationServiceSearchRepository;
import org.sopac.png.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.sopac.png.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link OtherEducationServiceResource} REST controller.
 */
@SpringBootTest(classes = PngDisabilityReligionApp.class)
public class OtherEducationServiceResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCE = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCE = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_WARD = "AAAAAAAAAA";
    private static final String UPDATED_WARD = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private OtherEducationServiceRepository otherEducationServiceRepository;

    /**
     * This repository is mocked in the org.sopac.png.repository.search test package.
     *
     * @see org.sopac.png.repository.search.OtherEducationServiceSearchRepositoryMockConfiguration
     */
    @Autowired
    private OtherEducationServiceSearchRepository mockOtherEducationServiceSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOtherEducationServiceMockMvc;

    private OtherEducationService otherEducationService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OtherEducationServiceResource otherEducationServiceResource = new OtherEducationServiceResource(otherEducationServiceRepository, mockOtherEducationServiceSearchRepository);
        this.restOtherEducationServiceMockMvc = MockMvcBuilders.standaloneSetup(otherEducationServiceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherEducationService createEntity(EntityManager em) {
        OtherEducationService otherEducationService = new OtherEducationService()
            .name(DEFAULT_NAME)
            .province(DEFAULT_PROVINCE)
            .district(DEFAULT_DISTRICT)
            .ward(DEFAULT_WARD)
            .active(DEFAULT_ACTIVE);
        return otherEducationService;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherEducationService createUpdatedEntity(EntityManager em) {
        OtherEducationService otherEducationService = new OtherEducationService()
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);
        return otherEducationService;
    }

    @BeforeEach
    public void initTest() {
        otherEducationService = createEntity(em);
    }

    @Test
    @Transactional
    public void createOtherEducationService() throws Exception {
        int databaseSizeBeforeCreate = otherEducationServiceRepository.findAll().size();

        // Create the OtherEducationService
        restOtherEducationServiceMockMvc.perform(post("/api/other-education-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otherEducationService)))
            .andExpect(status().isCreated());

        // Validate the OtherEducationService in the database
        List<OtherEducationService> otherEducationServiceList = otherEducationServiceRepository.findAll();
        assertThat(otherEducationServiceList).hasSize(databaseSizeBeforeCreate + 1);
        OtherEducationService testOtherEducationService = otherEducationServiceList.get(otherEducationServiceList.size() - 1);
        assertThat(testOtherEducationService.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testOtherEducationService.getProvince()).isEqualTo(DEFAULT_PROVINCE);
        assertThat(testOtherEducationService.getDistrict()).isEqualTo(DEFAULT_DISTRICT);
        assertThat(testOtherEducationService.getWard()).isEqualTo(DEFAULT_WARD);
        assertThat(testOtherEducationService.isActive()).isEqualTo(DEFAULT_ACTIVE);

        // Validate the OtherEducationService in Elasticsearch
        verify(mockOtherEducationServiceSearchRepository, times(1)).save(testOtherEducationService);
    }

    @Test
    @Transactional
    public void createOtherEducationServiceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = otherEducationServiceRepository.findAll().size();

        // Create the OtherEducationService with an existing ID
        otherEducationService.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOtherEducationServiceMockMvc.perform(post("/api/other-education-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otherEducationService)))
            .andExpect(status().isBadRequest());

        // Validate the OtherEducationService in the database
        List<OtherEducationService> otherEducationServiceList = otherEducationServiceRepository.findAll();
        assertThat(otherEducationServiceList).hasSize(databaseSizeBeforeCreate);

        // Validate the OtherEducationService in Elasticsearch
        verify(mockOtherEducationServiceSearchRepository, times(0)).save(otherEducationService);
    }


    @Test
    @Transactional
    public void getAllOtherEducationServices() throws Exception {
        // Initialize the database
        otherEducationServiceRepository.saveAndFlush(otherEducationService);

        // Get all the otherEducationServiceList
        restOtherEducationServiceMockMvc.perform(get("/api/other-education-services?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otherEducationService.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getOtherEducationService() throws Exception {
        // Initialize the database
        otherEducationServiceRepository.saveAndFlush(otherEducationService);

        // Get the otherEducationService
        restOtherEducationServiceMockMvc.perform(get("/api/other-education-services/{id}", otherEducationService.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(otherEducationService.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.province").value(DEFAULT_PROVINCE.toString()))
            .andExpect(jsonPath("$.district").value(DEFAULT_DISTRICT.toString()))
            .andExpect(jsonPath("$.ward").value(DEFAULT_WARD.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingOtherEducationService() throws Exception {
        // Get the otherEducationService
        restOtherEducationServiceMockMvc.perform(get("/api/other-education-services/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOtherEducationService() throws Exception {
        // Initialize the database
        otherEducationServiceRepository.saveAndFlush(otherEducationService);

        int databaseSizeBeforeUpdate = otherEducationServiceRepository.findAll().size();

        // Update the otherEducationService
        OtherEducationService updatedOtherEducationService = otherEducationServiceRepository.findById(otherEducationService.getId()).get();
        // Disconnect from session so that the updates on updatedOtherEducationService are not directly saved in db
        em.detach(updatedOtherEducationService);
        updatedOtherEducationService
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);

        restOtherEducationServiceMockMvc.perform(put("/api/other-education-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOtherEducationService)))
            .andExpect(status().isOk());

        // Validate the OtherEducationService in the database
        List<OtherEducationService> otherEducationServiceList = otherEducationServiceRepository.findAll();
        assertThat(otherEducationServiceList).hasSize(databaseSizeBeforeUpdate);
        OtherEducationService testOtherEducationService = otherEducationServiceList.get(otherEducationServiceList.size() - 1);
        assertThat(testOtherEducationService.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testOtherEducationService.getProvince()).isEqualTo(UPDATED_PROVINCE);
        assertThat(testOtherEducationService.getDistrict()).isEqualTo(UPDATED_DISTRICT);
        assertThat(testOtherEducationService.getWard()).isEqualTo(UPDATED_WARD);
        assertThat(testOtherEducationService.isActive()).isEqualTo(UPDATED_ACTIVE);

        // Validate the OtherEducationService in Elasticsearch
        verify(mockOtherEducationServiceSearchRepository, times(1)).save(testOtherEducationService);
    }

    @Test
    @Transactional
    public void updateNonExistingOtherEducationService() throws Exception {
        int databaseSizeBeforeUpdate = otherEducationServiceRepository.findAll().size();

        // Create the OtherEducationService

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherEducationServiceMockMvc.perform(put("/api/other-education-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otherEducationService)))
            .andExpect(status().isBadRequest());

        // Validate the OtherEducationService in the database
        List<OtherEducationService> otherEducationServiceList = otherEducationServiceRepository.findAll();
        assertThat(otherEducationServiceList).hasSize(databaseSizeBeforeUpdate);

        // Validate the OtherEducationService in Elasticsearch
        verify(mockOtherEducationServiceSearchRepository, times(0)).save(otherEducationService);
    }

    @Test
    @Transactional
    public void deleteOtherEducationService() throws Exception {
        // Initialize the database
        otherEducationServiceRepository.saveAndFlush(otherEducationService);

        int databaseSizeBeforeDelete = otherEducationServiceRepository.findAll().size();

        // Delete the otherEducationService
        restOtherEducationServiceMockMvc.perform(delete("/api/other-education-services/{id}", otherEducationService.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OtherEducationService> otherEducationServiceList = otherEducationServiceRepository.findAll();
        assertThat(otherEducationServiceList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the OtherEducationService in Elasticsearch
        verify(mockOtherEducationServiceSearchRepository, times(1)).deleteById(otherEducationService.getId());
    }

    @Test
    @Transactional
    public void searchOtherEducationService() throws Exception {
        // Initialize the database
        otherEducationServiceRepository.saveAndFlush(otherEducationService);
        when(mockOtherEducationServiceSearchRepository.search(queryStringQuery("id:" + otherEducationService.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(otherEducationService), PageRequest.of(0, 1), 1));
        // Search the otherEducationService
        restOtherEducationServiceMockMvc.perform(get("/api/_search/other-education-services?query=id:" + otherEducationService.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otherEducationService.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE)))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT)))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherEducationService.class);
        OtherEducationService otherEducationService1 = new OtherEducationService();
        otherEducationService1.setId(1L);
        OtherEducationService otherEducationService2 = new OtherEducationService();
        otherEducationService2.setId(otherEducationService1.getId());
        assertThat(otherEducationService1).isEqualTo(otherEducationService2);
        otherEducationService2.setId(2L);
        assertThat(otherEducationService1).isNotEqualTo(otherEducationService2);
        otherEducationService1.setId(null);
        assertThat(otherEducationService1).isNotEqualTo(otherEducationService2);
    }
}
