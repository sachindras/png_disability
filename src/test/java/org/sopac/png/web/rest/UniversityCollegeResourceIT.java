package org.sopac.png.web.rest;

import org.sopac.png.PngDisabilityReligionApp;
import org.sopac.png.domain.UniversityCollege;
import org.sopac.png.repository.UniversityCollegeRepository;
import org.sopac.png.repository.search.UniversityCollegeSearchRepository;
import org.sopac.png.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.sopac.png.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link UniversityCollegeResource} REST controller.
 */
@SpringBootTest(classes = PngDisabilityReligionApp.class)
public class UniversityCollegeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCE = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCE = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_WARD = "AAAAAAAAAA";
    private static final String UPDATED_WARD = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private UniversityCollegeRepository universityCollegeRepository;

    /**
     * This repository is mocked in the org.sopac.png.repository.search test package.
     *
     * @see org.sopac.png.repository.search.UniversityCollegeSearchRepositoryMockConfiguration
     */
    @Autowired
    private UniversityCollegeSearchRepository mockUniversityCollegeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUniversityCollegeMockMvc;

    private UniversityCollege universityCollege;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UniversityCollegeResource universityCollegeResource = new UniversityCollegeResource(universityCollegeRepository, mockUniversityCollegeSearchRepository);
        this.restUniversityCollegeMockMvc = MockMvcBuilders.standaloneSetup(universityCollegeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UniversityCollege createEntity(EntityManager em) {
        UniversityCollege universityCollege = new UniversityCollege()
            .name(DEFAULT_NAME)
            .province(DEFAULT_PROVINCE)
            .district(DEFAULT_DISTRICT)
            .ward(DEFAULT_WARD)
            .active(DEFAULT_ACTIVE);
        return universityCollege;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UniversityCollege createUpdatedEntity(EntityManager em) {
        UniversityCollege universityCollege = new UniversityCollege()
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);
        return universityCollege;
    }

    @BeforeEach
    public void initTest() {
        universityCollege = createEntity(em);
    }

    @Test
    @Transactional
    public void createUniversityCollege() throws Exception {
        int databaseSizeBeforeCreate = universityCollegeRepository.findAll().size();

        // Create the UniversityCollege
        restUniversityCollegeMockMvc.perform(post("/api/university-colleges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(universityCollege)))
            .andExpect(status().isCreated());

        // Validate the UniversityCollege in the database
        List<UniversityCollege> universityCollegeList = universityCollegeRepository.findAll();
        assertThat(universityCollegeList).hasSize(databaseSizeBeforeCreate + 1);
        UniversityCollege testUniversityCollege = universityCollegeList.get(universityCollegeList.size() - 1);
        assertThat(testUniversityCollege.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testUniversityCollege.getProvince()).isEqualTo(DEFAULT_PROVINCE);
        assertThat(testUniversityCollege.getDistrict()).isEqualTo(DEFAULT_DISTRICT);
        assertThat(testUniversityCollege.getWard()).isEqualTo(DEFAULT_WARD);
        assertThat(testUniversityCollege.isActive()).isEqualTo(DEFAULT_ACTIVE);

        // Validate the UniversityCollege in Elasticsearch
        verify(mockUniversityCollegeSearchRepository, times(1)).save(testUniversityCollege);
    }

    @Test
    @Transactional
    public void createUniversityCollegeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = universityCollegeRepository.findAll().size();

        // Create the UniversityCollege with an existing ID
        universityCollege.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUniversityCollegeMockMvc.perform(post("/api/university-colleges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(universityCollege)))
            .andExpect(status().isBadRequest());

        // Validate the UniversityCollege in the database
        List<UniversityCollege> universityCollegeList = universityCollegeRepository.findAll();
        assertThat(universityCollegeList).hasSize(databaseSizeBeforeCreate);

        // Validate the UniversityCollege in Elasticsearch
        verify(mockUniversityCollegeSearchRepository, times(0)).save(universityCollege);
    }


    @Test
    @Transactional
    public void getAllUniversityColleges() throws Exception {
        // Initialize the database
        universityCollegeRepository.saveAndFlush(universityCollege);

        // Get all the universityCollegeList
        restUniversityCollegeMockMvc.perform(get("/api/university-colleges?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(universityCollege.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getUniversityCollege() throws Exception {
        // Initialize the database
        universityCollegeRepository.saveAndFlush(universityCollege);

        // Get the universityCollege
        restUniversityCollegeMockMvc.perform(get("/api/university-colleges/{id}", universityCollege.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(universityCollege.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.province").value(DEFAULT_PROVINCE.toString()))
            .andExpect(jsonPath("$.district").value(DEFAULT_DISTRICT.toString()))
            .andExpect(jsonPath("$.ward").value(DEFAULT_WARD.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingUniversityCollege() throws Exception {
        // Get the universityCollege
        restUniversityCollegeMockMvc.perform(get("/api/university-colleges/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUniversityCollege() throws Exception {
        // Initialize the database
        universityCollegeRepository.saveAndFlush(universityCollege);

        int databaseSizeBeforeUpdate = universityCollegeRepository.findAll().size();

        // Update the universityCollege
        UniversityCollege updatedUniversityCollege = universityCollegeRepository.findById(universityCollege.getId()).get();
        // Disconnect from session so that the updates on updatedUniversityCollege are not directly saved in db
        em.detach(updatedUniversityCollege);
        updatedUniversityCollege
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);

        restUniversityCollegeMockMvc.perform(put("/api/university-colleges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUniversityCollege)))
            .andExpect(status().isOk());

        // Validate the UniversityCollege in the database
        List<UniversityCollege> universityCollegeList = universityCollegeRepository.findAll();
        assertThat(universityCollegeList).hasSize(databaseSizeBeforeUpdate);
        UniversityCollege testUniversityCollege = universityCollegeList.get(universityCollegeList.size() - 1);
        assertThat(testUniversityCollege.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testUniversityCollege.getProvince()).isEqualTo(UPDATED_PROVINCE);
        assertThat(testUniversityCollege.getDistrict()).isEqualTo(UPDATED_DISTRICT);
        assertThat(testUniversityCollege.getWard()).isEqualTo(UPDATED_WARD);
        assertThat(testUniversityCollege.isActive()).isEqualTo(UPDATED_ACTIVE);

        // Validate the UniversityCollege in Elasticsearch
        verify(mockUniversityCollegeSearchRepository, times(1)).save(testUniversityCollege);
    }

    @Test
    @Transactional
    public void updateNonExistingUniversityCollege() throws Exception {
        int databaseSizeBeforeUpdate = universityCollegeRepository.findAll().size();

        // Create the UniversityCollege

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUniversityCollegeMockMvc.perform(put("/api/university-colleges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(universityCollege)))
            .andExpect(status().isBadRequest());

        // Validate the UniversityCollege in the database
        List<UniversityCollege> universityCollegeList = universityCollegeRepository.findAll();
        assertThat(universityCollegeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the UniversityCollege in Elasticsearch
        verify(mockUniversityCollegeSearchRepository, times(0)).save(universityCollege);
    }

    @Test
    @Transactional
    public void deleteUniversityCollege() throws Exception {
        // Initialize the database
        universityCollegeRepository.saveAndFlush(universityCollege);

        int databaseSizeBeforeDelete = universityCollegeRepository.findAll().size();

        // Delete the universityCollege
        restUniversityCollegeMockMvc.perform(delete("/api/university-colleges/{id}", universityCollege.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UniversityCollege> universityCollegeList = universityCollegeRepository.findAll();
        assertThat(universityCollegeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the UniversityCollege in Elasticsearch
        verify(mockUniversityCollegeSearchRepository, times(1)).deleteById(universityCollege.getId());
    }

    @Test
    @Transactional
    public void searchUniversityCollege() throws Exception {
        // Initialize the database
        universityCollegeRepository.saveAndFlush(universityCollege);
        when(mockUniversityCollegeSearchRepository.search(queryStringQuery("id:" + universityCollege.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(universityCollege), PageRequest.of(0, 1), 1));
        // Search the universityCollege
        restUniversityCollegeMockMvc.perform(get("/api/_search/university-colleges?query=id:" + universityCollege.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(universityCollege.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE)))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT)))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UniversityCollege.class);
        UniversityCollege universityCollege1 = new UniversityCollege();
        universityCollege1.setId(1L);
        UniversityCollege universityCollege2 = new UniversityCollege();
        universityCollege2.setId(universityCollege1.getId());
        assertThat(universityCollege1).isEqualTo(universityCollege2);
        universityCollege2.setId(2L);
        assertThat(universityCollege1).isNotEqualTo(universityCollege2);
        universityCollege1.setId(null);
        assertThat(universityCollege1).isNotEqualTo(universityCollege2);
    }
}
