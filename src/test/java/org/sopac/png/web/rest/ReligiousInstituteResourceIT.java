package org.sopac.png.web.rest;

import org.sopac.png.PngDisabilityReligionApp;
import org.sopac.png.domain.ReligiousInstitute;
import org.sopac.png.repository.ReligiousInstituteRepository;
import org.sopac.png.repository.search.ReligiousInstituteSearchRepository;
import org.sopac.png.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.sopac.png.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ReligiousInstituteResource} REST controller.
 */
@SpringBootTest(classes = PngDisabilityReligionApp.class)
public class ReligiousInstituteResourceIT {

    private static final String DEFAULT_CHURCH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CHURCH_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CHURCH_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_CHURCH_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_DENOMINATION = "AAAAAAAAAA";
    private static final String UPDATED_DENOMINATION = "BBBBBBBBBB";

    private static final String DEFAULT_HEAD_OFFICE_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_HEAD_OFFICE_LOCATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMBER_CHURCH_MEMBERSHIP = 1;
    private static final Integer UPDATED_NUMBER_CHURCH_MEMBERSHIP = 2;

    @Autowired
    private ReligiousInstituteRepository religiousInstituteRepository;

    /**
     * This repository is mocked in the org.sopac.png.repository.search test package.
     *
     * @see org.sopac.png.repository.search.ReligiousInstituteSearchRepositoryMockConfiguration
     */
    @Autowired
    private ReligiousInstituteSearchRepository mockReligiousInstituteSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restReligiousInstituteMockMvc;

    private ReligiousInstitute religiousInstitute;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ReligiousInstituteResource religiousInstituteResource = new ReligiousInstituteResource(religiousInstituteRepository, mockReligiousInstituteSearchRepository);
        this.restReligiousInstituteMockMvc = MockMvcBuilders.standaloneSetup(religiousInstituteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReligiousInstitute createEntity(EntityManager em) {
        ReligiousInstitute religiousInstitute = new ReligiousInstitute()
            .churchName(DEFAULT_CHURCH_NAME)
            .churchAddress(DEFAULT_CHURCH_ADDRESS)
            .denomination(DEFAULT_DENOMINATION)
            .headOfficeLocation(DEFAULT_HEAD_OFFICE_LOCATION)
            .numberChurchMembership(DEFAULT_NUMBER_CHURCH_MEMBERSHIP);
        return religiousInstitute;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReligiousInstitute createUpdatedEntity(EntityManager em) {
        ReligiousInstitute religiousInstitute = new ReligiousInstitute()
            .churchName(UPDATED_CHURCH_NAME)
            .churchAddress(UPDATED_CHURCH_ADDRESS)
            .denomination(UPDATED_DENOMINATION)
            .headOfficeLocation(UPDATED_HEAD_OFFICE_LOCATION)
            .numberChurchMembership(UPDATED_NUMBER_CHURCH_MEMBERSHIP);
        return religiousInstitute;
    }

    @BeforeEach
    public void initTest() {
        religiousInstitute = createEntity(em);
    }

    @Test
    @Transactional
    public void createReligiousInstitute() throws Exception {
        int databaseSizeBeforeCreate = religiousInstituteRepository.findAll().size();

        // Create the ReligiousInstitute
        restReligiousInstituteMockMvc.perform(post("/api/religious-institutes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(religiousInstitute)))
            .andExpect(status().isCreated());

        // Validate the ReligiousInstitute in the database
        List<ReligiousInstitute> religiousInstituteList = religiousInstituteRepository.findAll();
        assertThat(religiousInstituteList).hasSize(databaseSizeBeforeCreate + 1);
        ReligiousInstitute testReligiousInstitute = religiousInstituteList.get(religiousInstituteList.size() - 1);
        assertThat(testReligiousInstitute.getChurchName()).isEqualTo(DEFAULT_CHURCH_NAME);
        assertThat(testReligiousInstitute.getChurchAddress()).isEqualTo(DEFAULT_CHURCH_ADDRESS);
        assertThat(testReligiousInstitute.getDenomination()).isEqualTo(DEFAULT_DENOMINATION);
        assertThat(testReligiousInstitute.getHeadOfficeLocation()).isEqualTo(DEFAULT_HEAD_OFFICE_LOCATION);
        assertThat(testReligiousInstitute.getNumberChurchMembership()).isEqualTo(DEFAULT_NUMBER_CHURCH_MEMBERSHIP);

        // Validate the ReligiousInstitute in Elasticsearch
        verify(mockReligiousInstituteSearchRepository, times(1)).save(testReligiousInstitute);
    }

    @Test
    @Transactional
    public void createReligiousInstituteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = religiousInstituteRepository.findAll().size();

        // Create the ReligiousInstitute with an existing ID
        religiousInstitute.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReligiousInstituteMockMvc.perform(post("/api/religious-institutes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(religiousInstitute)))
            .andExpect(status().isBadRequest());

        // Validate the ReligiousInstitute in the database
        List<ReligiousInstitute> religiousInstituteList = religiousInstituteRepository.findAll();
        assertThat(religiousInstituteList).hasSize(databaseSizeBeforeCreate);

        // Validate the ReligiousInstitute in Elasticsearch
        verify(mockReligiousInstituteSearchRepository, times(0)).save(religiousInstitute);
    }


    @Test
    @Transactional
    public void getAllReligiousInstitutes() throws Exception {
        // Initialize the database
        religiousInstituteRepository.saveAndFlush(religiousInstitute);

        // Get all the religiousInstituteList
        restReligiousInstituteMockMvc.perform(get("/api/religious-institutes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(religiousInstitute.getId().intValue())))
            .andExpect(jsonPath("$.[*].churchName").value(hasItem(DEFAULT_CHURCH_NAME.toString())))
            .andExpect(jsonPath("$.[*].churchAddress").value(hasItem(DEFAULT_CHURCH_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].denomination").value(hasItem(DEFAULT_DENOMINATION.toString())))
            .andExpect(jsonPath("$.[*].headOfficeLocation").value(hasItem(DEFAULT_HEAD_OFFICE_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].numberChurchMembership").value(hasItem(DEFAULT_NUMBER_CHURCH_MEMBERSHIP)));
    }
    
    @Test
    @Transactional
    public void getReligiousInstitute() throws Exception {
        // Initialize the database
        religiousInstituteRepository.saveAndFlush(religiousInstitute);

        // Get the religiousInstitute
        restReligiousInstituteMockMvc.perform(get("/api/religious-institutes/{id}", religiousInstitute.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(religiousInstitute.getId().intValue()))
            .andExpect(jsonPath("$.churchName").value(DEFAULT_CHURCH_NAME.toString()))
            .andExpect(jsonPath("$.churchAddress").value(DEFAULT_CHURCH_ADDRESS.toString()))
            .andExpect(jsonPath("$.denomination").value(DEFAULT_DENOMINATION.toString()))
            .andExpect(jsonPath("$.headOfficeLocation").value(DEFAULT_HEAD_OFFICE_LOCATION.toString()))
            .andExpect(jsonPath("$.numberChurchMembership").value(DEFAULT_NUMBER_CHURCH_MEMBERSHIP));
    }

    @Test
    @Transactional
    public void getNonExistingReligiousInstitute() throws Exception {
        // Get the religiousInstitute
        restReligiousInstituteMockMvc.perform(get("/api/religious-institutes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReligiousInstitute() throws Exception {
        // Initialize the database
        religiousInstituteRepository.saveAndFlush(religiousInstitute);

        int databaseSizeBeforeUpdate = religiousInstituteRepository.findAll().size();

        // Update the religiousInstitute
        ReligiousInstitute updatedReligiousInstitute = religiousInstituteRepository.findById(religiousInstitute.getId()).get();
        // Disconnect from session so that the updates on updatedReligiousInstitute are not directly saved in db
        em.detach(updatedReligiousInstitute);
        updatedReligiousInstitute
            .churchName(UPDATED_CHURCH_NAME)
            .churchAddress(UPDATED_CHURCH_ADDRESS)
            .denomination(UPDATED_DENOMINATION)
            .headOfficeLocation(UPDATED_HEAD_OFFICE_LOCATION)
            .numberChurchMembership(UPDATED_NUMBER_CHURCH_MEMBERSHIP);

        restReligiousInstituteMockMvc.perform(put("/api/religious-institutes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedReligiousInstitute)))
            .andExpect(status().isOk());

        // Validate the ReligiousInstitute in the database
        List<ReligiousInstitute> religiousInstituteList = religiousInstituteRepository.findAll();
        assertThat(religiousInstituteList).hasSize(databaseSizeBeforeUpdate);
        ReligiousInstitute testReligiousInstitute = religiousInstituteList.get(religiousInstituteList.size() - 1);
        assertThat(testReligiousInstitute.getChurchName()).isEqualTo(UPDATED_CHURCH_NAME);
        assertThat(testReligiousInstitute.getChurchAddress()).isEqualTo(UPDATED_CHURCH_ADDRESS);
        assertThat(testReligiousInstitute.getDenomination()).isEqualTo(UPDATED_DENOMINATION);
        assertThat(testReligiousInstitute.getHeadOfficeLocation()).isEqualTo(UPDATED_HEAD_OFFICE_LOCATION);
        assertThat(testReligiousInstitute.getNumberChurchMembership()).isEqualTo(UPDATED_NUMBER_CHURCH_MEMBERSHIP);

        // Validate the ReligiousInstitute in Elasticsearch
        verify(mockReligiousInstituteSearchRepository, times(1)).save(testReligiousInstitute);
    }

    @Test
    @Transactional
    public void updateNonExistingReligiousInstitute() throws Exception {
        int databaseSizeBeforeUpdate = religiousInstituteRepository.findAll().size();

        // Create the ReligiousInstitute

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReligiousInstituteMockMvc.perform(put("/api/religious-institutes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(religiousInstitute)))
            .andExpect(status().isBadRequest());

        // Validate the ReligiousInstitute in the database
        List<ReligiousInstitute> religiousInstituteList = religiousInstituteRepository.findAll();
        assertThat(religiousInstituteList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ReligiousInstitute in Elasticsearch
        verify(mockReligiousInstituteSearchRepository, times(0)).save(religiousInstitute);
    }

    @Test
    @Transactional
    public void deleteReligiousInstitute() throws Exception {
        // Initialize the database
        religiousInstituteRepository.saveAndFlush(religiousInstitute);

        int databaseSizeBeforeDelete = religiousInstituteRepository.findAll().size();

        // Delete the religiousInstitute
        restReligiousInstituteMockMvc.perform(delete("/api/religious-institutes/{id}", religiousInstitute.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ReligiousInstitute> religiousInstituteList = religiousInstituteRepository.findAll();
        assertThat(religiousInstituteList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ReligiousInstitute in Elasticsearch
        verify(mockReligiousInstituteSearchRepository, times(1)).deleteById(religiousInstitute.getId());
    }

    @Test
    @Transactional
    public void searchReligiousInstitute() throws Exception {
        // Initialize the database
        religiousInstituteRepository.saveAndFlush(religiousInstitute);
        when(mockReligiousInstituteSearchRepository.search(queryStringQuery("id:" + religiousInstitute.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(religiousInstitute), PageRequest.of(0, 1), 1));
        // Search the religiousInstitute
        restReligiousInstituteMockMvc.perform(get("/api/_search/religious-institutes?query=id:" + religiousInstitute.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(religiousInstitute.getId().intValue())))
            .andExpect(jsonPath("$.[*].churchName").value(hasItem(DEFAULT_CHURCH_NAME)))
            .andExpect(jsonPath("$.[*].churchAddress").value(hasItem(DEFAULT_CHURCH_ADDRESS)))
            .andExpect(jsonPath("$.[*].denomination").value(hasItem(DEFAULT_DENOMINATION)))
            .andExpect(jsonPath("$.[*].headOfficeLocation").value(hasItem(DEFAULT_HEAD_OFFICE_LOCATION)))
            .andExpect(jsonPath("$.[*].numberChurchMembership").value(hasItem(DEFAULT_NUMBER_CHURCH_MEMBERSHIP)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReligiousInstitute.class);
        ReligiousInstitute religiousInstitute1 = new ReligiousInstitute();
        religiousInstitute1.setId(1L);
        ReligiousInstitute religiousInstitute2 = new ReligiousInstitute();
        religiousInstitute2.setId(religiousInstitute1.getId());
        assertThat(religiousInstitute1).isEqualTo(religiousInstitute2);
        religiousInstitute2.setId(2L);
        assertThat(religiousInstitute1).isNotEqualTo(religiousInstitute2);
        religiousInstitute1.setId(null);
        assertThat(religiousInstitute1).isNotEqualTo(religiousInstitute2);
    }
}
