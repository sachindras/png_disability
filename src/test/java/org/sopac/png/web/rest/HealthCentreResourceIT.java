package org.sopac.png.web.rest;

import org.sopac.png.PngDisabilityReligionApp;
import org.sopac.png.domain.HealthCentre;
import org.sopac.png.repository.HealthCentreRepository;
import org.sopac.png.repository.search.HealthCentreSearchRepository;
import org.sopac.png.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.sopac.png.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link HealthCentreResource} REST controller.
 */
@SpringBootTest(classes = PngDisabilityReligionApp.class)
public class HealthCentreResourceIT {

    private static final String DEFAULT_CENTRE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CENTRE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCE = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCE = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_WARD = "AAAAAAAAAA";
    private static final String UPDATED_WARD = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private HealthCentreRepository healthCentreRepository;

    /**
     * This repository is mocked in the org.sopac.png.repository.search test package.
     *
     * @see org.sopac.png.repository.search.HealthCentreSearchRepositoryMockConfiguration
     */
    @Autowired
    private HealthCentreSearchRepository mockHealthCentreSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restHealthCentreMockMvc;

    private HealthCentre healthCentre;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HealthCentreResource healthCentreResource = new HealthCentreResource(healthCentreRepository, mockHealthCentreSearchRepository);
        this.restHealthCentreMockMvc = MockMvcBuilders.standaloneSetup(healthCentreResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HealthCentre createEntity(EntityManager em) {
        HealthCentre healthCentre = new HealthCentre()
            .centreName(DEFAULT_CENTRE_NAME)
            .province(DEFAULT_PROVINCE)
            .district(DEFAULT_DISTRICT)
            .ward(DEFAULT_WARD)
            .active(DEFAULT_ACTIVE);
        return healthCentre;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HealthCentre createUpdatedEntity(EntityManager em) {
        HealthCentre healthCentre = new HealthCentre()
            .centreName(UPDATED_CENTRE_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);
        return healthCentre;
    }

    @BeforeEach
    public void initTest() {
        healthCentre = createEntity(em);
    }

    @Test
    @Transactional
    public void createHealthCentre() throws Exception {
        int databaseSizeBeforeCreate = healthCentreRepository.findAll().size();

        // Create the HealthCentre
        restHealthCentreMockMvc.perform(post("/api/health-centres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(healthCentre)))
            .andExpect(status().isCreated());

        // Validate the HealthCentre in the database
        List<HealthCentre> healthCentreList = healthCentreRepository.findAll();
        assertThat(healthCentreList).hasSize(databaseSizeBeforeCreate + 1);
        HealthCentre testHealthCentre = healthCentreList.get(healthCentreList.size() - 1);
        assertThat(testHealthCentre.getCentreName()).isEqualTo(DEFAULT_CENTRE_NAME);
        assertThat(testHealthCentre.getProvince()).isEqualTo(DEFAULT_PROVINCE);
        assertThat(testHealthCentre.getDistrict()).isEqualTo(DEFAULT_DISTRICT);
        assertThat(testHealthCentre.getWard()).isEqualTo(DEFAULT_WARD);
        assertThat(testHealthCentre.isActive()).isEqualTo(DEFAULT_ACTIVE);

        // Validate the HealthCentre in Elasticsearch
        verify(mockHealthCentreSearchRepository, times(1)).save(testHealthCentre);
    }

    @Test
    @Transactional
    public void createHealthCentreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = healthCentreRepository.findAll().size();

        // Create the HealthCentre with an existing ID
        healthCentre.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHealthCentreMockMvc.perform(post("/api/health-centres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(healthCentre)))
            .andExpect(status().isBadRequest());

        // Validate the HealthCentre in the database
        List<HealthCentre> healthCentreList = healthCentreRepository.findAll();
        assertThat(healthCentreList).hasSize(databaseSizeBeforeCreate);

        // Validate the HealthCentre in Elasticsearch
        verify(mockHealthCentreSearchRepository, times(0)).save(healthCentre);
    }


    @Test
    @Transactional
    public void getAllHealthCentres() throws Exception {
        // Initialize the database
        healthCentreRepository.saveAndFlush(healthCentre);

        // Get all the healthCentreList
        restHealthCentreMockMvc.perform(get("/api/health-centres?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(healthCentre.getId().intValue())))
            .andExpect(jsonPath("$.[*].centreName").value(hasItem(DEFAULT_CENTRE_NAME.toString())))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getHealthCentre() throws Exception {
        // Initialize the database
        healthCentreRepository.saveAndFlush(healthCentre);

        // Get the healthCentre
        restHealthCentreMockMvc.perform(get("/api/health-centres/{id}", healthCentre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(healthCentre.getId().intValue()))
            .andExpect(jsonPath("$.centreName").value(DEFAULT_CENTRE_NAME.toString()))
            .andExpect(jsonPath("$.province").value(DEFAULT_PROVINCE.toString()))
            .andExpect(jsonPath("$.district").value(DEFAULT_DISTRICT.toString()))
            .andExpect(jsonPath("$.ward").value(DEFAULT_WARD.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingHealthCentre() throws Exception {
        // Get the healthCentre
        restHealthCentreMockMvc.perform(get("/api/health-centres/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHealthCentre() throws Exception {
        // Initialize the database
        healthCentreRepository.saveAndFlush(healthCentre);

        int databaseSizeBeforeUpdate = healthCentreRepository.findAll().size();

        // Update the healthCentre
        HealthCentre updatedHealthCentre = healthCentreRepository.findById(healthCentre.getId()).get();
        // Disconnect from session so that the updates on updatedHealthCentre are not directly saved in db
        em.detach(updatedHealthCentre);
        updatedHealthCentre
            .centreName(UPDATED_CENTRE_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);

        restHealthCentreMockMvc.perform(put("/api/health-centres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedHealthCentre)))
            .andExpect(status().isOk());

        // Validate the HealthCentre in the database
        List<HealthCentre> healthCentreList = healthCentreRepository.findAll();
        assertThat(healthCentreList).hasSize(databaseSizeBeforeUpdate);
        HealthCentre testHealthCentre = healthCentreList.get(healthCentreList.size() - 1);
        assertThat(testHealthCentre.getCentreName()).isEqualTo(UPDATED_CENTRE_NAME);
        assertThat(testHealthCentre.getProvince()).isEqualTo(UPDATED_PROVINCE);
        assertThat(testHealthCentre.getDistrict()).isEqualTo(UPDATED_DISTRICT);
        assertThat(testHealthCentre.getWard()).isEqualTo(UPDATED_WARD);
        assertThat(testHealthCentre.isActive()).isEqualTo(UPDATED_ACTIVE);

        // Validate the HealthCentre in Elasticsearch
        verify(mockHealthCentreSearchRepository, times(1)).save(testHealthCentre);
    }

    @Test
    @Transactional
    public void updateNonExistingHealthCentre() throws Exception {
        int databaseSizeBeforeUpdate = healthCentreRepository.findAll().size();

        // Create the HealthCentre

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHealthCentreMockMvc.perform(put("/api/health-centres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(healthCentre)))
            .andExpect(status().isBadRequest());

        // Validate the HealthCentre in the database
        List<HealthCentre> healthCentreList = healthCentreRepository.findAll();
        assertThat(healthCentreList).hasSize(databaseSizeBeforeUpdate);

        // Validate the HealthCentre in Elasticsearch
        verify(mockHealthCentreSearchRepository, times(0)).save(healthCentre);
    }

    @Test
    @Transactional
    public void deleteHealthCentre() throws Exception {
        // Initialize the database
        healthCentreRepository.saveAndFlush(healthCentre);

        int databaseSizeBeforeDelete = healthCentreRepository.findAll().size();

        // Delete the healthCentre
        restHealthCentreMockMvc.perform(delete("/api/health-centres/{id}", healthCentre.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HealthCentre> healthCentreList = healthCentreRepository.findAll();
        assertThat(healthCentreList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the HealthCentre in Elasticsearch
        verify(mockHealthCentreSearchRepository, times(1)).deleteById(healthCentre.getId());
    }

    @Test
    @Transactional
    public void searchHealthCentre() throws Exception {
        // Initialize the database
        healthCentreRepository.saveAndFlush(healthCentre);
        when(mockHealthCentreSearchRepository.search(queryStringQuery("id:" + healthCentre.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(healthCentre), PageRequest.of(0, 1), 1));
        // Search the healthCentre
        restHealthCentreMockMvc.perform(get("/api/_search/health-centres?query=id:" + healthCentre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(healthCentre.getId().intValue())))
            .andExpect(jsonPath("$.[*].centreName").value(hasItem(DEFAULT_CENTRE_NAME)))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE)))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT)))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HealthCentre.class);
        HealthCentre healthCentre1 = new HealthCentre();
        healthCentre1.setId(1L);
        HealthCentre healthCentre2 = new HealthCentre();
        healthCentre2.setId(healthCentre1.getId());
        assertThat(healthCentre1).isEqualTo(healthCentre2);
        healthCentre2.setId(2L);
        assertThat(healthCentre1).isNotEqualTo(healthCentre2);
        healthCentre1.setId(null);
        assertThat(healthCentre1).isNotEqualTo(healthCentre2);
    }
}
