package org.sopac.png.web.rest;

import org.sopac.png.PngDisabilityReligionApp;
import org.sopac.png.domain.SecondarySchool;
import org.sopac.png.repository.SecondarySchoolRepository;
import org.sopac.png.repository.search.SecondarySchoolSearchRepository;
import org.sopac.png.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.sopac.png.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link SecondarySchoolResource} REST controller.
 */
@SpringBootTest(classes = PngDisabilityReligionApp.class)
public class SecondarySchoolResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCE = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCE = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_WARD = "AAAAAAAAAA";
    private static final String UPDATED_WARD = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private SecondarySchoolRepository secondarySchoolRepository;

    /**
     * This repository is mocked in the org.sopac.png.repository.search test package.
     *
     * @see org.sopac.png.repository.search.SecondarySchoolSearchRepositoryMockConfiguration
     */
    @Autowired
    private SecondarySchoolSearchRepository mockSecondarySchoolSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSecondarySchoolMockMvc;

    private SecondarySchool secondarySchool;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SecondarySchoolResource secondarySchoolResource = new SecondarySchoolResource(secondarySchoolRepository, mockSecondarySchoolSearchRepository);
        this.restSecondarySchoolMockMvc = MockMvcBuilders.standaloneSetup(secondarySchoolResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SecondarySchool createEntity(EntityManager em) {
        SecondarySchool secondarySchool = new SecondarySchool()
            .name(DEFAULT_NAME)
            .province(DEFAULT_PROVINCE)
            .district(DEFAULT_DISTRICT)
            .ward(DEFAULT_WARD)
            .active(DEFAULT_ACTIVE);
        return secondarySchool;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SecondarySchool createUpdatedEntity(EntityManager em) {
        SecondarySchool secondarySchool = new SecondarySchool()
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);
        return secondarySchool;
    }

    @BeforeEach
    public void initTest() {
        secondarySchool = createEntity(em);
    }

    @Test
    @Transactional
    public void createSecondarySchool() throws Exception {
        int databaseSizeBeforeCreate = secondarySchoolRepository.findAll().size();

        // Create the SecondarySchool
        restSecondarySchoolMockMvc.perform(post("/api/secondary-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secondarySchool)))
            .andExpect(status().isCreated());

        // Validate the SecondarySchool in the database
        List<SecondarySchool> secondarySchoolList = secondarySchoolRepository.findAll();
        assertThat(secondarySchoolList).hasSize(databaseSizeBeforeCreate + 1);
        SecondarySchool testSecondarySchool = secondarySchoolList.get(secondarySchoolList.size() - 1);
        assertThat(testSecondarySchool.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSecondarySchool.getProvince()).isEqualTo(DEFAULT_PROVINCE);
        assertThat(testSecondarySchool.getDistrict()).isEqualTo(DEFAULT_DISTRICT);
        assertThat(testSecondarySchool.getWard()).isEqualTo(DEFAULT_WARD);
        assertThat(testSecondarySchool.isActive()).isEqualTo(DEFAULT_ACTIVE);

        // Validate the SecondarySchool in Elasticsearch
        verify(mockSecondarySchoolSearchRepository, times(1)).save(testSecondarySchool);
    }

    @Test
    @Transactional
    public void createSecondarySchoolWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = secondarySchoolRepository.findAll().size();

        // Create the SecondarySchool with an existing ID
        secondarySchool.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSecondarySchoolMockMvc.perform(post("/api/secondary-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secondarySchool)))
            .andExpect(status().isBadRequest());

        // Validate the SecondarySchool in the database
        List<SecondarySchool> secondarySchoolList = secondarySchoolRepository.findAll();
        assertThat(secondarySchoolList).hasSize(databaseSizeBeforeCreate);

        // Validate the SecondarySchool in Elasticsearch
        verify(mockSecondarySchoolSearchRepository, times(0)).save(secondarySchool);
    }


    @Test
    @Transactional
    public void getAllSecondarySchools() throws Exception {
        // Initialize the database
        secondarySchoolRepository.saveAndFlush(secondarySchool);

        // Get all the secondarySchoolList
        restSecondarySchoolMockMvc.perform(get("/api/secondary-schools?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(secondarySchool.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getSecondarySchool() throws Exception {
        // Initialize the database
        secondarySchoolRepository.saveAndFlush(secondarySchool);

        // Get the secondarySchool
        restSecondarySchoolMockMvc.perform(get("/api/secondary-schools/{id}", secondarySchool.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(secondarySchool.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.province").value(DEFAULT_PROVINCE.toString()))
            .andExpect(jsonPath("$.district").value(DEFAULT_DISTRICT.toString()))
            .andExpect(jsonPath("$.ward").value(DEFAULT_WARD.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSecondarySchool() throws Exception {
        // Get the secondarySchool
        restSecondarySchoolMockMvc.perform(get("/api/secondary-schools/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSecondarySchool() throws Exception {
        // Initialize the database
        secondarySchoolRepository.saveAndFlush(secondarySchool);

        int databaseSizeBeforeUpdate = secondarySchoolRepository.findAll().size();

        // Update the secondarySchool
        SecondarySchool updatedSecondarySchool = secondarySchoolRepository.findById(secondarySchool.getId()).get();
        // Disconnect from session so that the updates on updatedSecondarySchool are not directly saved in db
        em.detach(updatedSecondarySchool);
        updatedSecondarySchool
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);

        restSecondarySchoolMockMvc.perform(put("/api/secondary-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSecondarySchool)))
            .andExpect(status().isOk());

        // Validate the SecondarySchool in the database
        List<SecondarySchool> secondarySchoolList = secondarySchoolRepository.findAll();
        assertThat(secondarySchoolList).hasSize(databaseSizeBeforeUpdate);
        SecondarySchool testSecondarySchool = secondarySchoolList.get(secondarySchoolList.size() - 1);
        assertThat(testSecondarySchool.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSecondarySchool.getProvince()).isEqualTo(UPDATED_PROVINCE);
        assertThat(testSecondarySchool.getDistrict()).isEqualTo(UPDATED_DISTRICT);
        assertThat(testSecondarySchool.getWard()).isEqualTo(UPDATED_WARD);
        assertThat(testSecondarySchool.isActive()).isEqualTo(UPDATED_ACTIVE);

        // Validate the SecondarySchool in Elasticsearch
        verify(mockSecondarySchoolSearchRepository, times(1)).save(testSecondarySchool);
    }

    @Test
    @Transactional
    public void updateNonExistingSecondarySchool() throws Exception {
        int databaseSizeBeforeUpdate = secondarySchoolRepository.findAll().size();

        // Create the SecondarySchool

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSecondarySchoolMockMvc.perform(put("/api/secondary-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secondarySchool)))
            .andExpect(status().isBadRequest());

        // Validate the SecondarySchool in the database
        List<SecondarySchool> secondarySchoolList = secondarySchoolRepository.findAll();
        assertThat(secondarySchoolList).hasSize(databaseSizeBeforeUpdate);

        // Validate the SecondarySchool in Elasticsearch
        verify(mockSecondarySchoolSearchRepository, times(0)).save(secondarySchool);
    }

    @Test
    @Transactional
    public void deleteSecondarySchool() throws Exception {
        // Initialize the database
        secondarySchoolRepository.saveAndFlush(secondarySchool);

        int databaseSizeBeforeDelete = secondarySchoolRepository.findAll().size();

        // Delete the secondarySchool
        restSecondarySchoolMockMvc.perform(delete("/api/secondary-schools/{id}", secondarySchool.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SecondarySchool> secondarySchoolList = secondarySchoolRepository.findAll();
        assertThat(secondarySchoolList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the SecondarySchool in Elasticsearch
        verify(mockSecondarySchoolSearchRepository, times(1)).deleteById(secondarySchool.getId());
    }

    @Test
    @Transactional
    public void searchSecondarySchool() throws Exception {
        // Initialize the database
        secondarySchoolRepository.saveAndFlush(secondarySchool);
        when(mockSecondarySchoolSearchRepository.search(queryStringQuery("id:" + secondarySchool.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(secondarySchool), PageRequest.of(0, 1), 1));
        // Search the secondarySchool
        restSecondarySchoolMockMvc.perform(get("/api/_search/secondary-schools?query=id:" + secondarySchool.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(secondarySchool.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE)))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT)))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SecondarySchool.class);
        SecondarySchool secondarySchool1 = new SecondarySchool();
        secondarySchool1.setId(1L);
        SecondarySchool secondarySchool2 = new SecondarySchool();
        secondarySchool2.setId(secondarySchool1.getId());
        assertThat(secondarySchool1).isEqualTo(secondarySchool2);
        secondarySchool2.setId(2L);
        assertThat(secondarySchool1).isNotEqualTo(secondarySchool2);
        secondarySchool1.setId(null);
        assertThat(secondarySchool1).isNotEqualTo(secondarySchool2);
    }
}
