package org.sopac.png.web.rest;

import org.sopac.png.PngDisabilityReligionApp;
import org.sopac.png.domain.ChurchLeader;
import org.sopac.png.repository.ChurchLeaderRepository;
import org.sopac.png.repository.search.ChurchLeaderSearchRepository;
import org.sopac.png.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.sopac.png.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ChurchLeaderResource} REST controller.
 */
@SpringBootTest(classes = PngDisabilityReligionApp.class)
public class ChurchLeaderResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESIGNATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    @Autowired
    private ChurchLeaderRepository churchLeaderRepository;

    /**
     * This repository is mocked in the org.sopac.png.repository.search test package.
     *
     * @see org.sopac.png.repository.search.ChurchLeaderSearchRepositoryMockConfiguration
     */
    @Autowired
    private ChurchLeaderSearchRepository mockChurchLeaderSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restChurchLeaderMockMvc;

    private ChurchLeader churchLeader;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ChurchLeaderResource churchLeaderResource = new ChurchLeaderResource(churchLeaderRepository, mockChurchLeaderSearchRepository);
        this.restChurchLeaderMockMvc = MockMvcBuilders.standaloneSetup(churchLeaderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChurchLeader createEntity(EntityManager em) {
        ChurchLeader churchLeader = new ChurchLeader()
            .name(DEFAULT_NAME)
            .designation(DEFAULT_DESIGNATION)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .email(DEFAULT_EMAIL);
        return churchLeader;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChurchLeader createUpdatedEntity(EntityManager em) {
        ChurchLeader churchLeader = new ChurchLeader()
            .name(UPDATED_NAME)
            .designation(UPDATED_DESIGNATION)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL);
        return churchLeader;
    }

    @BeforeEach
    public void initTest() {
        churchLeader = createEntity(em);
    }

    @Test
    @Transactional
    public void createChurchLeader() throws Exception {
        int databaseSizeBeforeCreate = churchLeaderRepository.findAll().size();

        // Create the ChurchLeader
        restChurchLeaderMockMvc.perform(post("/api/church-leaders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(churchLeader)))
            .andExpect(status().isCreated());

        // Validate the ChurchLeader in the database
        List<ChurchLeader> churchLeaderList = churchLeaderRepository.findAll();
        assertThat(churchLeaderList).hasSize(databaseSizeBeforeCreate + 1);
        ChurchLeader testChurchLeader = churchLeaderList.get(churchLeaderList.size() - 1);
        assertThat(testChurchLeader.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testChurchLeader.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testChurchLeader.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testChurchLeader.getEmail()).isEqualTo(DEFAULT_EMAIL);

        // Validate the ChurchLeader in Elasticsearch
        verify(mockChurchLeaderSearchRepository, times(1)).save(testChurchLeader);
    }

    @Test
    @Transactional
    public void createChurchLeaderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = churchLeaderRepository.findAll().size();

        // Create the ChurchLeader with an existing ID
        churchLeader.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChurchLeaderMockMvc.perform(post("/api/church-leaders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(churchLeader)))
            .andExpect(status().isBadRequest());

        // Validate the ChurchLeader in the database
        List<ChurchLeader> churchLeaderList = churchLeaderRepository.findAll();
        assertThat(churchLeaderList).hasSize(databaseSizeBeforeCreate);

        // Validate the ChurchLeader in Elasticsearch
        verify(mockChurchLeaderSearchRepository, times(0)).save(churchLeader);
    }


    @Test
    @Transactional
    public void getAllChurchLeaders() throws Exception {
        // Initialize the database
        churchLeaderRepository.saveAndFlush(churchLeader);

        // Get all the churchLeaderList
        restChurchLeaderMockMvc.perform(get("/api/church-leaders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(churchLeader.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION.toString())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())));
    }
    
    @Test
    @Transactional
    public void getChurchLeader() throws Exception {
        // Initialize the database
        churchLeaderRepository.saveAndFlush(churchLeader);

        // Get the churchLeader
        restChurchLeaderMockMvc.perform(get("/api/church-leaders/{id}", churchLeader.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(churchLeader.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION.toString()))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingChurchLeader() throws Exception {
        // Get the churchLeader
        restChurchLeaderMockMvc.perform(get("/api/church-leaders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChurchLeader() throws Exception {
        // Initialize the database
        churchLeaderRepository.saveAndFlush(churchLeader);

        int databaseSizeBeforeUpdate = churchLeaderRepository.findAll().size();

        // Update the churchLeader
        ChurchLeader updatedChurchLeader = churchLeaderRepository.findById(churchLeader.getId()).get();
        // Disconnect from session so that the updates on updatedChurchLeader are not directly saved in db
        em.detach(updatedChurchLeader);
        updatedChurchLeader
            .name(UPDATED_NAME)
            .designation(UPDATED_DESIGNATION)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL);

        restChurchLeaderMockMvc.perform(put("/api/church-leaders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedChurchLeader)))
            .andExpect(status().isOk());

        // Validate the ChurchLeader in the database
        List<ChurchLeader> churchLeaderList = churchLeaderRepository.findAll();
        assertThat(churchLeaderList).hasSize(databaseSizeBeforeUpdate);
        ChurchLeader testChurchLeader = churchLeaderList.get(churchLeaderList.size() - 1);
        assertThat(testChurchLeader.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testChurchLeader.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testChurchLeader.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testChurchLeader.getEmail()).isEqualTo(UPDATED_EMAIL);

        // Validate the ChurchLeader in Elasticsearch
        verify(mockChurchLeaderSearchRepository, times(1)).save(testChurchLeader);
    }

    @Test
    @Transactional
    public void updateNonExistingChurchLeader() throws Exception {
        int databaseSizeBeforeUpdate = churchLeaderRepository.findAll().size();

        // Create the ChurchLeader

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChurchLeaderMockMvc.perform(put("/api/church-leaders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(churchLeader)))
            .andExpect(status().isBadRequest());

        // Validate the ChurchLeader in the database
        List<ChurchLeader> churchLeaderList = churchLeaderRepository.findAll();
        assertThat(churchLeaderList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ChurchLeader in Elasticsearch
        verify(mockChurchLeaderSearchRepository, times(0)).save(churchLeader);
    }

    @Test
    @Transactional
    public void deleteChurchLeader() throws Exception {
        // Initialize the database
        churchLeaderRepository.saveAndFlush(churchLeader);

        int databaseSizeBeforeDelete = churchLeaderRepository.findAll().size();

        // Delete the churchLeader
        restChurchLeaderMockMvc.perform(delete("/api/church-leaders/{id}", churchLeader.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChurchLeader> churchLeaderList = churchLeaderRepository.findAll();
        assertThat(churchLeaderList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ChurchLeader in Elasticsearch
        verify(mockChurchLeaderSearchRepository, times(1)).deleteById(churchLeader.getId());
    }

    @Test
    @Transactional
    public void searchChurchLeader() throws Exception {
        // Initialize the database
        churchLeaderRepository.saveAndFlush(churchLeader);
        when(mockChurchLeaderSearchRepository.search(queryStringQuery("id:" + churchLeader.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(churchLeader), PageRequest.of(0, 1), 1));
        // Search the churchLeader
        restChurchLeaderMockMvc.perform(get("/api/_search/church-leaders?query=id:" + churchLeader.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(churchLeader.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChurchLeader.class);
        ChurchLeader churchLeader1 = new ChurchLeader();
        churchLeader1.setId(1L);
        ChurchLeader churchLeader2 = new ChurchLeader();
        churchLeader2.setId(churchLeader1.getId());
        assertThat(churchLeader1).isEqualTo(churchLeader2);
        churchLeader2.setId(2L);
        assertThat(churchLeader1).isNotEqualTo(churchLeader2);
        churchLeader1.setId(null);
        assertThat(churchLeader1).isNotEqualTo(churchLeader2);
    }
}
