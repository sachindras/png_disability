package org.sopac.png.web.rest;

import org.sopac.png.PngDisabilityReligionApp;
import org.sopac.png.domain.DisabledIndividual;
import org.sopac.png.repository.DisabledIndividualRepository;
import org.sopac.png.repository.search.DisabledIndividualSearchRepository;
import org.sopac.png.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import static org.sopac.png.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.sopac.png.domain.enumeration.Gender;
import org.sopac.png.domain.enumeration.EstimateAge;
import org.sopac.png.domain.enumeration.MartialStatus;
import org.sopac.png.domain.enumeration.CarerRelationship;
import org.sopac.png.domain.enumeration.Difficulty;
import org.sopac.png.domain.enumeration.Difficulty;
import org.sopac.png.domain.enumeration.Difficulty;
import org.sopac.png.domain.enumeration.Difficulty;
import org.sopac.png.domain.enumeration.Difficulty;
import org.sopac.png.domain.enumeration.Difficulty;
import org.sopac.png.domain.enumeration.CauseOfDisability;
import org.sopac.png.domain.enumeration.ServiceAvailibility;
import org.sopac.png.domain.enumeration.ServiceAccessibility;
import org.sopac.png.domain.enumeration.ServiceAvailibility;
import org.sopac.png.domain.enumeration.ServiceAccessibility;
import org.sopac.png.domain.enumeration.ServiceAvailibility;
import org.sopac.png.domain.enumeration.ServiceAccessibility;
import org.sopac.png.domain.enumeration.ServiceAvailibility;
import org.sopac.png.domain.enumeration.ServiceAccessibility;
import org.sopac.png.domain.enumeration.ServiceAvailibility;
import org.sopac.png.domain.enumeration.ServiceAccessibility;
import org.sopac.png.domain.enumeration.ServiceAvailibility;
import org.sopac.png.domain.enumeration.ServiceAccessibility;
import org.sopac.png.domain.enumeration.ServiceAvailibility;
import org.sopac.png.domain.enumeration.ServiceAccessibility;
import org.sopac.png.domain.enumeration.ServiceAvailibility;
import org.sopac.png.domain.enumeration.ServiceAccessibility;
import org.sopac.png.domain.enumeration.ServiceAvailibility;
import org.sopac.png.domain.enumeration.ServiceAccessibility;
import org.sopac.png.domain.enumeration.ServiceAvailibility;
import org.sopac.png.domain.enumeration.ServiceAccessibility;
import org.sopac.png.domain.enumeration.Livelihood;
import org.sopac.png.domain.enumeration.Talents;
/**
 * Integration tests for the {@Link DisabledIndividualResource} REST controller.
 */
@SpringBootTest(classes = PngDisabilityReligionApp.class)
public class DisabledIndividualResourceIT {

    private static final String DEFAULT_CIVIL_REGISTRATION_NO = "AAAAAAAAAA";
    private static final String UPDATED_CIVIL_REGISTRATION_NO = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final String DEFAULT_RELIGION = "AAAAAAAAAA";
    private static final String UPDATED_RELIGION = "BBBBBBBBBB";

    private static final String DEFAULT_OCCUPATION = "AAAAAAAAAA";
    private static final String UPDATED_OCCUPATION = "BBBBBBBBBB";

    private static final String DEFAULT_EMPLOYER = "AAAAAAAAAA";
    private static final String UPDATED_EMPLOYER = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_OF_BIRTH = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_OF_BIRTH = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final EstimateAge DEFAULT_ESTIMATED_AGE = EstimateAge.LESS_THAN_18;
    private static final EstimateAge UPDATED_ESTIMATED_AGE = EstimateAge.BETWEEN_18_AND_29;

    private static final MartialStatus DEFAULT_MARTIAL_STATUS = MartialStatus.MARRIED;
    private static final MartialStatus UPDATED_MARTIAL_STATUS = MartialStatus.WIDOW_WIDOWER;

    private static final Integer DEFAULT_NO_OF_CHILDREN_ALIVE = 1;
    private static final Integer UPDATED_NO_OF_CHILDREN_ALIVE = 2;

    private static final Integer DEFAULT_NO_OF_CHILDREN_DECEASED = 1;
    private static final Integer UPDATED_NO_OF_CHILDREN_DECEASED = 2;

    private static final Integer DEFAULT_NO_OF_CHILDREN_CONGENITAL = 1;
    private static final Integer UPDATED_NO_OF_CHILDREN_CONGENITAL = 2;

    private static final String DEFAULT_EDUCATION_FORMAL = "AAAAAAAAAA";
    private static final String UPDATED_EDUCATION_FORMAL = "BBBBBBBBBB";

    private static final String DEFAULT_EDUCATION_TRADE = "AAAAAAAAAA";
    private static final String UPDATED_EDUCATION_TRADE = "BBBBBBBBBB";

    private static final CarerRelationship DEFAULT_CARER_RELATIONSHIP = CarerRelationship.FAMILY_MEMBER;
    private static final CarerRelationship UPDATED_CARER_RELATIONSHIP = CarerRelationship.NON_FAMILY_MEMBER;

    private static final Difficulty DEFAULT_DIFFCULTY_SEEING = Difficulty.NO;
    private static final Difficulty UPDATED_DIFFCULTY_SEEING = Difficulty.SOME;

    private static final Difficulty DEFAULT_DIFFCULTY_HEARING = Difficulty.NO;
    private static final Difficulty UPDATED_DIFFCULTY_HEARING = Difficulty.SOME;

    private static final Difficulty DEFAULT_DIFFCULTY_WALKING = Difficulty.NO;
    private static final Difficulty UPDATED_DIFFCULTY_WALKING = Difficulty.SOME;

    private static final Difficulty DEFAULT_DIFFCULTY_MEMORY = Difficulty.NO;
    private static final Difficulty UPDATED_DIFFCULTY_MEMORY = Difficulty.SOME;

    private static final Difficulty DEFAULT_DIFFCULTY_SELF_CARE = Difficulty.NO;
    private static final Difficulty UPDATED_DIFFCULTY_SELF_CARE = Difficulty.SOME;

    private static final Difficulty DEFAULT_DIFFCULTY_COMMUNICATING = Difficulty.NO;
    private static final Difficulty UPDATED_DIFFCULTY_COMMUNICATING = Difficulty.SOME;

    private static final CauseOfDisability DEFAULT_CAUSE_OF_DISABILITY = CauseOfDisability.DISEASE;
    private static final CauseOfDisability UPDATED_CAUSE_OF_DISABILITY = CauseOfDisability.ACCIDENT;

    private static final ServiceAvailibility DEFAULT_HEALTH_SERVICE_AVAILIBILITY = ServiceAvailibility.YES_AVAILABLE;
    private static final ServiceAvailibility UPDATED_HEALTH_SERVICE_AVAILIBILITY = ServiceAvailibility.NOT_AVAILABLE;

    private static final ServiceAccessibility DEFAULT_HEALTH_SERVICE_ACCESSIBILITY = ServiceAccessibility.YES_ACCESSIBLE;
    private static final ServiceAccessibility UPDATED_HEALTH_SERVICE_ACCESSIBILITY = ServiceAccessibility.NOT_ACCESSIBLE;

    private static final ServiceAvailibility DEFAULT_EDUCATION_SERVICE_AVAILIBILITY = ServiceAvailibility.YES_AVAILABLE;
    private static final ServiceAvailibility UPDATED_EDUCATION_SERVICE_AVAILIBILITY = ServiceAvailibility.NOT_AVAILABLE;

    private static final ServiceAccessibility DEFAULT_EDUCATION_SERVICE_ACCESSIBILITY = ServiceAccessibility.YES_ACCESSIBLE;
    private static final ServiceAccessibility UPDATED_EDUCATION_SERVICE_ACCESSIBILITY = ServiceAccessibility.NOT_ACCESSIBLE;

    private static final ServiceAvailibility DEFAULT_LAW_SERVICE_AVAILIBILITY = ServiceAvailibility.YES_AVAILABLE;
    private static final ServiceAvailibility UPDATED_LAW_SERVICE_AVAILIBILITY = ServiceAvailibility.NOT_AVAILABLE;

    private static final ServiceAccessibility DEFAULT_LAW_SERVICE_ACCESSIBILITY = ServiceAccessibility.YES_ACCESSIBLE;
    private static final ServiceAccessibility UPDATED_LAW_SERVICE_ACCESSIBILITY = ServiceAccessibility.NOT_ACCESSIBLE;

    private static final ServiceAvailibility DEFAULT_WELFARE_SERVICE_AVAILIBILITY = ServiceAvailibility.YES_AVAILABLE;
    private static final ServiceAvailibility UPDATED_WELFARE_SERVICE_AVAILIBILITY = ServiceAvailibility.NOT_AVAILABLE;

    private static final ServiceAccessibility DEFAULT_WELFARE_SERVICE_ACCESSIBILITY = ServiceAccessibility.YES_ACCESSIBLE;
    private static final ServiceAccessibility UPDATED_WELFARE_SERVICE_ACCESSIBILITY = ServiceAccessibility.NOT_ACCESSIBLE;

    private static final ServiceAvailibility DEFAULT_SPORTS_SERVICE_AVAILIBILITY = ServiceAvailibility.YES_AVAILABLE;
    private static final ServiceAvailibility UPDATED_SPORTS_SERVICE_AVAILIBILITY = ServiceAvailibility.NOT_AVAILABLE;

    private static final ServiceAccessibility DEFAULT_SPORTS_SERVICE_ACCESSIBILITY = ServiceAccessibility.YES_ACCESSIBLE;
    private static final ServiceAccessibility UPDATED_SPORTS_SERVICE_ACCESSIBILITY = ServiceAccessibility.NOT_ACCESSIBLE;

    private static final ServiceAvailibility DEFAULT_ROAD_SERVICE_AVAILIBILITY = ServiceAvailibility.YES_AVAILABLE;
    private static final ServiceAvailibility UPDATED_ROAD_SERVICE_AVAILIBILITY = ServiceAvailibility.NOT_AVAILABLE;

    private static final ServiceAccessibility DEFAULT_ROAD_SERVICE_ACCESSIBILITY = ServiceAccessibility.YES_ACCESSIBLE;
    private static final ServiceAccessibility UPDATED_ROAD_SERVICE_ACCESSIBILITY = ServiceAccessibility.NOT_ACCESSIBLE;

    private static final ServiceAvailibility DEFAULT_MARKET_SERVICE_AVAILIBILITY = ServiceAvailibility.YES_AVAILABLE;
    private static final ServiceAvailibility UPDATED_MARKET_SERVICE_AVAILIBILITY = ServiceAvailibility.NOT_AVAILABLE;

    private static final ServiceAccessibility DEFAULT_MARKET_SERVICE_ACCESSIBILITY = ServiceAccessibility.YES_ACCESSIBLE;
    private static final ServiceAccessibility UPDATED_MARKET_SERVICE_ACCESSIBILITY = ServiceAccessibility.NOT_ACCESSIBLE;

    private static final ServiceAvailibility DEFAULT_BANKING_SERVICE_AVAILIBILITY = ServiceAvailibility.YES_AVAILABLE;
    private static final ServiceAvailibility UPDATED_BANKING_SERVICE_AVAILIBILITY = ServiceAvailibility.NOT_AVAILABLE;

    private static final ServiceAccessibility DEFAULT_BANKING_SERVICE_ACCESSIBILITY = ServiceAccessibility.YES_ACCESSIBLE;
    private static final ServiceAccessibility UPDATED_BANKING_SERVICE_ACCESSIBILITY = ServiceAccessibility.NOT_ACCESSIBLE;

    private static final ServiceAvailibility DEFAULT_TELE_COMMUNICATION_SERVICE_AVAILIBILITY = ServiceAvailibility.YES_AVAILABLE;
    private static final ServiceAvailibility UPDATED_TELE_COMMUNICATION_SERVICE_AVAILIBILITY = ServiceAvailibility.NOT_AVAILABLE;

    private static final ServiceAccessibility DEFAULT_TELE_COMMUNICATION_SERVICE_ACCESSIBILITY = ServiceAccessibility.YES_ACCESSIBLE;
    private static final ServiceAccessibility UPDATED_TELE_COMMUNICATION_SERVICE_ACCESSIBILITY = ServiceAccessibility.NOT_ACCESSIBLE;

    private static final ServiceAvailibility DEFAULT_CHURCH_SERVICE_AVAILIBILITY = ServiceAvailibility.YES_AVAILABLE;
    private static final ServiceAvailibility UPDATED_CHURCH_SERVICE_AVAILIBILITY = ServiceAvailibility.NOT_AVAILABLE;

    private static final ServiceAccessibility DEFAULT_CHURCH_SERVICE_ACCESSIBILITY = ServiceAccessibility.YES_ACCESSIBLE;
    private static final ServiceAccessibility UPDATED_CHURCH_SERVICE_ACCESSIBILITY = ServiceAccessibility.NOT_ACCESSIBLE;

    private static final Boolean DEFAULT_MEMBER_DISABLED_PERSONS_ORGANISATION = false;
    private static final Boolean UPDATED_MEMBER_DISABLED_PERSONS_ORGANISATION = true;

    private static final String DEFAULT_AWARENESS_ADVOCACY_PROGRAMME_DETAILS = "AAAAAAAAAA";
    private static final String UPDATED_AWARENESS_ADVOCACY_PROGRAMME_DETAILS = "BBBBBBBBBB";

    private static final Livelihood DEFAULT_LIVELIHOOD = Livelihood.EMPLOYED_FORM;
    private static final Livelihood UPDATED_LIVELIHOOD = Livelihood.EMPLOYED_INFORMAL;

    private static final String DEFAULT_LIVELIHOOD_DETAILS = "AAAAAAAAAA";
    private static final String UPDATED_LIVELIHOOD_DETAILS = "BBBBBBBBBB";

    private static final Talents DEFAULT_TALENTS = Talents.LEADERSHIP;
    private static final Talents UPDATED_TALENTS = Talents.SPORTS;

    @Autowired
    private DisabledIndividualRepository disabledIndividualRepository;

    /**
     * This repository is mocked in the org.sopac.png.repository.search test package.
     *
     * @see org.sopac.png.repository.search.DisabledIndividualSearchRepositoryMockConfiguration
     */
    @Autowired
    private DisabledIndividualSearchRepository mockDisabledIndividualSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDisabledIndividualMockMvc;

    private DisabledIndividual disabledIndividual;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DisabledIndividualResource disabledIndividualResource = new DisabledIndividualResource(disabledIndividualRepository, mockDisabledIndividualSearchRepository);
        this.restDisabledIndividualMockMvc = MockMvcBuilders.standaloneSetup(disabledIndividualResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DisabledIndividual createEntity(EntityManager em) {
        DisabledIndividual disabledIndividual = new DisabledIndividual()
            .civilRegistrationNo(DEFAULT_CIVIL_REGISTRATION_NO)
            .name(DEFAULT_NAME)
            .gender(DEFAULT_GENDER)
            .religion(DEFAULT_RELIGION)
            .occupation(DEFAULT_OCCUPATION)
            .employer(DEFAULT_EMPLOYER)
            .dateOfBirth(DEFAULT_DATE_OF_BIRTH)
            .estimatedAge(DEFAULT_ESTIMATED_AGE)
            .martialStatus(DEFAULT_MARTIAL_STATUS)
            .noOfChildrenAlive(DEFAULT_NO_OF_CHILDREN_ALIVE)
            .noOfChildrenDeceased(DEFAULT_NO_OF_CHILDREN_DECEASED)
            .noOfChildrenCongenital(DEFAULT_NO_OF_CHILDREN_CONGENITAL)
            .educationFormal(DEFAULT_EDUCATION_FORMAL)
            .educationTrade(DEFAULT_EDUCATION_TRADE)
            .carerRelationship(DEFAULT_CARER_RELATIONSHIP)
            .diffcultySeeing(DEFAULT_DIFFCULTY_SEEING)
            .diffcultyHearing(DEFAULT_DIFFCULTY_HEARING)
            .diffcultyWalking(DEFAULT_DIFFCULTY_WALKING)
            .diffcultyMemory(DEFAULT_DIFFCULTY_MEMORY)
            .diffcultySelfCare(DEFAULT_DIFFCULTY_SELF_CARE)
            .diffcultyCommunicating(DEFAULT_DIFFCULTY_COMMUNICATING)
            .causeOfDisability(DEFAULT_CAUSE_OF_DISABILITY)
            .healthServiceAvailibility(DEFAULT_HEALTH_SERVICE_AVAILIBILITY)
            .healthServiceAccessibility(DEFAULT_HEALTH_SERVICE_ACCESSIBILITY)
            .educationServiceAvailibility(DEFAULT_EDUCATION_SERVICE_AVAILIBILITY)
            .educationServiceAccessibility(DEFAULT_EDUCATION_SERVICE_ACCESSIBILITY)
            .lawServiceAvailibility(DEFAULT_LAW_SERVICE_AVAILIBILITY)
            .lawServiceAccessibility(DEFAULT_LAW_SERVICE_ACCESSIBILITY)
            .welfareServiceAvailibility(DEFAULT_WELFARE_SERVICE_AVAILIBILITY)
            .welfareServiceAccessibility(DEFAULT_WELFARE_SERVICE_ACCESSIBILITY)
            .sportsServiceAvailibility(DEFAULT_SPORTS_SERVICE_AVAILIBILITY)
            .sportsServiceAccessibility(DEFAULT_SPORTS_SERVICE_ACCESSIBILITY)
            .roadServiceAvailibility(DEFAULT_ROAD_SERVICE_AVAILIBILITY)
            .roadServiceAccessibility(DEFAULT_ROAD_SERVICE_ACCESSIBILITY)
            .marketServiceAvailibility(DEFAULT_MARKET_SERVICE_AVAILIBILITY)
            .marketServiceAccessibility(DEFAULT_MARKET_SERVICE_ACCESSIBILITY)
            .bankingServiceAvailibility(DEFAULT_BANKING_SERVICE_AVAILIBILITY)
            .bankingServiceAccessibility(DEFAULT_BANKING_SERVICE_ACCESSIBILITY)
            .teleCommunicationServiceAvailibility(DEFAULT_TELE_COMMUNICATION_SERVICE_AVAILIBILITY)
            .teleCommunicationServiceAccessibility(DEFAULT_TELE_COMMUNICATION_SERVICE_ACCESSIBILITY)
            .churchServiceAvailibility(DEFAULT_CHURCH_SERVICE_AVAILIBILITY)
            .churchServiceAccessibility(DEFAULT_CHURCH_SERVICE_ACCESSIBILITY)
            .memberDisabledPersonsOrganisation(DEFAULT_MEMBER_DISABLED_PERSONS_ORGANISATION)
            .awarenessAdvocacyProgrammeDetails(DEFAULT_AWARENESS_ADVOCACY_PROGRAMME_DETAILS)
            .livelihood(DEFAULT_LIVELIHOOD)
            .livelihoodDetails(DEFAULT_LIVELIHOOD_DETAILS)
            .talents(DEFAULT_TALENTS);
        return disabledIndividual;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DisabledIndividual createUpdatedEntity(EntityManager em) {
        DisabledIndividual disabledIndividual = new DisabledIndividual()
            .civilRegistrationNo(UPDATED_CIVIL_REGISTRATION_NO)
            .name(UPDATED_NAME)
            .gender(UPDATED_GENDER)
            .religion(UPDATED_RELIGION)
            .occupation(UPDATED_OCCUPATION)
            .employer(UPDATED_EMPLOYER)
            .dateOfBirth(UPDATED_DATE_OF_BIRTH)
            .estimatedAge(UPDATED_ESTIMATED_AGE)
            .martialStatus(UPDATED_MARTIAL_STATUS)
            .noOfChildrenAlive(UPDATED_NO_OF_CHILDREN_ALIVE)
            .noOfChildrenDeceased(UPDATED_NO_OF_CHILDREN_DECEASED)
            .noOfChildrenCongenital(UPDATED_NO_OF_CHILDREN_CONGENITAL)
            .educationFormal(UPDATED_EDUCATION_FORMAL)
            .educationTrade(UPDATED_EDUCATION_TRADE)
            .carerRelationship(UPDATED_CARER_RELATIONSHIP)
            .diffcultySeeing(UPDATED_DIFFCULTY_SEEING)
            .diffcultyHearing(UPDATED_DIFFCULTY_HEARING)
            .diffcultyWalking(UPDATED_DIFFCULTY_WALKING)
            .diffcultyMemory(UPDATED_DIFFCULTY_MEMORY)
            .diffcultySelfCare(UPDATED_DIFFCULTY_SELF_CARE)
            .diffcultyCommunicating(UPDATED_DIFFCULTY_COMMUNICATING)
            .causeOfDisability(UPDATED_CAUSE_OF_DISABILITY)
            .healthServiceAvailibility(UPDATED_HEALTH_SERVICE_AVAILIBILITY)
            .healthServiceAccessibility(UPDATED_HEALTH_SERVICE_ACCESSIBILITY)
            .educationServiceAvailibility(UPDATED_EDUCATION_SERVICE_AVAILIBILITY)
            .educationServiceAccessibility(UPDATED_EDUCATION_SERVICE_ACCESSIBILITY)
            .lawServiceAvailibility(UPDATED_LAW_SERVICE_AVAILIBILITY)
            .lawServiceAccessibility(UPDATED_LAW_SERVICE_ACCESSIBILITY)
            .welfareServiceAvailibility(UPDATED_WELFARE_SERVICE_AVAILIBILITY)
            .welfareServiceAccessibility(UPDATED_WELFARE_SERVICE_ACCESSIBILITY)
            .sportsServiceAvailibility(UPDATED_SPORTS_SERVICE_AVAILIBILITY)
            .sportsServiceAccessibility(UPDATED_SPORTS_SERVICE_ACCESSIBILITY)
            .roadServiceAvailibility(UPDATED_ROAD_SERVICE_AVAILIBILITY)
            .roadServiceAccessibility(UPDATED_ROAD_SERVICE_ACCESSIBILITY)
            .marketServiceAvailibility(UPDATED_MARKET_SERVICE_AVAILIBILITY)
            .marketServiceAccessibility(UPDATED_MARKET_SERVICE_ACCESSIBILITY)
            .bankingServiceAvailibility(UPDATED_BANKING_SERVICE_AVAILIBILITY)
            .bankingServiceAccessibility(UPDATED_BANKING_SERVICE_ACCESSIBILITY)
            .teleCommunicationServiceAvailibility(UPDATED_TELE_COMMUNICATION_SERVICE_AVAILIBILITY)
            .teleCommunicationServiceAccessibility(UPDATED_TELE_COMMUNICATION_SERVICE_ACCESSIBILITY)
            .churchServiceAvailibility(UPDATED_CHURCH_SERVICE_AVAILIBILITY)
            .churchServiceAccessibility(UPDATED_CHURCH_SERVICE_ACCESSIBILITY)
            .memberDisabledPersonsOrganisation(UPDATED_MEMBER_DISABLED_PERSONS_ORGANISATION)
            .awarenessAdvocacyProgrammeDetails(UPDATED_AWARENESS_ADVOCACY_PROGRAMME_DETAILS)
            .livelihood(UPDATED_LIVELIHOOD)
            .livelihoodDetails(UPDATED_LIVELIHOOD_DETAILS)
            .talents(UPDATED_TALENTS);
        return disabledIndividual;
    }

    @BeforeEach
    public void initTest() {
        disabledIndividual = createEntity(em);
    }

    @Test
    @Transactional
    public void createDisabledIndividual() throws Exception {
        int databaseSizeBeforeCreate = disabledIndividualRepository.findAll().size();

        // Create the DisabledIndividual
        restDisabledIndividualMockMvc.perform(post("/api/disabled-individuals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(disabledIndividual)))
            .andExpect(status().isCreated());

        // Validate the DisabledIndividual in the database
        List<DisabledIndividual> disabledIndividualList = disabledIndividualRepository.findAll();
        assertThat(disabledIndividualList).hasSize(databaseSizeBeforeCreate + 1);
        DisabledIndividual testDisabledIndividual = disabledIndividualList.get(disabledIndividualList.size() - 1);
        assertThat(testDisabledIndividual.getCivilRegistrationNo()).isEqualTo(DEFAULT_CIVIL_REGISTRATION_NO);
        assertThat(testDisabledIndividual.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDisabledIndividual.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testDisabledIndividual.getReligion()).isEqualTo(DEFAULT_RELIGION);
        assertThat(testDisabledIndividual.getOccupation()).isEqualTo(DEFAULT_OCCUPATION);
        assertThat(testDisabledIndividual.getEmployer()).isEqualTo(DEFAULT_EMPLOYER);
        assertThat(testDisabledIndividual.getDateOfBirth()).isEqualTo(DEFAULT_DATE_OF_BIRTH);
        assertThat(testDisabledIndividual.getEstimatedAge()).isEqualTo(DEFAULT_ESTIMATED_AGE);
        assertThat(testDisabledIndividual.getMartialStatus()).isEqualTo(DEFAULT_MARTIAL_STATUS);
        assertThat(testDisabledIndividual.getNoOfChildrenAlive()).isEqualTo(DEFAULT_NO_OF_CHILDREN_ALIVE);
        assertThat(testDisabledIndividual.getNoOfChildrenDeceased()).isEqualTo(DEFAULT_NO_OF_CHILDREN_DECEASED);
        assertThat(testDisabledIndividual.getNoOfChildrenCongenital()).isEqualTo(DEFAULT_NO_OF_CHILDREN_CONGENITAL);
        assertThat(testDisabledIndividual.getEducationFormal()).isEqualTo(DEFAULT_EDUCATION_FORMAL);
        assertThat(testDisabledIndividual.getEducationTrade()).isEqualTo(DEFAULT_EDUCATION_TRADE);
        assertThat(testDisabledIndividual.getCarerRelationship()).isEqualTo(DEFAULT_CARER_RELATIONSHIP);
        assertThat(testDisabledIndividual.getDiffcultySeeing()).isEqualTo(DEFAULT_DIFFCULTY_SEEING);
        assertThat(testDisabledIndividual.getDiffcultyHearing()).isEqualTo(DEFAULT_DIFFCULTY_HEARING);
        assertThat(testDisabledIndividual.getDiffcultyWalking()).isEqualTo(DEFAULT_DIFFCULTY_WALKING);
        assertThat(testDisabledIndividual.getDiffcultyMemory()).isEqualTo(DEFAULT_DIFFCULTY_MEMORY);
        assertThat(testDisabledIndividual.getDiffcultySelfCare()).isEqualTo(DEFAULT_DIFFCULTY_SELF_CARE);
        assertThat(testDisabledIndividual.getDiffcultyCommunicating()).isEqualTo(DEFAULT_DIFFCULTY_COMMUNICATING);
        assertThat(testDisabledIndividual.getCauseOfDisability()).isEqualTo(DEFAULT_CAUSE_OF_DISABILITY);
        assertThat(testDisabledIndividual.getHealthServiceAvailibility()).isEqualTo(DEFAULT_HEALTH_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getHealthServiceAccessibility()).isEqualTo(DEFAULT_HEALTH_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getEducationServiceAvailibility()).isEqualTo(DEFAULT_EDUCATION_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getEducationServiceAccessibility()).isEqualTo(DEFAULT_EDUCATION_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getLawServiceAvailibility()).isEqualTo(DEFAULT_LAW_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getLawServiceAccessibility()).isEqualTo(DEFAULT_LAW_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getWelfareServiceAvailibility()).isEqualTo(DEFAULT_WELFARE_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getWelfareServiceAccessibility()).isEqualTo(DEFAULT_WELFARE_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getSportsServiceAvailibility()).isEqualTo(DEFAULT_SPORTS_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getSportsServiceAccessibility()).isEqualTo(DEFAULT_SPORTS_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getRoadServiceAvailibility()).isEqualTo(DEFAULT_ROAD_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getRoadServiceAccessibility()).isEqualTo(DEFAULT_ROAD_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getMarketServiceAvailibility()).isEqualTo(DEFAULT_MARKET_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getMarketServiceAccessibility()).isEqualTo(DEFAULT_MARKET_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getBankingServiceAvailibility()).isEqualTo(DEFAULT_BANKING_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getBankingServiceAccessibility()).isEqualTo(DEFAULT_BANKING_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getTeleCommunicationServiceAvailibility()).isEqualTo(DEFAULT_TELE_COMMUNICATION_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getTeleCommunicationServiceAccessibility()).isEqualTo(DEFAULT_TELE_COMMUNICATION_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getChurchServiceAvailibility()).isEqualTo(DEFAULT_CHURCH_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getChurchServiceAccessibility()).isEqualTo(DEFAULT_CHURCH_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.isMemberDisabledPersonsOrganisation()).isEqualTo(DEFAULT_MEMBER_DISABLED_PERSONS_ORGANISATION);
        assertThat(testDisabledIndividual.getAwarenessAdvocacyProgrammeDetails()).isEqualTo(DEFAULT_AWARENESS_ADVOCACY_PROGRAMME_DETAILS);
        assertThat(testDisabledIndividual.getLivelihood()).isEqualTo(DEFAULT_LIVELIHOOD);
        assertThat(testDisabledIndividual.getLivelihoodDetails()).isEqualTo(DEFAULT_LIVELIHOOD_DETAILS);
        assertThat(testDisabledIndividual.getTalents()).isEqualTo(DEFAULT_TALENTS);

        // Validate the DisabledIndividual in Elasticsearch
        verify(mockDisabledIndividualSearchRepository, times(1)).save(testDisabledIndividual);
    }

    @Test
    @Transactional
    public void createDisabledIndividualWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = disabledIndividualRepository.findAll().size();

        // Create the DisabledIndividual with an existing ID
        disabledIndividual.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDisabledIndividualMockMvc.perform(post("/api/disabled-individuals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(disabledIndividual)))
            .andExpect(status().isBadRequest());

        // Validate the DisabledIndividual in the database
        List<DisabledIndividual> disabledIndividualList = disabledIndividualRepository.findAll();
        assertThat(disabledIndividualList).hasSize(databaseSizeBeforeCreate);

        // Validate the DisabledIndividual in Elasticsearch
        verify(mockDisabledIndividualSearchRepository, times(0)).save(disabledIndividual);
    }


    @Test
    @Transactional
    public void getAllDisabledIndividuals() throws Exception {
        // Initialize the database
        disabledIndividualRepository.saveAndFlush(disabledIndividual);

        // Get all the disabledIndividualList
        restDisabledIndividualMockMvc.perform(get("/api/disabled-individuals?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(disabledIndividual.getId().intValue())))
            .andExpect(jsonPath("$.[*].civilRegistrationNo").value(hasItem(DEFAULT_CIVIL_REGISTRATION_NO.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].religion").value(hasItem(DEFAULT_RELIGION.toString())))
            .andExpect(jsonPath("$.[*].occupation").value(hasItem(DEFAULT_OCCUPATION.toString())))
            .andExpect(jsonPath("$.[*].employer").value(hasItem(DEFAULT_EMPLOYER.toString())))
            .andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH.toString())))
            .andExpect(jsonPath("$.[*].estimatedAge").value(hasItem(DEFAULT_ESTIMATED_AGE.toString())))
            .andExpect(jsonPath("$.[*].martialStatus").value(hasItem(DEFAULT_MARTIAL_STATUS.toString())))
            .andExpect(jsonPath("$.[*].noOfChildrenAlive").value(hasItem(DEFAULT_NO_OF_CHILDREN_ALIVE)))
            .andExpect(jsonPath("$.[*].noOfChildrenDeceased").value(hasItem(DEFAULT_NO_OF_CHILDREN_DECEASED)))
            .andExpect(jsonPath("$.[*].noOfChildrenCongenital").value(hasItem(DEFAULT_NO_OF_CHILDREN_CONGENITAL)))
            .andExpect(jsonPath("$.[*].educationFormal").value(hasItem(DEFAULT_EDUCATION_FORMAL.toString())))
            .andExpect(jsonPath("$.[*].educationTrade").value(hasItem(DEFAULT_EDUCATION_TRADE.toString())))
            .andExpect(jsonPath("$.[*].carerRelationship").value(hasItem(DEFAULT_CARER_RELATIONSHIP.toString())))
            .andExpect(jsonPath("$.[*].diffcultySeeing").value(hasItem(DEFAULT_DIFFCULTY_SEEING.toString())))
            .andExpect(jsonPath("$.[*].diffcultyHearing").value(hasItem(DEFAULT_DIFFCULTY_HEARING.toString())))
            .andExpect(jsonPath("$.[*].diffcultyWalking").value(hasItem(DEFAULT_DIFFCULTY_WALKING.toString())))
            .andExpect(jsonPath("$.[*].diffcultyMemory").value(hasItem(DEFAULT_DIFFCULTY_MEMORY.toString())))
            .andExpect(jsonPath("$.[*].diffcultySelfCare").value(hasItem(DEFAULT_DIFFCULTY_SELF_CARE.toString())))
            .andExpect(jsonPath("$.[*].diffcultyCommunicating").value(hasItem(DEFAULT_DIFFCULTY_COMMUNICATING.toString())))
            .andExpect(jsonPath("$.[*].causeOfDisability").value(hasItem(DEFAULT_CAUSE_OF_DISABILITY.toString())))
            .andExpect(jsonPath("$.[*].healthServiceAvailibility").value(hasItem(DEFAULT_HEALTH_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].healthServiceAccessibility").value(hasItem(DEFAULT_HEALTH_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].educationServiceAvailibility").value(hasItem(DEFAULT_EDUCATION_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].educationServiceAccessibility").value(hasItem(DEFAULT_EDUCATION_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].lawServiceAvailibility").value(hasItem(DEFAULT_LAW_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].lawServiceAccessibility").value(hasItem(DEFAULT_LAW_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].welfareServiceAvailibility").value(hasItem(DEFAULT_WELFARE_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].welfareServiceAccessibility").value(hasItem(DEFAULT_WELFARE_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].sportsServiceAvailibility").value(hasItem(DEFAULT_SPORTS_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].sportsServiceAccessibility").value(hasItem(DEFAULT_SPORTS_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].roadServiceAvailibility").value(hasItem(DEFAULT_ROAD_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].roadServiceAccessibility").value(hasItem(DEFAULT_ROAD_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].marketServiceAvailibility").value(hasItem(DEFAULT_MARKET_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].marketServiceAccessibility").value(hasItem(DEFAULT_MARKET_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].bankingServiceAvailibility").value(hasItem(DEFAULT_BANKING_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].bankingServiceAccessibility").value(hasItem(DEFAULT_BANKING_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].teleCommunicationServiceAvailibility").value(hasItem(DEFAULT_TELE_COMMUNICATION_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].teleCommunicationServiceAccessibility").value(hasItem(DEFAULT_TELE_COMMUNICATION_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].churchServiceAvailibility").value(hasItem(DEFAULT_CHURCH_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].churchServiceAccessibility").value(hasItem(DEFAULT_CHURCH_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].memberDisabledPersonsOrganisation").value(hasItem(DEFAULT_MEMBER_DISABLED_PERSONS_ORGANISATION.booleanValue())))
            .andExpect(jsonPath("$.[*].awarenessAdvocacyProgrammeDetails").value(hasItem(DEFAULT_AWARENESS_ADVOCACY_PROGRAMME_DETAILS.toString())))
            .andExpect(jsonPath("$.[*].livelihood").value(hasItem(DEFAULT_LIVELIHOOD.toString())))
            .andExpect(jsonPath("$.[*].livelihoodDetails").value(hasItem(DEFAULT_LIVELIHOOD_DETAILS.toString())))
            .andExpect(jsonPath("$.[*].talents").value(hasItem(DEFAULT_TALENTS.toString())));
    }
    
    @Test
    @Transactional
    public void getDisabledIndividual() throws Exception {
        // Initialize the database
        disabledIndividualRepository.saveAndFlush(disabledIndividual);

        // Get the disabledIndividual
        restDisabledIndividualMockMvc.perform(get("/api/disabled-individuals/{id}", disabledIndividual.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(disabledIndividual.getId().intValue()))
            .andExpect(jsonPath("$.civilRegistrationNo").value(DEFAULT_CIVIL_REGISTRATION_NO.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.religion").value(DEFAULT_RELIGION.toString()))
            .andExpect(jsonPath("$.occupation").value(DEFAULT_OCCUPATION.toString()))
            .andExpect(jsonPath("$.employer").value(DEFAULT_EMPLOYER.toString()))
            .andExpect(jsonPath("$.dateOfBirth").value(DEFAULT_DATE_OF_BIRTH.toString()))
            .andExpect(jsonPath("$.estimatedAge").value(DEFAULT_ESTIMATED_AGE.toString()))
            .andExpect(jsonPath("$.martialStatus").value(DEFAULT_MARTIAL_STATUS.toString()))
            .andExpect(jsonPath("$.noOfChildrenAlive").value(DEFAULT_NO_OF_CHILDREN_ALIVE))
            .andExpect(jsonPath("$.noOfChildrenDeceased").value(DEFAULT_NO_OF_CHILDREN_DECEASED))
            .andExpect(jsonPath("$.noOfChildrenCongenital").value(DEFAULT_NO_OF_CHILDREN_CONGENITAL))
            .andExpect(jsonPath("$.educationFormal").value(DEFAULT_EDUCATION_FORMAL.toString()))
            .andExpect(jsonPath("$.educationTrade").value(DEFAULT_EDUCATION_TRADE.toString()))
            .andExpect(jsonPath("$.carerRelationship").value(DEFAULT_CARER_RELATIONSHIP.toString()))
            .andExpect(jsonPath("$.diffcultySeeing").value(DEFAULT_DIFFCULTY_SEEING.toString()))
            .andExpect(jsonPath("$.diffcultyHearing").value(DEFAULT_DIFFCULTY_HEARING.toString()))
            .andExpect(jsonPath("$.diffcultyWalking").value(DEFAULT_DIFFCULTY_WALKING.toString()))
            .andExpect(jsonPath("$.diffcultyMemory").value(DEFAULT_DIFFCULTY_MEMORY.toString()))
            .andExpect(jsonPath("$.diffcultySelfCare").value(DEFAULT_DIFFCULTY_SELF_CARE.toString()))
            .andExpect(jsonPath("$.diffcultyCommunicating").value(DEFAULT_DIFFCULTY_COMMUNICATING.toString()))
            .andExpect(jsonPath("$.causeOfDisability").value(DEFAULT_CAUSE_OF_DISABILITY.toString()))
            .andExpect(jsonPath("$.healthServiceAvailibility").value(DEFAULT_HEALTH_SERVICE_AVAILIBILITY.toString()))
            .andExpect(jsonPath("$.healthServiceAccessibility").value(DEFAULT_HEALTH_SERVICE_ACCESSIBILITY.toString()))
            .andExpect(jsonPath("$.educationServiceAvailibility").value(DEFAULT_EDUCATION_SERVICE_AVAILIBILITY.toString()))
            .andExpect(jsonPath("$.educationServiceAccessibility").value(DEFAULT_EDUCATION_SERVICE_ACCESSIBILITY.toString()))
            .andExpect(jsonPath("$.lawServiceAvailibility").value(DEFAULT_LAW_SERVICE_AVAILIBILITY.toString()))
            .andExpect(jsonPath("$.lawServiceAccessibility").value(DEFAULT_LAW_SERVICE_ACCESSIBILITY.toString()))
            .andExpect(jsonPath("$.welfareServiceAvailibility").value(DEFAULT_WELFARE_SERVICE_AVAILIBILITY.toString()))
            .andExpect(jsonPath("$.welfareServiceAccessibility").value(DEFAULT_WELFARE_SERVICE_ACCESSIBILITY.toString()))
            .andExpect(jsonPath("$.sportsServiceAvailibility").value(DEFAULT_SPORTS_SERVICE_AVAILIBILITY.toString()))
            .andExpect(jsonPath("$.sportsServiceAccessibility").value(DEFAULT_SPORTS_SERVICE_ACCESSIBILITY.toString()))
            .andExpect(jsonPath("$.roadServiceAvailibility").value(DEFAULT_ROAD_SERVICE_AVAILIBILITY.toString()))
            .andExpect(jsonPath("$.roadServiceAccessibility").value(DEFAULT_ROAD_SERVICE_ACCESSIBILITY.toString()))
            .andExpect(jsonPath("$.marketServiceAvailibility").value(DEFAULT_MARKET_SERVICE_AVAILIBILITY.toString()))
            .andExpect(jsonPath("$.marketServiceAccessibility").value(DEFAULT_MARKET_SERVICE_ACCESSIBILITY.toString()))
            .andExpect(jsonPath("$.bankingServiceAvailibility").value(DEFAULT_BANKING_SERVICE_AVAILIBILITY.toString()))
            .andExpect(jsonPath("$.bankingServiceAccessibility").value(DEFAULT_BANKING_SERVICE_ACCESSIBILITY.toString()))
            .andExpect(jsonPath("$.teleCommunicationServiceAvailibility").value(DEFAULT_TELE_COMMUNICATION_SERVICE_AVAILIBILITY.toString()))
            .andExpect(jsonPath("$.teleCommunicationServiceAccessibility").value(DEFAULT_TELE_COMMUNICATION_SERVICE_ACCESSIBILITY.toString()))
            .andExpect(jsonPath("$.churchServiceAvailibility").value(DEFAULT_CHURCH_SERVICE_AVAILIBILITY.toString()))
            .andExpect(jsonPath("$.churchServiceAccessibility").value(DEFAULT_CHURCH_SERVICE_ACCESSIBILITY.toString()))
            .andExpect(jsonPath("$.memberDisabledPersonsOrganisation").value(DEFAULT_MEMBER_DISABLED_PERSONS_ORGANISATION.booleanValue()))
            .andExpect(jsonPath("$.awarenessAdvocacyProgrammeDetails").value(DEFAULT_AWARENESS_ADVOCACY_PROGRAMME_DETAILS.toString()))
            .andExpect(jsonPath("$.livelihood").value(DEFAULT_LIVELIHOOD.toString()))
            .andExpect(jsonPath("$.livelihoodDetails").value(DEFAULT_LIVELIHOOD_DETAILS.toString()))
            .andExpect(jsonPath("$.talents").value(DEFAULT_TALENTS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDisabledIndividual() throws Exception {
        // Get the disabledIndividual
        restDisabledIndividualMockMvc.perform(get("/api/disabled-individuals/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDisabledIndividual() throws Exception {
        // Initialize the database
        disabledIndividualRepository.saveAndFlush(disabledIndividual);

        int databaseSizeBeforeUpdate = disabledIndividualRepository.findAll().size();

        // Update the disabledIndividual
        DisabledIndividual updatedDisabledIndividual = disabledIndividualRepository.findById(disabledIndividual.getId()).get();
        // Disconnect from session so that the updates on updatedDisabledIndividual are not directly saved in db
        em.detach(updatedDisabledIndividual);
        updatedDisabledIndividual
            .civilRegistrationNo(UPDATED_CIVIL_REGISTRATION_NO)
            .name(UPDATED_NAME)
            .gender(UPDATED_GENDER)
            .religion(UPDATED_RELIGION)
            .occupation(UPDATED_OCCUPATION)
            .employer(UPDATED_EMPLOYER)
            .dateOfBirth(UPDATED_DATE_OF_BIRTH)
            .estimatedAge(UPDATED_ESTIMATED_AGE)
            .martialStatus(UPDATED_MARTIAL_STATUS)
            .noOfChildrenAlive(UPDATED_NO_OF_CHILDREN_ALIVE)
            .noOfChildrenDeceased(UPDATED_NO_OF_CHILDREN_DECEASED)
            .noOfChildrenCongenital(UPDATED_NO_OF_CHILDREN_CONGENITAL)
            .educationFormal(UPDATED_EDUCATION_FORMAL)
            .educationTrade(UPDATED_EDUCATION_TRADE)
            .carerRelationship(UPDATED_CARER_RELATIONSHIP)
            .diffcultySeeing(UPDATED_DIFFCULTY_SEEING)
            .diffcultyHearing(UPDATED_DIFFCULTY_HEARING)
            .diffcultyWalking(UPDATED_DIFFCULTY_WALKING)
            .diffcultyMemory(UPDATED_DIFFCULTY_MEMORY)
            .diffcultySelfCare(UPDATED_DIFFCULTY_SELF_CARE)
            .diffcultyCommunicating(UPDATED_DIFFCULTY_COMMUNICATING)
            .causeOfDisability(UPDATED_CAUSE_OF_DISABILITY)
            .healthServiceAvailibility(UPDATED_HEALTH_SERVICE_AVAILIBILITY)
            .healthServiceAccessibility(UPDATED_HEALTH_SERVICE_ACCESSIBILITY)
            .educationServiceAvailibility(UPDATED_EDUCATION_SERVICE_AVAILIBILITY)
            .educationServiceAccessibility(UPDATED_EDUCATION_SERVICE_ACCESSIBILITY)
            .lawServiceAvailibility(UPDATED_LAW_SERVICE_AVAILIBILITY)
            .lawServiceAccessibility(UPDATED_LAW_SERVICE_ACCESSIBILITY)
            .welfareServiceAvailibility(UPDATED_WELFARE_SERVICE_AVAILIBILITY)
            .welfareServiceAccessibility(UPDATED_WELFARE_SERVICE_ACCESSIBILITY)
            .sportsServiceAvailibility(UPDATED_SPORTS_SERVICE_AVAILIBILITY)
            .sportsServiceAccessibility(UPDATED_SPORTS_SERVICE_ACCESSIBILITY)
            .roadServiceAvailibility(UPDATED_ROAD_SERVICE_AVAILIBILITY)
            .roadServiceAccessibility(UPDATED_ROAD_SERVICE_ACCESSIBILITY)
            .marketServiceAvailibility(UPDATED_MARKET_SERVICE_AVAILIBILITY)
            .marketServiceAccessibility(UPDATED_MARKET_SERVICE_ACCESSIBILITY)
            .bankingServiceAvailibility(UPDATED_BANKING_SERVICE_AVAILIBILITY)
            .bankingServiceAccessibility(UPDATED_BANKING_SERVICE_ACCESSIBILITY)
            .teleCommunicationServiceAvailibility(UPDATED_TELE_COMMUNICATION_SERVICE_AVAILIBILITY)
            .teleCommunicationServiceAccessibility(UPDATED_TELE_COMMUNICATION_SERVICE_ACCESSIBILITY)
            .churchServiceAvailibility(UPDATED_CHURCH_SERVICE_AVAILIBILITY)
            .churchServiceAccessibility(UPDATED_CHURCH_SERVICE_ACCESSIBILITY)
            .memberDisabledPersonsOrganisation(UPDATED_MEMBER_DISABLED_PERSONS_ORGANISATION)
            .awarenessAdvocacyProgrammeDetails(UPDATED_AWARENESS_ADVOCACY_PROGRAMME_DETAILS)
            .livelihood(UPDATED_LIVELIHOOD)
            .livelihoodDetails(UPDATED_LIVELIHOOD_DETAILS)
            .talents(UPDATED_TALENTS);

        restDisabledIndividualMockMvc.perform(put("/api/disabled-individuals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDisabledIndividual)))
            .andExpect(status().isOk());

        // Validate the DisabledIndividual in the database
        List<DisabledIndividual> disabledIndividualList = disabledIndividualRepository.findAll();
        assertThat(disabledIndividualList).hasSize(databaseSizeBeforeUpdate);
        DisabledIndividual testDisabledIndividual = disabledIndividualList.get(disabledIndividualList.size() - 1);
        assertThat(testDisabledIndividual.getCivilRegistrationNo()).isEqualTo(UPDATED_CIVIL_REGISTRATION_NO);
        assertThat(testDisabledIndividual.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDisabledIndividual.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testDisabledIndividual.getReligion()).isEqualTo(UPDATED_RELIGION);
        assertThat(testDisabledIndividual.getOccupation()).isEqualTo(UPDATED_OCCUPATION);
        assertThat(testDisabledIndividual.getEmployer()).isEqualTo(UPDATED_EMPLOYER);
        assertThat(testDisabledIndividual.getDateOfBirth()).isEqualTo(UPDATED_DATE_OF_BIRTH);
        assertThat(testDisabledIndividual.getEstimatedAge()).isEqualTo(UPDATED_ESTIMATED_AGE);
        assertThat(testDisabledIndividual.getMartialStatus()).isEqualTo(UPDATED_MARTIAL_STATUS);
        assertThat(testDisabledIndividual.getNoOfChildrenAlive()).isEqualTo(UPDATED_NO_OF_CHILDREN_ALIVE);
        assertThat(testDisabledIndividual.getNoOfChildrenDeceased()).isEqualTo(UPDATED_NO_OF_CHILDREN_DECEASED);
        assertThat(testDisabledIndividual.getNoOfChildrenCongenital()).isEqualTo(UPDATED_NO_OF_CHILDREN_CONGENITAL);
        assertThat(testDisabledIndividual.getEducationFormal()).isEqualTo(UPDATED_EDUCATION_FORMAL);
        assertThat(testDisabledIndividual.getEducationTrade()).isEqualTo(UPDATED_EDUCATION_TRADE);
        assertThat(testDisabledIndividual.getCarerRelationship()).isEqualTo(UPDATED_CARER_RELATIONSHIP);
        assertThat(testDisabledIndividual.getDiffcultySeeing()).isEqualTo(UPDATED_DIFFCULTY_SEEING);
        assertThat(testDisabledIndividual.getDiffcultyHearing()).isEqualTo(UPDATED_DIFFCULTY_HEARING);
        assertThat(testDisabledIndividual.getDiffcultyWalking()).isEqualTo(UPDATED_DIFFCULTY_WALKING);
        assertThat(testDisabledIndividual.getDiffcultyMemory()).isEqualTo(UPDATED_DIFFCULTY_MEMORY);
        assertThat(testDisabledIndividual.getDiffcultySelfCare()).isEqualTo(UPDATED_DIFFCULTY_SELF_CARE);
        assertThat(testDisabledIndividual.getDiffcultyCommunicating()).isEqualTo(UPDATED_DIFFCULTY_COMMUNICATING);
        assertThat(testDisabledIndividual.getCauseOfDisability()).isEqualTo(UPDATED_CAUSE_OF_DISABILITY);
        assertThat(testDisabledIndividual.getHealthServiceAvailibility()).isEqualTo(UPDATED_HEALTH_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getHealthServiceAccessibility()).isEqualTo(UPDATED_HEALTH_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getEducationServiceAvailibility()).isEqualTo(UPDATED_EDUCATION_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getEducationServiceAccessibility()).isEqualTo(UPDATED_EDUCATION_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getLawServiceAvailibility()).isEqualTo(UPDATED_LAW_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getLawServiceAccessibility()).isEqualTo(UPDATED_LAW_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getWelfareServiceAvailibility()).isEqualTo(UPDATED_WELFARE_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getWelfareServiceAccessibility()).isEqualTo(UPDATED_WELFARE_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getSportsServiceAvailibility()).isEqualTo(UPDATED_SPORTS_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getSportsServiceAccessibility()).isEqualTo(UPDATED_SPORTS_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getRoadServiceAvailibility()).isEqualTo(UPDATED_ROAD_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getRoadServiceAccessibility()).isEqualTo(UPDATED_ROAD_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getMarketServiceAvailibility()).isEqualTo(UPDATED_MARKET_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getMarketServiceAccessibility()).isEqualTo(UPDATED_MARKET_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getBankingServiceAvailibility()).isEqualTo(UPDATED_BANKING_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getBankingServiceAccessibility()).isEqualTo(UPDATED_BANKING_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getTeleCommunicationServiceAvailibility()).isEqualTo(UPDATED_TELE_COMMUNICATION_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getTeleCommunicationServiceAccessibility()).isEqualTo(UPDATED_TELE_COMMUNICATION_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.getChurchServiceAvailibility()).isEqualTo(UPDATED_CHURCH_SERVICE_AVAILIBILITY);
        assertThat(testDisabledIndividual.getChurchServiceAccessibility()).isEqualTo(UPDATED_CHURCH_SERVICE_ACCESSIBILITY);
        assertThat(testDisabledIndividual.isMemberDisabledPersonsOrganisation()).isEqualTo(UPDATED_MEMBER_DISABLED_PERSONS_ORGANISATION);
        assertThat(testDisabledIndividual.getAwarenessAdvocacyProgrammeDetails()).isEqualTo(UPDATED_AWARENESS_ADVOCACY_PROGRAMME_DETAILS);
        assertThat(testDisabledIndividual.getLivelihood()).isEqualTo(UPDATED_LIVELIHOOD);
        assertThat(testDisabledIndividual.getLivelihoodDetails()).isEqualTo(UPDATED_LIVELIHOOD_DETAILS);
        assertThat(testDisabledIndividual.getTalents()).isEqualTo(UPDATED_TALENTS);

        // Validate the DisabledIndividual in Elasticsearch
        verify(mockDisabledIndividualSearchRepository, times(1)).save(testDisabledIndividual);
    }

    @Test
    @Transactional
    public void updateNonExistingDisabledIndividual() throws Exception {
        int databaseSizeBeforeUpdate = disabledIndividualRepository.findAll().size();

        // Create the DisabledIndividual

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDisabledIndividualMockMvc.perform(put("/api/disabled-individuals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(disabledIndividual)))
            .andExpect(status().isBadRequest());

        // Validate the DisabledIndividual in the database
        List<DisabledIndividual> disabledIndividualList = disabledIndividualRepository.findAll();
        assertThat(disabledIndividualList).hasSize(databaseSizeBeforeUpdate);

        // Validate the DisabledIndividual in Elasticsearch
        verify(mockDisabledIndividualSearchRepository, times(0)).save(disabledIndividual);
    }

    @Test
    @Transactional
    public void deleteDisabledIndividual() throws Exception {
        // Initialize the database
        disabledIndividualRepository.saveAndFlush(disabledIndividual);

        int databaseSizeBeforeDelete = disabledIndividualRepository.findAll().size();

        // Delete the disabledIndividual
        restDisabledIndividualMockMvc.perform(delete("/api/disabled-individuals/{id}", disabledIndividual.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DisabledIndividual> disabledIndividualList = disabledIndividualRepository.findAll();
        assertThat(disabledIndividualList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the DisabledIndividual in Elasticsearch
        verify(mockDisabledIndividualSearchRepository, times(1)).deleteById(disabledIndividual.getId());
    }

    @Test
    @Transactional
    public void searchDisabledIndividual() throws Exception {
        // Initialize the database
        disabledIndividualRepository.saveAndFlush(disabledIndividual);
        when(mockDisabledIndividualSearchRepository.search(queryStringQuery("id:" + disabledIndividual.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(disabledIndividual), PageRequest.of(0, 1), 1));
        // Search the disabledIndividual
        restDisabledIndividualMockMvc.perform(get("/api/_search/disabled-individuals?query=id:" + disabledIndividual.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(disabledIndividual.getId().intValue())))
            .andExpect(jsonPath("$.[*].civilRegistrationNo").value(hasItem(DEFAULT_CIVIL_REGISTRATION_NO)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].religion").value(hasItem(DEFAULT_RELIGION)))
            .andExpect(jsonPath("$.[*].occupation").value(hasItem(DEFAULT_OCCUPATION)))
            .andExpect(jsonPath("$.[*].employer").value(hasItem(DEFAULT_EMPLOYER)))
            .andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH.toString())))
            .andExpect(jsonPath("$.[*].estimatedAge").value(hasItem(DEFAULT_ESTIMATED_AGE.toString())))
            .andExpect(jsonPath("$.[*].martialStatus").value(hasItem(DEFAULT_MARTIAL_STATUS.toString())))
            .andExpect(jsonPath("$.[*].noOfChildrenAlive").value(hasItem(DEFAULT_NO_OF_CHILDREN_ALIVE)))
            .andExpect(jsonPath("$.[*].noOfChildrenDeceased").value(hasItem(DEFAULT_NO_OF_CHILDREN_DECEASED)))
            .andExpect(jsonPath("$.[*].noOfChildrenCongenital").value(hasItem(DEFAULT_NO_OF_CHILDREN_CONGENITAL)))
            .andExpect(jsonPath("$.[*].educationFormal").value(hasItem(DEFAULT_EDUCATION_FORMAL)))
            .andExpect(jsonPath("$.[*].educationTrade").value(hasItem(DEFAULT_EDUCATION_TRADE)))
            .andExpect(jsonPath("$.[*].carerRelationship").value(hasItem(DEFAULT_CARER_RELATIONSHIP.toString())))
            .andExpect(jsonPath("$.[*].diffcultySeeing").value(hasItem(DEFAULT_DIFFCULTY_SEEING.toString())))
            .andExpect(jsonPath("$.[*].diffcultyHearing").value(hasItem(DEFAULT_DIFFCULTY_HEARING.toString())))
            .andExpect(jsonPath("$.[*].diffcultyWalking").value(hasItem(DEFAULT_DIFFCULTY_WALKING.toString())))
            .andExpect(jsonPath("$.[*].diffcultyMemory").value(hasItem(DEFAULT_DIFFCULTY_MEMORY.toString())))
            .andExpect(jsonPath("$.[*].diffcultySelfCare").value(hasItem(DEFAULT_DIFFCULTY_SELF_CARE.toString())))
            .andExpect(jsonPath("$.[*].diffcultyCommunicating").value(hasItem(DEFAULT_DIFFCULTY_COMMUNICATING.toString())))
            .andExpect(jsonPath("$.[*].causeOfDisability").value(hasItem(DEFAULT_CAUSE_OF_DISABILITY.toString())))
            .andExpect(jsonPath("$.[*].healthServiceAvailibility").value(hasItem(DEFAULT_HEALTH_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].healthServiceAccessibility").value(hasItem(DEFAULT_HEALTH_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].educationServiceAvailibility").value(hasItem(DEFAULT_EDUCATION_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].educationServiceAccessibility").value(hasItem(DEFAULT_EDUCATION_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].lawServiceAvailibility").value(hasItem(DEFAULT_LAW_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].lawServiceAccessibility").value(hasItem(DEFAULT_LAW_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].welfareServiceAvailibility").value(hasItem(DEFAULT_WELFARE_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].welfareServiceAccessibility").value(hasItem(DEFAULT_WELFARE_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].sportsServiceAvailibility").value(hasItem(DEFAULT_SPORTS_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].sportsServiceAccessibility").value(hasItem(DEFAULT_SPORTS_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].roadServiceAvailibility").value(hasItem(DEFAULT_ROAD_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].roadServiceAccessibility").value(hasItem(DEFAULT_ROAD_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].marketServiceAvailibility").value(hasItem(DEFAULT_MARKET_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].marketServiceAccessibility").value(hasItem(DEFAULT_MARKET_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].bankingServiceAvailibility").value(hasItem(DEFAULT_BANKING_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].bankingServiceAccessibility").value(hasItem(DEFAULT_BANKING_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].teleCommunicationServiceAvailibility").value(hasItem(DEFAULT_TELE_COMMUNICATION_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].teleCommunicationServiceAccessibility").value(hasItem(DEFAULT_TELE_COMMUNICATION_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].churchServiceAvailibility").value(hasItem(DEFAULT_CHURCH_SERVICE_AVAILIBILITY.toString())))
            .andExpect(jsonPath("$.[*].churchServiceAccessibility").value(hasItem(DEFAULT_CHURCH_SERVICE_ACCESSIBILITY.toString())))
            .andExpect(jsonPath("$.[*].memberDisabledPersonsOrganisation").value(hasItem(DEFAULT_MEMBER_DISABLED_PERSONS_ORGANISATION.booleanValue())))
            .andExpect(jsonPath("$.[*].awarenessAdvocacyProgrammeDetails").value(hasItem(DEFAULT_AWARENESS_ADVOCACY_PROGRAMME_DETAILS)))
            .andExpect(jsonPath("$.[*].livelihood").value(hasItem(DEFAULT_LIVELIHOOD.toString())))
            .andExpect(jsonPath("$.[*].livelihoodDetails").value(hasItem(DEFAULT_LIVELIHOOD_DETAILS)))
            .andExpect(jsonPath("$.[*].talents").value(hasItem(DEFAULT_TALENTS.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DisabledIndividual.class);
        DisabledIndividual disabledIndividual1 = new DisabledIndividual();
        disabledIndividual1.setId(1L);
        DisabledIndividual disabledIndividual2 = new DisabledIndividual();
        disabledIndividual2.setId(disabledIndividual1.getId());
        assertThat(disabledIndividual1).isEqualTo(disabledIndividual2);
        disabledIndividual2.setId(2L);
        assertThat(disabledIndividual1).isNotEqualTo(disabledIndividual2);
        disabledIndividual1.setId(null);
        assertThat(disabledIndividual1).isNotEqualTo(disabledIndividual2);
    }
}
