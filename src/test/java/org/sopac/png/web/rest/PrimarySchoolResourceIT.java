package org.sopac.png.web.rest;

import org.sopac.png.PngDisabilityReligionApp;
import org.sopac.png.domain.PrimarySchool;
import org.sopac.png.repository.PrimarySchoolRepository;
import org.sopac.png.repository.search.PrimarySchoolSearchRepository;
import org.sopac.png.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.sopac.png.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link PrimarySchoolResource} REST controller.
 */
@SpringBootTest(classes = PngDisabilityReligionApp.class)
public class PrimarySchoolResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCE = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCE = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_WARD = "AAAAAAAAAA";
    private static final String UPDATED_WARD = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private PrimarySchoolRepository primarySchoolRepository;

    /**
     * This repository is mocked in the org.sopac.png.repository.search test package.
     *
     * @see org.sopac.png.repository.search.PrimarySchoolSearchRepositoryMockConfiguration
     */
    @Autowired
    private PrimarySchoolSearchRepository mockPrimarySchoolSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPrimarySchoolMockMvc;

    private PrimarySchool primarySchool;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PrimarySchoolResource primarySchoolResource = new PrimarySchoolResource(primarySchoolRepository, mockPrimarySchoolSearchRepository);
        this.restPrimarySchoolMockMvc = MockMvcBuilders.standaloneSetup(primarySchoolResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PrimarySchool createEntity(EntityManager em) {
        PrimarySchool primarySchool = new PrimarySchool()
            .name(DEFAULT_NAME)
            .province(DEFAULT_PROVINCE)
            .district(DEFAULT_DISTRICT)
            .ward(DEFAULT_WARD)
            .active(DEFAULT_ACTIVE);
        return primarySchool;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PrimarySchool createUpdatedEntity(EntityManager em) {
        PrimarySchool primarySchool = new PrimarySchool()
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);
        return primarySchool;
    }

    @BeforeEach
    public void initTest() {
        primarySchool = createEntity(em);
    }

    @Test
    @Transactional
    public void createPrimarySchool() throws Exception {
        int databaseSizeBeforeCreate = primarySchoolRepository.findAll().size();

        // Create the PrimarySchool
        restPrimarySchoolMockMvc.perform(post("/api/primary-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primarySchool)))
            .andExpect(status().isCreated());

        // Validate the PrimarySchool in the database
        List<PrimarySchool> primarySchoolList = primarySchoolRepository.findAll();
        assertThat(primarySchoolList).hasSize(databaseSizeBeforeCreate + 1);
        PrimarySchool testPrimarySchool = primarySchoolList.get(primarySchoolList.size() - 1);
        assertThat(testPrimarySchool.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPrimarySchool.getProvince()).isEqualTo(DEFAULT_PROVINCE);
        assertThat(testPrimarySchool.getDistrict()).isEqualTo(DEFAULT_DISTRICT);
        assertThat(testPrimarySchool.getWard()).isEqualTo(DEFAULT_WARD);
        assertThat(testPrimarySchool.isActive()).isEqualTo(DEFAULT_ACTIVE);

        // Validate the PrimarySchool in Elasticsearch
        verify(mockPrimarySchoolSearchRepository, times(1)).save(testPrimarySchool);
    }

    @Test
    @Transactional
    public void createPrimarySchoolWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = primarySchoolRepository.findAll().size();

        // Create the PrimarySchool with an existing ID
        primarySchool.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPrimarySchoolMockMvc.perform(post("/api/primary-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primarySchool)))
            .andExpect(status().isBadRequest());

        // Validate the PrimarySchool in the database
        List<PrimarySchool> primarySchoolList = primarySchoolRepository.findAll();
        assertThat(primarySchoolList).hasSize(databaseSizeBeforeCreate);

        // Validate the PrimarySchool in Elasticsearch
        verify(mockPrimarySchoolSearchRepository, times(0)).save(primarySchool);
    }


    @Test
    @Transactional
    public void getAllPrimarySchools() throws Exception {
        // Initialize the database
        primarySchoolRepository.saveAndFlush(primarySchool);

        // Get all the primarySchoolList
        restPrimarySchoolMockMvc.perform(get("/api/primary-schools?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(primarySchool.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getPrimarySchool() throws Exception {
        // Initialize the database
        primarySchoolRepository.saveAndFlush(primarySchool);

        // Get the primarySchool
        restPrimarySchoolMockMvc.perform(get("/api/primary-schools/{id}", primarySchool.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(primarySchool.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.province").value(DEFAULT_PROVINCE.toString()))
            .andExpect(jsonPath("$.district").value(DEFAULT_DISTRICT.toString()))
            .andExpect(jsonPath("$.ward").value(DEFAULT_WARD.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPrimarySchool() throws Exception {
        // Get the primarySchool
        restPrimarySchoolMockMvc.perform(get("/api/primary-schools/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePrimarySchool() throws Exception {
        // Initialize the database
        primarySchoolRepository.saveAndFlush(primarySchool);

        int databaseSizeBeforeUpdate = primarySchoolRepository.findAll().size();

        // Update the primarySchool
        PrimarySchool updatedPrimarySchool = primarySchoolRepository.findById(primarySchool.getId()).get();
        // Disconnect from session so that the updates on updatedPrimarySchool are not directly saved in db
        em.detach(updatedPrimarySchool);
        updatedPrimarySchool
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);

        restPrimarySchoolMockMvc.perform(put("/api/primary-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPrimarySchool)))
            .andExpect(status().isOk());

        // Validate the PrimarySchool in the database
        List<PrimarySchool> primarySchoolList = primarySchoolRepository.findAll();
        assertThat(primarySchoolList).hasSize(databaseSizeBeforeUpdate);
        PrimarySchool testPrimarySchool = primarySchoolList.get(primarySchoolList.size() - 1);
        assertThat(testPrimarySchool.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPrimarySchool.getProvince()).isEqualTo(UPDATED_PROVINCE);
        assertThat(testPrimarySchool.getDistrict()).isEqualTo(UPDATED_DISTRICT);
        assertThat(testPrimarySchool.getWard()).isEqualTo(UPDATED_WARD);
        assertThat(testPrimarySchool.isActive()).isEqualTo(UPDATED_ACTIVE);

        // Validate the PrimarySchool in Elasticsearch
        verify(mockPrimarySchoolSearchRepository, times(1)).save(testPrimarySchool);
    }

    @Test
    @Transactional
    public void updateNonExistingPrimarySchool() throws Exception {
        int databaseSizeBeforeUpdate = primarySchoolRepository.findAll().size();

        // Create the PrimarySchool

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPrimarySchoolMockMvc.perform(put("/api/primary-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primarySchool)))
            .andExpect(status().isBadRequest());

        // Validate the PrimarySchool in the database
        List<PrimarySchool> primarySchoolList = primarySchoolRepository.findAll();
        assertThat(primarySchoolList).hasSize(databaseSizeBeforeUpdate);

        // Validate the PrimarySchool in Elasticsearch
        verify(mockPrimarySchoolSearchRepository, times(0)).save(primarySchool);
    }

    @Test
    @Transactional
    public void deletePrimarySchool() throws Exception {
        // Initialize the database
        primarySchoolRepository.saveAndFlush(primarySchool);

        int databaseSizeBeforeDelete = primarySchoolRepository.findAll().size();

        // Delete the primarySchool
        restPrimarySchoolMockMvc.perform(delete("/api/primary-schools/{id}", primarySchool.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PrimarySchool> primarySchoolList = primarySchoolRepository.findAll();
        assertThat(primarySchoolList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the PrimarySchool in Elasticsearch
        verify(mockPrimarySchoolSearchRepository, times(1)).deleteById(primarySchool.getId());
    }

    @Test
    @Transactional
    public void searchPrimarySchool() throws Exception {
        // Initialize the database
        primarySchoolRepository.saveAndFlush(primarySchool);
        when(mockPrimarySchoolSearchRepository.search(queryStringQuery("id:" + primarySchool.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(primarySchool), PageRequest.of(0, 1), 1));
        // Search the primarySchool
        restPrimarySchoolMockMvc.perform(get("/api/_search/primary-schools?query=id:" + primarySchool.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(primarySchool.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE)))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT)))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PrimarySchool.class);
        PrimarySchool primarySchool1 = new PrimarySchool();
        primarySchool1.setId(1L);
        PrimarySchool primarySchool2 = new PrimarySchool();
        primarySchool2.setId(primarySchool1.getId());
        assertThat(primarySchool1).isEqualTo(primarySchool2);
        primarySchool2.setId(2L);
        assertThat(primarySchool1).isNotEqualTo(primarySchool2);
        primarySchool1.setId(null);
        assertThat(primarySchool1).isNotEqualTo(primarySchool2);
    }
}
