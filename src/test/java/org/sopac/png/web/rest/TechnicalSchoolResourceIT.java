package org.sopac.png.web.rest;

import org.sopac.png.PngDisabilityReligionApp;
import org.sopac.png.domain.TechnicalSchool;
import org.sopac.png.repository.TechnicalSchoolRepository;
import org.sopac.png.repository.search.TechnicalSchoolSearchRepository;
import org.sopac.png.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.sopac.png.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link TechnicalSchoolResource} REST controller.
 */
@SpringBootTest(classes = PngDisabilityReligionApp.class)
public class TechnicalSchoolResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCE = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCE = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_WARD = "AAAAAAAAAA";
    private static final String UPDATED_WARD = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private TechnicalSchoolRepository technicalSchoolRepository;

    /**
     * This repository is mocked in the org.sopac.png.repository.search test package.
     *
     * @see org.sopac.png.repository.search.TechnicalSchoolSearchRepositoryMockConfiguration
     */
    @Autowired
    private TechnicalSchoolSearchRepository mockTechnicalSchoolSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTechnicalSchoolMockMvc;

    private TechnicalSchool technicalSchool;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TechnicalSchoolResource technicalSchoolResource = new TechnicalSchoolResource(technicalSchoolRepository, mockTechnicalSchoolSearchRepository);
        this.restTechnicalSchoolMockMvc = MockMvcBuilders.standaloneSetup(technicalSchoolResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TechnicalSchool createEntity(EntityManager em) {
        TechnicalSchool technicalSchool = new TechnicalSchool()
            .name(DEFAULT_NAME)
            .province(DEFAULT_PROVINCE)
            .district(DEFAULT_DISTRICT)
            .ward(DEFAULT_WARD)
            .active(DEFAULT_ACTIVE);
        return technicalSchool;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TechnicalSchool createUpdatedEntity(EntityManager em) {
        TechnicalSchool technicalSchool = new TechnicalSchool()
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);
        return technicalSchool;
    }

    @BeforeEach
    public void initTest() {
        technicalSchool = createEntity(em);
    }

    @Test
    @Transactional
    public void createTechnicalSchool() throws Exception {
        int databaseSizeBeforeCreate = technicalSchoolRepository.findAll().size();

        // Create the TechnicalSchool
        restTechnicalSchoolMockMvc.perform(post("/api/technical-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(technicalSchool)))
            .andExpect(status().isCreated());

        // Validate the TechnicalSchool in the database
        List<TechnicalSchool> technicalSchoolList = technicalSchoolRepository.findAll();
        assertThat(technicalSchoolList).hasSize(databaseSizeBeforeCreate + 1);
        TechnicalSchool testTechnicalSchool = technicalSchoolList.get(technicalSchoolList.size() - 1);
        assertThat(testTechnicalSchool.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTechnicalSchool.getProvince()).isEqualTo(DEFAULT_PROVINCE);
        assertThat(testTechnicalSchool.getDistrict()).isEqualTo(DEFAULT_DISTRICT);
        assertThat(testTechnicalSchool.getWard()).isEqualTo(DEFAULT_WARD);
        assertThat(testTechnicalSchool.isActive()).isEqualTo(DEFAULT_ACTIVE);

        // Validate the TechnicalSchool in Elasticsearch
        verify(mockTechnicalSchoolSearchRepository, times(1)).save(testTechnicalSchool);
    }

    @Test
    @Transactional
    public void createTechnicalSchoolWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = technicalSchoolRepository.findAll().size();

        // Create the TechnicalSchool with an existing ID
        technicalSchool.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTechnicalSchoolMockMvc.perform(post("/api/technical-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(technicalSchool)))
            .andExpect(status().isBadRequest());

        // Validate the TechnicalSchool in the database
        List<TechnicalSchool> technicalSchoolList = technicalSchoolRepository.findAll();
        assertThat(technicalSchoolList).hasSize(databaseSizeBeforeCreate);

        // Validate the TechnicalSchool in Elasticsearch
        verify(mockTechnicalSchoolSearchRepository, times(0)).save(technicalSchool);
    }


    @Test
    @Transactional
    public void getAllTechnicalSchools() throws Exception {
        // Initialize the database
        technicalSchoolRepository.saveAndFlush(technicalSchool);

        // Get all the technicalSchoolList
        restTechnicalSchoolMockMvc.perform(get("/api/technical-schools?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(technicalSchool.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getTechnicalSchool() throws Exception {
        // Initialize the database
        technicalSchoolRepository.saveAndFlush(technicalSchool);

        // Get the technicalSchool
        restTechnicalSchoolMockMvc.perform(get("/api/technical-schools/{id}", technicalSchool.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(technicalSchool.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.province").value(DEFAULT_PROVINCE.toString()))
            .andExpect(jsonPath("$.district").value(DEFAULT_DISTRICT.toString()))
            .andExpect(jsonPath("$.ward").value(DEFAULT_WARD.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTechnicalSchool() throws Exception {
        // Get the technicalSchool
        restTechnicalSchoolMockMvc.perform(get("/api/technical-schools/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTechnicalSchool() throws Exception {
        // Initialize the database
        technicalSchoolRepository.saveAndFlush(technicalSchool);

        int databaseSizeBeforeUpdate = technicalSchoolRepository.findAll().size();

        // Update the technicalSchool
        TechnicalSchool updatedTechnicalSchool = technicalSchoolRepository.findById(technicalSchool.getId()).get();
        // Disconnect from session so that the updates on updatedTechnicalSchool are not directly saved in db
        em.detach(updatedTechnicalSchool);
        updatedTechnicalSchool
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);

        restTechnicalSchoolMockMvc.perform(put("/api/technical-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTechnicalSchool)))
            .andExpect(status().isOk());

        // Validate the TechnicalSchool in the database
        List<TechnicalSchool> technicalSchoolList = technicalSchoolRepository.findAll();
        assertThat(technicalSchoolList).hasSize(databaseSizeBeforeUpdate);
        TechnicalSchool testTechnicalSchool = technicalSchoolList.get(technicalSchoolList.size() - 1);
        assertThat(testTechnicalSchool.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTechnicalSchool.getProvince()).isEqualTo(UPDATED_PROVINCE);
        assertThat(testTechnicalSchool.getDistrict()).isEqualTo(UPDATED_DISTRICT);
        assertThat(testTechnicalSchool.getWard()).isEqualTo(UPDATED_WARD);
        assertThat(testTechnicalSchool.isActive()).isEqualTo(UPDATED_ACTIVE);

        // Validate the TechnicalSchool in Elasticsearch
        verify(mockTechnicalSchoolSearchRepository, times(1)).save(testTechnicalSchool);
    }

    @Test
    @Transactional
    public void updateNonExistingTechnicalSchool() throws Exception {
        int databaseSizeBeforeUpdate = technicalSchoolRepository.findAll().size();

        // Create the TechnicalSchool

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTechnicalSchoolMockMvc.perform(put("/api/technical-schools")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(technicalSchool)))
            .andExpect(status().isBadRequest());

        // Validate the TechnicalSchool in the database
        List<TechnicalSchool> technicalSchoolList = technicalSchoolRepository.findAll();
        assertThat(technicalSchoolList).hasSize(databaseSizeBeforeUpdate);

        // Validate the TechnicalSchool in Elasticsearch
        verify(mockTechnicalSchoolSearchRepository, times(0)).save(technicalSchool);
    }

    @Test
    @Transactional
    public void deleteTechnicalSchool() throws Exception {
        // Initialize the database
        technicalSchoolRepository.saveAndFlush(technicalSchool);

        int databaseSizeBeforeDelete = technicalSchoolRepository.findAll().size();

        // Delete the technicalSchool
        restTechnicalSchoolMockMvc.perform(delete("/api/technical-schools/{id}", technicalSchool.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TechnicalSchool> technicalSchoolList = technicalSchoolRepository.findAll();
        assertThat(technicalSchoolList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the TechnicalSchool in Elasticsearch
        verify(mockTechnicalSchoolSearchRepository, times(1)).deleteById(technicalSchool.getId());
    }

    @Test
    @Transactional
    public void searchTechnicalSchool() throws Exception {
        // Initialize the database
        technicalSchoolRepository.saveAndFlush(technicalSchool);
        when(mockTechnicalSchoolSearchRepository.search(queryStringQuery("id:" + technicalSchool.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(technicalSchool), PageRequest.of(0, 1), 1));
        // Search the technicalSchool
        restTechnicalSchoolMockMvc.perform(get("/api/_search/technical-schools?query=id:" + technicalSchool.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(technicalSchool.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE)))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT)))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TechnicalSchool.class);
        TechnicalSchool technicalSchool1 = new TechnicalSchool();
        technicalSchool1.setId(1L);
        TechnicalSchool technicalSchool2 = new TechnicalSchool();
        technicalSchool2.setId(technicalSchool1.getId());
        assertThat(technicalSchool1).isEqualTo(technicalSchool2);
        technicalSchool2.setId(2L);
        assertThat(technicalSchool1).isNotEqualTo(technicalSchool2);
        technicalSchool1.setId(null);
        assertThat(technicalSchool1).isNotEqualTo(technicalSchool2);
    }
}
