package org.sopac.png.web.rest;

import org.sopac.png.PngDisabilityReligionApp;
import org.sopac.png.domain.OtherHealthService;
import org.sopac.png.repository.OtherHealthServiceRepository;
import org.sopac.png.repository.search.OtherHealthServiceSearchRepository;
import org.sopac.png.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.sopac.png.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link OtherHealthServiceResource} REST controller.
 */
@SpringBootTest(classes = PngDisabilityReligionApp.class)
public class OtherHealthServiceResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCE = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCE = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_WARD = "AAAAAAAAAA";
    private static final String UPDATED_WARD = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private OtherHealthServiceRepository otherHealthServiceRepository;

    /**
     * This repository is mocked in the org.sopac.png.repository.search test package.
     *
     * @see org.sopac.png.repository.search.OtherHealthServiceSearchRepositoryMockConfiguration
     */
    @Autowired
    private OtherHealthServiceSearchRepository mockOtherHealthServiceSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOtherHealthServiceMockMvc;

    private OtherHealthService otherHealthService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OtherHealthServiceResource otherHealthServiceResource = new OtherHealthServiceResource(otherHealthServiceRepository, mockOtherHealthServiceSearchRepository);
        this.restOtherHealthServiceMockMvc = MockMvcBuilders.standaloneSetup(otherHealthServiceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherHealthService createEntity(EntityManager em) {
        OtherHealthService otherHealthService = new OtherHealthService()
            .name(DEFAULT_NAME)
            .province(DEFAULT_PROVINCE)
            .district(DEFAULT_DISTRICT)
            .ward(DEFAULT_WARD)
            .active(DEFAULT_ACTIVE);
        return otherHealthService;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherHealthService createUpdatedEntity(EntityManager em) {
        OtherHealthService otherHealthService = new OtherHealthService()
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);
        return otherHealthService;
    }

    @BeforeEach
    public void initTest() {
        otherHealthService = createEntity(em);
    }

    @Test
    @Transactional
    public void createOtherHealthService() throws Exception {
        int databaseSizeBeforeCreate = otherHealthServiceRepository.findAll().size();

        // Create the OtherHealthService
        restOtherHealthServiceMockMvc.perform(post("/api/other-health-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otherHealthService)))
            .andExpect(status().isCreated());

        // Validate the OtherHealthService in the database
        List<OtherHealthService> otherHealthServiceList = otherHealthServiceRepository.findAll();
        assertThat(otherHealthServiceList).hasSize(databaseSizeBeforeCreate + 1);
        OtherHealthService testOtherHealthService = otherHealthServiceList.get(otherHealthServiceList.size() - 1);
        assertThat(testOtherHealthService.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testOtherHealthService.getProvince()).isEqualTo(DEFAULT_PROVINCE);
        assertThat(testOtherHealthService.getDistrict()).isEqualTo(DEFAULT_DISTRICT);
        assertThat(testOtherHealthService.getWard()).isEqualTo(DEFAULT_WARD);
        assertThat(testOtherHealthService.isActive()).isEqualTo(DEFAULT_ACTIVE);

        // Validate the OtherHealthService in Elasticsearch
        verify(mockOtherHealthServiceSearchRepository, times(1)).save(testOtherHealthService);
    }

    @Test
    @Transactional
    public void createOtherHealthServiceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = otherHealthServiceRepository.findAll().size();

        // Create the OtherHealthService with an existing ID
        otherHealthService.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOtherHealthServiceMockMvc.perform(post("/api/other-health-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otherHealthService)))
            .andExpect(status().isBadRequest());

        // Validate the OtherHealthService in the database
        List<OtherHealthService> otherHealthServiceList = otherHealthServiceRepository.findAll();
        assertThat(otherHealthServiceList).hasSize(databaseSizeBeforeCreate);

        // Validate the OtherHealthService in Elasticsearch
        verify(mockOtherHealthServiceSearchRepository, times(0)).save(otherHealthService);
    }


    @Test
    @Transactional
    public void getAllOtherHealthServices() throws Exception {
        // Initialize the database
        otherHealthServiceRepository.saveAndFlush(otherHealthService);

        // Get all the otherHealthServiceList
        restOtherHealthServiceMockMvc.perform(get("/api/other-health-services?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otherHealthService.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getOtherHealthService() throws Exception {
        // Initialize the database
        otherHealthServiceRepository.saveAndFlush(otherHealthService);

        // Get the otherHealthService
        restOtherHealthServiceMockMvc.perform(get("/api/other-health-services/{id}", otherHealthService.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(otherHealthService.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.province").value(DEFAULT_PROVINCE.toString()))
            .andExpect(jsonPath("$.district").value(DEFAULT_DISTRICT.toString()))
            .andExpect(jsonPath("$.ward").value(DEFAULT_WARD.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingOtherHealthService() throws Exception {
        // Get the otherHealthService
        restOtherHealthServiceMockMvc.perform(get("/api/other-health-services/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOtherHealthService() throws Exception {
        // Initialize the database
        otherHealthServiceRepository.saveAndFlush(otherHealthService);

        int databaseSizeBeforeUpdate = otherHealthServiceRepository.findAll().size();

        // Update the otherHealthService
        OtherHealthService updatedOtherHealthService = otherHealthServiceRepository.findById(otherHealthService.getId()).get();
        // Disconnect from session so that the updates on updatedOtherHealthService are not directly saved in db
        em.detach(updatedOtherHealthService);
        updatedOtherHealthService
            .name(UPDATED_NAME)
            .province(UPDATED_PROVINCE)
            .district(UPDATED_DISTRICT)
            .ward(UPDATED_WARD)
            .active(UPDATED_ACTIVE);

        restOtherHealthServiceMockMvc.perform(put("/api/other-health-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOtherHealthService)))
            .andExpect(status().isOk());

        // Validate the OtherHealthService in the database
        List<OtherHealthService> otherHealthServiceList = otherHealthServiceRepository.findAll();
        assertThat(otherHealthServiceList).hasSize(databaseSizeBeforeUpdate);
        OtherHealthService testOtherHealthService = otherHealthServiceList.get(otherHealthServiceList.size() - 1);
        assertThat(testOtherHealthService.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testOtherHealthService.getProvince()).isEqualTo(UPDATED_PROVINCE);
        assertThat(testOtherHealthService.getDistrict()).isEqualTo(UPDATED_DISTRICT);
        assertThat(testOtherHealthService.getWard()).isEqualTo(UPDATED_WARD);
        assertThat(testOtherHealthService.isActive()).isEqualTo(UPDATED_ACTIVE);

        // Validate the OtherHealthService in Elasticsearch
        verify(mockOtherHealthServiceSearchRepository, times(1)).save(testOtherHealthService);
    }

    @Test
    @Transactional
    public void updateNonExistingOtherHealthService() throws Exception {
        int databaseSizeBeforeUpdate = otherHealthServiceRepository.findAll().size();

        // Create the OtherHealthService

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherHealthServiceMockMvc.perform(put("/api/other-health-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otherHealthService)))
            .andExpect(status().isBadRequest());

        // Validate the OtherHealthService in the database
        List<OtherHealthService> otherHealthServiceList = otherHealthServiceRepository.findAll();
        assertThat(otherHealthServiceList).hasSize(databaseSizeBeforeUpdate);

        // Validate the OtherHealthService in Elasticsearch
        verify(mockOtherHealthServiceSearchRepository, times(0)).save(otherHealthService);
    }

    @Test
    @Transactional
    public void deleteOtherHealthService() throws Exception {
        // Initialize the database
        otherHealthServiceRepository.saveAndFlush(otherHealthService);

        int databaseSizeBeforeDelete = otherHealthServiceRepository.findAll().size();

        // Delete the otherHealthService
        restOtherHealthServiceMockMvc.perform(delete("/api/other-health-services/{id}", otherHealthService.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OtherHealthService> otherHealthServiceList = otherHealthServiceRepository.findAll();
        assertThat(otherHealthServiceList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the OtherHealthService in Elasticsearch
        verify(mockOtherHealthServiceSearchRepository, times(1)).deleteById(otherHealthService.getId());
    }

    @Test
    @Transactional
    public void searchOtherHealthService() throws Exception {
        // Initialize the database
        otherHealthServiceRepository.saveAndFlush(otherHealthService);
        when(mockOtherHealthServiceSearchRepository.search(queryStringQuery("id:" + otherHealthService.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(otherHealthService), PageRequest.of(0, 1), 1));
        // Search the otherHealthService
        restOtherHealthServiceMockMvc.perform(get("/api/_search/other-health-services?query=id:" + otherHealthService.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otherHealthService.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE)))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT)))
            .andExpect(jsonPath("$.[*].ward").value(hasItem(DEFAULT_WARD)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherHealthService.class);
        OtherHealthService otherHealthService1 = new OtherHealthService();
        otherHealthService1.setId(1L);
        OtherHealthService otherHealthService2 = new OtherHealthService();
        otherHealthService2.setId(otherHealthService1.getId());
        assertThat(otherHealthService1).isEqualTo(otherHealthService2);
        otherHealthService2.setId(2L);
        assertThat(otherHealthService1).isNotEqualTo(otherHealthService2);
        otherHealthService1.setId(null);
        assertThat(otherHealthService1).isNotEqualTo(otherHealthService2);
    }
}
