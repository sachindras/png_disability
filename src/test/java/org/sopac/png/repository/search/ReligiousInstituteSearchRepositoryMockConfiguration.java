package org.sopac.png.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link ReligiousInstituteSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class ReligiousInstituteSearchRepositoryMockConfiguration {

    @MockBean
    private ReligiousInstituteSearchRepository mockReligiousInstituteSearchRepository;

}
