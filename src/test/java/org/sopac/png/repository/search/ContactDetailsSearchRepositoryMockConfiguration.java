package org.sopac.png.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link ContactDetailsSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class ContactDetailsSearchRepositoryMockConfiguration {

    @MockBean
    private ContactDetailsSearchRepository mockContactDetailsSearchRepository;

}
