package org.sopac.png.repository;

import org.sopac.png.domain.OtherHealthService;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OtherHealthService entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OtherHealthServiceRepository extends JpaRepository<OtherHealthService, Long> {

}
