package org.sopac.png.repository;

import org.sopac.png.domain.PrimarySchool;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PrimarySchool entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PrimarySchoolRepository extends JpaRepository<PrimarySchool, Long> {

}
