package org.sopac.png.repository;

import org.sopac.png.domain.HealthCentre;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the HealthCentre entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HealthCentreRepository extends JpaRepository<HealthCentre, Long> {

}
