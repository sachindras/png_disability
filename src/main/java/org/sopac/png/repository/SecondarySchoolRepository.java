package org.sopac.png.repository;

import org.sopac.png.domain.SecondarySchool;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SecondarySchool entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SecondarySchoolRepository extends JpaRepository<SecondarySchool, Long> {

}
