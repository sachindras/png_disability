package org.sopac.png.repository;

import org.sopac.png.domain.TechnicalSchool;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TechnicalSchool entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TechnicalSchoolRepository extends JpaRepository<TechnicalSchool, Long> {

}
