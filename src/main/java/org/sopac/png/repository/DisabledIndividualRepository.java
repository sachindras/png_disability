package org.sopac.png.repository;

import org.sopac.png.domain.DisabledIndividual;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DisabledIndividual entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DisabledIndividualRepository extends JpaRepository<DisabledIndividual, Long> {

}
