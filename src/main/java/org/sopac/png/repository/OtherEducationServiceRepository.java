package org.sopac.png.repository;

import org.sopac.png.domain.OtherEducationService;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OtherEducationService entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OtherEducationServiceRepository extends JpaRepository<OtherEducationService, Long> {

}
