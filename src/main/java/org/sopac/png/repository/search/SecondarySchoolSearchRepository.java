package org.sopac.png.repository.search;

import org.sopac.png.domain.SecondarySchool;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link SecondarySchool} entity.
 */
public interface SecondarySchoolSearchRepository extends ElasticsearchRepository<SecondarySchool, Long> {
}
