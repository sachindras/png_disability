package org.sopac.png.repository.search;

import org.sopac.png.domain.PrimarySchool;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link PrimarySchool} entity.
 */
public interface PrimarySchoolSearchRepository extends ElasticsearchRepository<PrimarySchool, Long> {
}
