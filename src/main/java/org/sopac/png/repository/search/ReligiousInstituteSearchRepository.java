package org.sopac.png.repository.search;

import org.sopac.png.domain.ReligiousInstitute;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ReligiousInstitute} entity.
 */
public interface ReligiousInstituteSearchRepository extends ElasticsearchRepository<ReligiousInstitute, Long> {
}
