package org.sopac.png.repository.search;

import org.sopac.png.domain.HealthCentre;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link HealthCentre} entity.
 */
public interface HealthCentreSearchRepository extends ElasticsearchRepository<HealthCentre, Long> {
}
