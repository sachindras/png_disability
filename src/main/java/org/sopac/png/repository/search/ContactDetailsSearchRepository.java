package org.sopac.png.repository.search;

import org.sopac.png.domain.ContactDetails;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ContactDetails} entity.
 */
public interface ContactDetailsSearchRepository extends ElasticsearchRepository<ContactDetails, Long> {
}
