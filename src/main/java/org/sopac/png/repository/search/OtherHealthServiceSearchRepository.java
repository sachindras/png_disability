package org.sopac.png.repository.search;

import org.sopac.png.domain.OtherHealthService;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link OtherHealthService} entity.
 */
public interface OtherHealthServiceSearchRepository extends ElasticsearchRepository<OtherHealthService, Long> {
}
