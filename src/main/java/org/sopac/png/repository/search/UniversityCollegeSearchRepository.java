package org.sopac.png.repository.search;

import org.sopac.png.domain.UniversityCollege;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link UniversityCollege} entity.
 */
public interface UniversityCollegeSearchRepository extends ElasticsearchRepository<UniversityCollege, Long> {
}
