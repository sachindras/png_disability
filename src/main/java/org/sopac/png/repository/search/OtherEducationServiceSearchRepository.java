package org.sopac.png.repository.search;

import org.sopac.png.domain.OtherEducationService;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link OtherEducationService} entity.
 */
public interface OtherEducationServiceSearchRepository extends ElasticsearchRepository<OtherEducationService, Long> {
}
