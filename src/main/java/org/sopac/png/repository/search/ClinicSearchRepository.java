package org.sopac.png.repository.search;

import org.sopac.png.domain.Clinic;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Clinic} entity.
 */
public interface ClinicSearchRepository extends ElasticsearchRepository<Clinic, Long> {
}
