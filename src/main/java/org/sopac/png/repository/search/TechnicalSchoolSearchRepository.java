package org.sopac.png.repository.search;

import org.sopac.png.domain.TechnicalSchool;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link TechnicalSchool} entity.
 */
public interface TechnicalSchoolSearchRepository extends ElasticsearchRepository<TechnicalSchool, Long> {
}
