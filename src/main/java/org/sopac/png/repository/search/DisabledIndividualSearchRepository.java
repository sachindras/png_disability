package org.sopac.png.repository.search;

import org.sopac.png.domain.DisabledIndividual;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link DisabledIndividual} entity.
 */
public interface DisabledIndividualSearchRepository extends ElasticsearchRepository<DisabledIndividual, Long> {
}
