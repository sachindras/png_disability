package org.sopac.png.repository.search;

import org.sopac.png.domain.ChurchLeader;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ChurchLeader} entity.
 */
public interface ChurchLeaderSearchRepository extends ElasticsearchRepository<ChurchLeader, Long> {
}
