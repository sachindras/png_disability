package org.sopac.png.repository;

import org.sopac.png.domain.UniversityCollege;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UniversityCollege entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UniversityCollegeRepository extends JpaRepository<UniversityCollege, Long> {

}
