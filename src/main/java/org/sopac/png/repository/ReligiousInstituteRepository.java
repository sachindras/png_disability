package org.sopac.png.repository;

import org.sopac.png.domain.ReligiousInstitute;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ReligiousInstitute entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReligiousInstituteRepository extends JpaRepository<ReligiousInstitute, Long> {

}
