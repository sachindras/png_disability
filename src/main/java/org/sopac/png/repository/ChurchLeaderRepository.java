package org.sopac.png.repository;

import org.sopac.png.domain.ChurchLeader;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ChurchLeader entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChurchLeaderRepository extends JpaRepository<ChurchLeader, Long> {

}
