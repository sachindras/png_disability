package org.sopac.png.domain.enumeration;

/**
 * The CarerRelationship enumeration.
 */
public enum CarerRelationship {
    FAMILY_MEMBER, NON_FAMILY_MEMBER, FRIEND
}
