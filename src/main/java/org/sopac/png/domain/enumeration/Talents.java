package org.sopac.png.domain.enumeration;

/**
 * The Talents enumeration.
 */
public enum Talents {
    LEADERSHIP, SPORTS, MUSIC, SIGN_LANGUAGE, HAND_CRAFTING, SCREEN_PAINTING, TECHNICAL_SKILLS, MECHANICAL_SKILLS
}
