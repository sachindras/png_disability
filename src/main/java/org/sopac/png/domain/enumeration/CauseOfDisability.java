package org.sopac.png.domain.enumeration;

/**
 * The CauseOfDisability enumeration.
 */
public enum CauseOfDisability {
    DISEASE, ACCIDENT, SUBSTANCE_ABUSE, PHYSICAL_ABUSE, OTHERS
}
