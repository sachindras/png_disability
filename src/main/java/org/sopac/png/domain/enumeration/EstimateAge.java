package org.sopac.png.domain.enumeration;

/**
 * The EstimateAge enumeration.
 */
public enum EstimateAge {
    LESS_THAN_18, BETWEEN_18_AND_29, BETWEEN_30_AND_39, BETWEEN_40_AND_49, BETWEEN_50_AND_59, BETWEEN_60_AND_64, ABOVE_65
}
