package org.sopac.png.domain.enumeration;

/**
 * The Livelihood enumeration.
 */
public enum Livelihood {
    EMPLOYED_FORM, EMPLOYED_INFORMAL, SELF_EMPLOYED, SUBSISTENCE_FARMING, CHARITY, BEGGING, DEPEDNDENT
}
