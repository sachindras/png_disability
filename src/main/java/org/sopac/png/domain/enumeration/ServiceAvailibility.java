package org.sopac.png.domain.enumeration;

/**
 * The ServiceAvailibility enumeration.
 */
public enum ServiceAvailibility {
    YES_AVAILABLE, NOT_AVAILABLE
}
