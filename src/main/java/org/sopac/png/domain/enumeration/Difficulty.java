package org.sopac.png.domain.enumeration;

/**
 * The Difficulty enumeration.
 */
public enum Difficulty {
    NO, SOME, LOT, ENTIRELY
}
