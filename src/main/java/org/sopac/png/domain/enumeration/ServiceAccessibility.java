package org.sopac.png.domain.enumeration;

/**
 * The ServiceAccessibility enumeration.
 */
public enum ServiceAccessibility {
    YES_ACCESSIBLE, NOT_ACCESSIBLE
}
