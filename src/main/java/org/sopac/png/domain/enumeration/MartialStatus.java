package org.sopac.png.domain.enumeration;

/**
 * The MartialStatus enumeration.
 */
public enum MartialStatus {
    MARRIED, WIDOW_WIDOWER, DEFACTO, DIVORCED, SEPARATED, SINGLE
}
