package org.sopac.png.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.Instant;

import org.sopac.png.domain.enumeration.Gender;

import org.sopac.png.domain.enumeration.EstimateAge;

import org.sopac.png.domain.enumeration.MartialStatus;

import org.sopac.png.domain.enumeration.CarerRelationship;

import org.sopac.png.domain.enumeration.Difficulty;

import org.sopac.png.domain.enumeration.CauseOfDisability;

import org.sopac.png.domain.enumeration.ServiceAvailibility;

import org.sopac.png.domain.enumeration.ServiceAccessibility;

import org.sopac.png.domain.enumeration.Livelihood;

import org.sopac.png.domain.enumeration.Talents;

/**
 * A DisabledIndividual.
 */
@Entity
@Table(name = "disabled_individual")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "disabledindividual")
public class DisabledIndividual implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "civil_registration_no")
    private String civilRegistrationNo;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Column(name = "religion")
    private String religion;

    @Column(name = "occupation")
    private String occupation;

    @Column(name = "employer")
    private String employer;

    @Column(name = "date_of_birth")
    private Instant dateOfBirth;

    @Enumerated(EnumType.STRING)
    @Column(name = "estimated_age")
    private EstimateAge estimatedAge;

    @Enumerated(EnumType.STRING)
    @Column(name = "martial_status")
    private MartialStatus martialStatus;

    @Column(name = "no_of_children_alive")
    private Integer noOfChildrenAlive;

    @Column(name = "no_of_children_deceased")
    private Integer noOfChildrenDeceased;

    @Column(name = "no_of_children_congenital")
    private Integer noOfChildrenCongenital;

    @Column(name = "education_formal")
    private String educationFormal;

    @Column(name = "education_trade")
    private String educationTrade;

    @Enumerated(EnumType.STRING)
    @Column(name = "carer_relationship")
    private CarerRelationship carerRelationship;

    @Enumerated(EnumType.STRING)
    @Column(name = "diffculty_seeing")
    private Difficulty diffcultySeeing;

    @Enumerated(EnumType.STRING)
    @Column(name = "diffculty_hearing")
    private Difficulty diffcultyHearing;

    @Enumerated(EnumType.STRING)
    @Column(name = "diffculty_walking")
    private Difficulty diffcultyWalking;

    @Enumerated(EnumType.STRING)
    @Column(name = "diffculty_memory")
    private Difficulty diffcultyMemory;

    @Enumerated(EnumType.STRING)
    @Column(name = "diffculty_self_care")
    private Difficulty diffcultySelfCare;

    @Enumerated(EnumType.STRING)
    @Column(name = "diffculty_communicating")
    private Difficulty diffcultyCommunicating;

    @Enumerated(EnumType.STRING)
    @Column(name = "cause_of_disability")
    private CauseOfDisability causeOfDisability;

    @Enumerated(EnumType.STRING)
    @Column(name = "health_service_availibility")
    private ServiceAvailibility healthServiceAvailibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "health_service_accessibility")
    private ServiceAccessibility healthServiceAccessibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "education_service_availibility")
    private ServiceAvailibility educationServiceAvailibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "education_service_accessibility")
    private ServiceAccessibility educationServiceAccessibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "law_service_availibility")
    private ServiceAvailibility lawServiceAvailibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "law_service_accessibility")
    private ServiceAccessibility lawServiceAccessibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "welfare_service_availibility")
    private ServiceAvailibility welfareServiceAvailibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "welfare_service_accessibility")
    private ServiceAccessibility welfareServiceAccessibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "sports_service_availibility")
    private ServiceAvailibility sportsServiceAvailibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "sports_service_accessibility")
    private ServiceAccessibility sportsServiceAccessibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "road_service_availibility")
    private ServiceAvailibility roadServiceAvailibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "road_service_accessibility")
    private ServiceAccessibility roadServiceAccessibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "market_service_availibility")
    private ServiceAvailibility marketServiceAvailibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "market_service_accessibility")
    private ServiceAccessibility marketServiceAccessibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "banking_service_availibility")
    private ServiceAvailibility bankingServiceAvailibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "banking_service_accessibility")
    private ServiceAccessibility bankingServiceAccessibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "tele_communication_service_availibility")
    private ServiceAvailibility teleCommunicationServiceAvailibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "tele_communication_service_accessibility")
    private ServiceAccessibility teleCommunicationServiceAccessibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "church_service_availibility")
    private ServiceAvailibility churchServiceAvailibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "church_service_accessibility")
    private ServiceAccessibility churchServiceAccessibility;

    @Column(name = "member_disabled_persons_organisation")
    private Boolean memberDisabledPersonsOrganisation;

    @Column(name = "awareness_advocacy_programme_details")
    private String awarenessAdvocacyProgrammeDetails;

    @Enumerated(EnumType.STRING)
    @Column(name = "livelihood")
    private Livelihood livelihood;

    @Column(name = "livelihood_details")
    private String livelihoodDetails;

    @Enumerated(EnumType.STRING)
    @Column(name = "talents")
    private Talents talents;

    @OneToOne
    @JoinColumn(unique = true)
    private ContactDetails individualContactDetails;

    @OneToOne
    @JoinColumn(unique = true)
    private ContactDetails carerContactDetails;

    @ManyToOne
    @JsonIgnoreProperties("disabledIndividuals")
    private Location individualLocation;

    @ManyToOne
    @JsonIgnoreProperties("disabledIndividuals")
    private Location carerLocation;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCivilRegistrationNo() {
        return civilRegistrationNo;
    }

    public DisabledIndividual civilRegistrationNo(String civilRegistrationNo) {
        this.civilRegistrationNo = civilRegistrationNo;
        return this;
    }

    public void setCivilRegistrationNo(String civilRegistrationNo) {
        this.civilRegistrationNo = civilRegistrationNo;
    }

    public String getName() {
        return name;
    }

    public DisabledIndividual name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public DisabledIndividual gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getReligion() {
        return religion;
    }

    public DisabledIndividual religion(String religion) {
        this.religion = religion;
        return this;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getOccupation() {
        return occupation;
    }

    public DisabledIndividual occupation(String occupation) {
        this.occupation = occupation;
        return this;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmployer() {
        return employer;
    }

    public DisabledIndividual employer(String employer) {
        this.employer = employer;
        return this;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public Instant getDateOfBirth() {
        return dateOfBirth;
    }

    public DisabledIndividual dateOfBirth(Instant dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public void setDateOfBirth(Instant dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public EstimateAge getEstimatedAge() {
        return estimatedAge;
    }

    public DisabledIndividual estimatedAge(EstimateAge estimatedAge) {
        this.estimatedAge = estimatedAge;
        return this;
    }

    public void setEstimatedAge(EstimateAge estimatedAge) {
        this.estimatedAge = estimatedAge;
    }

    public MartialStatus getMartialStatus() {
        return martialStatus;
    }

    public DisabledIndividual martialStatus(MartialStatus martialStatus) {
        this.martialStatus = martialStatus;
        return this;
    }

    public void setMartialStatus(MartialStatus martialStatus) {
        this.martialStatus = martialStatus;
    }

    public Integer getNoOfChildrenAlive() {
        return noOfChildrenAlive;
    }

    public DisabledIndividual noOfChildrenAlive(Integer noOfChildrenAlive) {
        this.noOfChildrenAlive = noOfChildrenAlive;
        return this;
    }

    public void setNoOfChildrenAlive(Integer noOfChildrenAlive) {
        this.noOfChildrenAlive = noOfChildrenAlive;
    }

    public Integer getNoOfChildrenDeceased() {
        return noOfChildrenDeceased;
    }

    public DisabledIndividual noOfChildrenDeceased(Integer noOfChildrenDeceased) {
        this.noOfChildrenDeceased = noOfChildrenDeceased;
        return this;
    }

    public void setNoOfChildrenDeceased(Integer noOfChildrenDeceased) {
        this.noOfChildrenDeceased = noOfChildrenDeceased;
    }

    public Integer getNoOfChildrenCongenital() {
        return noOfChildrenCongenital;
    }

    public DisabledIndividual noOfChildrenCongenital(Integer noOfChildrenCongenital) {
        this.noOfChildrenCongenital = noOfChildrenCongenital;
        return this;
    }

    public void setNoOfChildrenCongenital(Integer noOfChildrenCongenital) {
        this.noOfChildrenCongenital = noOfChildrenCongenital;
    }

    public String getEducationFormal() {
        return educationFormal;
    }

    public DisabledIndividual educationFormal(String educationFormal) {
        this.educationFormal = educationFormal;
        return this;
    }

    public void setEducationFormal(String educationFormal) {
        this.educationFormal = educationFormal;
    }

    public String getEducationTrade() {
        return educationTrade;
    }

    public DisabledIndividual educationTrade(String educationTrade) {
        this.educationTrade = educationTrade;
        return this;
    }

    public void setEducationTrade(String educationTrade) {
        this.educationTrade = educationTrade;
    }

    public CarerRelationship getCarerRelationship() {
        return carerRelationship;
    }

    public DisabledIndividual carerRelationship(CarerRelationship carerRelationship) {
        this.carerRelationship = carerRelationship;
        return this;
    }

    public void setCarerRelationship(CarerRelationship carerRelationship) {
        this.carerRelationship = carerRelationship;
    }

    public Difficulty getDiffcultySeeing() {
        return diffcultySeeing;
    }

    public DisabledIndividual diffcultySeeing(Difficulty diffcultySeeing) {
        this.diffcultySeeing = diffcultySeeing;
        return this;
    }

    public void setDiffcultySeeing(Difficulty diffcultySeeing) {
        this.diffcultySeeing = diffcultySeeing;
    }

    public Difficulty getDiffcultyHearing() {
        return diffcultyHearing;
    }

    public DisabledIndividual diffcultyHearing(Difficulty diffcultyHearing) {
        this.diffcultyHearing = diffcultyHearing;
        return this;
    }

    public void setDiffcultyHearing(Difficulty diffcultyHearing) {
        this.diffcultyHearing = diffcultyHearing;
    }

    public Difficulty getDiffcultyWalking() {
        return diffcultyWalking;
    }

    public DisabledIndividual diffcultyWalking(Difficulty diffcultyWalking) {
        this.diffcultyWalking = diffcultyWalking;
        return this;
    }

    public void setDiffcultyWalking(Difficulty diffcultyWalking) {
        this.diffcultyWalking = diffcultyWalking;
    }

    public Difficulty getDiffcultyMemory() {
        return diffcultyMemory;
    }

    public DisabledIndividual diffcultyMemory(Difficulty diffcultyMemory) {
        this.diffcultyMemory = diffcultyMemory;
        return this;
    }

    public void setDiffcultyMemory(Difficulty diffcultyMemory) {
        this.diffcultyMemory = diffcultyMemory;
    }

    public Difficulty getDiffcultySelfCare() {
        return diffcultySelfCare;
    }

    public DisabledIndividual diffcultySelfCare(Difficulty diffcultySelfCare) {
        this.diffcultySelfCare = diffcultySelfCare;
        return this;
    }

    public void setDiffcultySelfCare(Difficulty diffcultySelfCare) {
        this.diffcultySelfCare = diffcultySelfCare;
    }

    public Difficulty getDiffcultyCommunicating() {
        return diffcultyCommunicating;
    }

    public DisabledIndividual diffcultyCommunicating(Difficulty diffcultyCommunicating) {
        this.diffcultyCommunicating = diffcultyCommunicating;
        return this;
    }

    public void setDiffcultyCommunicating(Difficulty diffcultyCommunicating) {
        this.diffcultyCommunicating = diffcultyCommunicating;
    }

    public CauseOfDisability getCauseOfDisability() {
        return causeOfDisability;
    }

    public DisabledIndividual causeOfDisability(CauseOfDisability causeOfDisability) {
        this.causeOfDisability = causeOfDisability;
        return this;
    }

    public void setCauseOfDisability(CauseOfDisability causeOfDisability) {
        this.causeOfDisability = causeOfDisability;
    }

    public ServiceAvailibility getHealthServiceAvailibility() {
        return healthServiceAvailibility;
    }

    public DisabledIndividual healthServiceAvailibility(ServiceAvailibility healthServiceAvailibility) {
        this.healthServiceAvailibility = healthServiceAvailibility;
        return this;
    }

    public void setHealthServiceAvailibility(ServiceAvailibility healthServiceAvailibility) {
        this.healthServiceAvailibility = healthServiceAvailibility;
    }

    public ServiceAccessibility getHealthServiceAccessibility() {
        return healthServiceAccessibility;
    }

    public DisabledIndividual healthServiceAccessibility(ServiceAccessibility healthServiceAccessibility) {
        this.healthServiceAccessibility = healthServiceAccessibility;
        return this;
    }

    public void setHealthServiceAccessibility(ServiceAccessibility healthServiceAccessibility) {
        this.healthServiceAccessibility = healthServiceAccessibility;
    }

    public ServiceAvailibility getEducationServiceAvailibility() {
        return educationServiceAvailibility;
    }

    public DisabledIndividual educationServiceAvailibility(ServiceAvailibility educationServiceAvailibility) {
        this.educationServiceAvailibility = educationServiceAvailibility;
        return this;
    }

    public void setEducationServiceAvailibility(ServiceAvailibility educationServiceAvailibility) {
        this.educationServiceAvailibility = educationServiceAvailibility;
    }

    public ServiceAccessibility getEducationServiceAccessibility() {
        return educationServiceAccessibility;
    }

    public DisabledIndividual educationServiceAccessibility(ServiceAccessibility educationServiceAccessibility) {
        this.educationServiceAccessibility = educationServiceAccessibility;
        return this;
    }

    public void setEducationServiceAccessibility(ServiceAccessibility educationServiceAccessibility) {
        this.educationServiceAccessibility = educationServiceAccessibility;
    }

    public ServiceAvailibility getLawServiceAvailibility() {
        return lawServiceAvailibility;
    }

    public DisabledIndividual lawServiceAvailibility(ServiceAvailibility lawServiceAvailibility) {
        this.lawServiceAvailibility = lawServiceAvailibility;
        return this;
    }

    public void setLawServiceAvailibility(ServiceAvailibility lawServiceAvailibility) {
        this.lawServiceAvailibility = lawServiceAvailibility;
    }

    public ServiceAccessibility getLawServiceAccessibility() {
        return lawServiceAccessibility;
    }

    public DisabledIndividual lawServiceAccessibility(ServiceAccessibility lawServiceAccessibility) {
        this.lawServiceAccessibility = lawServiceAccessibility;
        return this;
    }

    public void setLawServiceAccessibility(ServiceAccessibility lawServiceAccessibility) {
        this.lawServiceAccessibility = lawServiceAccessibility;
    }

    public ServiceAvailibility getWelfareServiceAvailibility() {
        return welfareServiceAvailibility;
    }

    public DisabledIndividual welfareServiceAvailibility(ServiceAvailibility welfareServiceAvailibility) {
        this.welfareServiceAvailibility = welfareServiceAvailibility;
        return this;
    }

    public void setWelfareServiceAvailibility(ServiceAvailibility welfareServiceAvailibility) {
        this.welfareServiceAvailibility = welfareServiceAvailibility;
    }

    public ServiceAccessibility getWelfareServiceAccessibility() {
        return welfareServiceAccessibility;
    }

    public DisabledIndividual welfareServiceAccessibility(ServiceAccessibility welfareServiceAccessibility) {
        this.welfareServiceAccessibility = welfareServiceAccessibility;
        return this;
    }

    public void setWelfareServiceAccessibility(ServiceAccessibility welfareServiceAccessibility) {
        this.welfareServiceAccessibility = welfareServiceAccessibility;
    }

    public ServiceAvailibility getSportsServiceAvailibility() {
        return sportsServiceAvailibility;
    }

    public DisabledIndividual sportsServiceAvailibility(ServiceAvailibility sportsServiceAvailibility) {
        this.sportsServiceAvailibility = sportsServiceAvailibility;
        return this;
    }

    public void setSportsServiceAvailibility(ServiceAvailibility sportsServiceAvailibility) {
        this.sportsServiceAvailibility = sportsServiceAvailibility;
    }

    public ServiceAccessibility getSportsServiceAccessibility() {
        return sportsServiceAccessibility;
    }

    public DisabledIndividual sportsServiceAccessibility(ServiceAccessibility sportsServiceAccessibility) {
        this.sportsServiceAccessibility = sportsServiceAccessibility;
        return this;
    }

    public void setSportsServiceAccessibility(ServiceAccessibility sportsServiceAccessibility) {
        this.sportsServiceAccessibility = sportsServiceAccessibility;
    }

    public ServiceAvailibility getRoadServiceAvailibility() {
        return roadServiceAvailibility;
    }

    public DisabledIndividual roadServiceAvailibility(ServiceAvailibility roadServiceAvailibility) {
        this.roadServiceAvailibility = roadServiceAvailibility;
        return this;
    }

    public void setRoadServiceAvailibility(ServiceAvailibility roadServiceAvailibility) {
        this.roadServiceAvailibility = roadServiceAvailibility;
    }

    public ServiceAccessibility getRoadServiceAccessibility() {
        return roadServiceAccessibility;
    }

    public DisabledIndividual roadServiceAccessibility(ServiceAccessibility roadServiceAccessibility) {
        this.roadServiceAccessibility = roadServiceAccessibility;
        return this;
    }

    public void setRoadServiceAccessibility(ServiceAccessibility roadServiceAccessibility) {
        this.roadServiceAccessibility = roadServiceAccessibility;
    }

    public ServiceAvailibility getMarketServiceAvailibility() {
        return marketServiceAvailibility;
    }

    public DisabledIndividual marketServiceAvailibility(ServiceAvailibility marketServiceAvailibility) {
        this.marketServiceAvailibility = marketServiceAvailibility;
        return this;
    }

    public void setMarketServiceAvailibility(ServiceAvailibility marketServiceAvailibility) {
        this.marketServiceAvailibility = marketServiceAvailibility;
    }

    public ServiceAccessibility getMarketServiceAccessibility() {
        return marketServiceAccessibility;
    }

    public DisabledIndividual marketServiceAccessibility(ServiceAccessibility marketServiceAccessibility) {
        this.marketServiceAccessibility = marketServiceAccessibility;
        return this;
    }

    public void setMarketServiceAccessibility(ServiceAccessibility marketServiceAccessibility) {
        this.marketServiceAccessibility = marketServiceAccessibility;
    }

    public ServiceAvailibility getBankingServiceAvailibility() {
        return bankingServiceAvailibility;
    }

    public DisabledIndividual bankingServiceAvailibility(ServiceAvailibility bankingServiceAvailibility) {
        this.bankingServiceAvailibility = bankingServiceAvailibility;
        return this;
    }

    public void setBankingServiceAvailibility(ServiceAvailibility bankingServiceAvailibility) {
        this.bankingServiceAvailibility = bankingServiceAvailibility;
    }

    public ServiceAccessibility getBankingServiceAccessibility() {
        return bankingServiceAccessibility;
    }

    public DisabledIndividual bankingServiceAccessibility(ServiceAccessibility bankingServiceAccessibility) {
        this.bankingServiceAccessibility = bankingServiceAccessibility;
        return this;
    }

    public void setBankingServiceAccessibility(ServiceAccessibility bankingServiceAccessibility) {
        this.bankingServiceAccessibility = bankingServiceAccessibility;
    }

    public ServiceAvailibility getTeleCommunicationServiceAvailibility() {
        return teleCommunicationServiceAvailibility;
    }

    public DisabledIndividual teleCommunicationServiceAvailibility(ServiceAvailibility teleCommunicationServiceAvailibility) {
        this.teleCommunicationServiceAvailibility = teleCommunicationServiceAvailibility;
        return this;
    }

    public void setTeleCommunicationServiceAvailibility(ServiceAvailibility teleCommunicationServiceAvailibility) {
        this.teleCommunicationServiceAvailibility = teleCommunicationServiceAvailibility;
    }

    public ServiceAccessibility getTeleCommunicationServiceAccessibility() {
        return teleCommunicationServiceAccessibility;
    }

    public DisabledIndividual teleCommunicationServiceAccessibility(ServiceAccessibility teleCommunicationServiceAccessibility) {
        this.teleCommunicationServiceAccessibility = teleCommunicationServiceAccessibility;
        return this;
    }

    public void setTeleCommunicationServiceAccessibility(ServiceAccessibility teleCommunicationServiceAccessibility) {
        this.teleCommunicationServiceAccessibility = teleCommunicationServiceAccessibility;
    }

    public ServiceAvailibility getChurchServiceAvailibility() {
        return churchServiceAvailibility;
    }

    public DisabledIndividual churchServiceAvailibility(ServiceAvailibility churchServiceAvailibility) {
        this.churchServiceAvailibility = churchServiceAvailibility;
        return this;
    }

    public void setChurchServiceAvailibility(ServiceAvailibility churchServiceAvailibility) {
        this.churchServiceAvailibility = churchServiceAvailibility;
    }

    public ServiceAccessibility getChurchServiceAccessibility() {
        return churchServiceAccessibility;
    }

    public DisabledIndividual churchServiceAccessibility(ServiceAccessibility churchServiceAccessibility) {
        this.churchServiceAccessibility = churchServiceAccessibility;
        return this;
    }

    public void setChurchServiceAccessibility(ServiceAccessibility churchServiceAccessibility) {
        this.churchServiceAccessibility = churchServiceAccessibility;
    }

    public Boolean isMemberDisabledPersonsOrganisation() {
        return memberDisabledPersonsOrganisation;
    }

    public DisabledIndividual memberDisabledPersonsOrganisation(Boolean memberDisabledPersonsOrganisation) {
        this.memberDisabledPersonsOrganisation = memberDisabledPersonsOrganisation;
        return this;
    }

    public void setMemberDisabledPersonsOrganisation(Boolean memberDisabledPersonsOrganisation) {
        this.memberDisabledPersonsOrganisation = memberDisabledPersonsOrganisation;
    }

    public String getAwarenessAdvocacyProgrammeDetails() {
        return awarenessAdvocacyProgrammeDetails;
    }

    public DisabledIndividual awarenessAdvocacyProgrammeDetails(String awarenessAdvocacyProgrammeDetails) {
        this.awarenessAdvocacyProgrammeDetails = awarenessAdvocacyProgrammeDetails;
        return this;
    }

    public void setAwarenessAdvocacyProgrammeDetails(String awarenessAdvocacyProgrammeDetails) {
        this.awarenessAdvocacyProgrammeDetails = awarenessAdvocacyProgrammeDetails;
    }

    public Livelihood getLivelihood() {
        return livelihood;
    }

    public DisabledIndividual livelihood(Livelihood livelihood) {
        this.livelihood = livelihood;
        return this;
    }

    public void setLivelihood(Livelihood livelihood) {
        this.livelihood = livelihood;
    }

    public String getLivelihoodDetails() {
        return livelihoodDetails;
    }

    public DisabledIndividual livelihoodDetails(String livelihoodDetails) {
        this.livelihoodDetails = livelihoodDetails;
        return this;
    }

    public void setLivelihoodDetails(String livelihoodDetails) {
        this.livelihoodDetails = livelihoodDetails;
    }

    public Talents getTalents() {
        return talents;
    }

    public DisabledIndividual talents(Talents talents) {
        this.talents = talents;
        return this;
    }

    public void setTalents(Talents talents) {
        this.talents = talents;
    }

    public ContactDetails getIndividualContactDetails() {
        return individualContactDetails;
    }

    public DisabledIndividual individualContactDetails(ContactDetails contactDetails) {
        this.individualContactDetails = contactDetails;
        return this;
    }

    public void setIndividualContactDetails(ContactDetails contactDetails) {
        this.individualContactDetails = contactDetails;
    }

    public ContactDetails getCarerContactDetails() {
        return carerContactDetails;
    }

    public DisabledIndividual carerContactDetails(ContactDetails contactDetails) {
        this.carerContactDetails = contactDetails;
        return this;
    }

    public void setCarerContactDetails(ContactDetails contactDetails) {
        this.carerContactDetails = contactDetails;
    }

    public Location getIndividualLocation() {
        return individualLocation;
    }

    public DisabledIndividual individualLocation(Location location) {
        this.individualLocation = location;
        return this;
    }

    public void setIndividualLocation(Location location) {
        this.individualLocation = location;
    }

    public Location getCarerLocation() {
        return carerLocation;
    }

    public DisabledIndividual carerLocation(Location location) {
        this.carerLocation = location;
        return this;
    }

    public void setCarerLocation(Location location) {
        this.carerLocation = location;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DisabledIndividual)) {
            return false;
        }
        return id != null && id.equals(((DisabledIndividual) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DisabledIndividual{" +
            "id=" + getId() +
            ", civilRegistrationNo='" + getCivilRegistrationNo() + "'" +
            ", name='" + getName() + "'" +
            ", gender='" + getGender() + "'" +
            ", religion='" + getReligion() + "'" +
            ", occupation='" + getOccupation() + "'" +
            ", employer='" + getEmployer() + "'" +
            ", dateOfBirth='" + getDateOfBirth() + "'" +
            ", estimatedAge='" + getEstimatedAge() + "'" +
            ", martialStatus='" + getMartialStatus() + "'" +
            ", noOfChildrenAlive=" + getNoOfChildrenAlive() +
            ", noOfChildrenDeceased=" + getNoOfChildrenDeceased() +
            ", noOfChildrenCongenital=" + getNoOfChildrenCongenital() +
            ", educationFormal='" + getEducationFormal() + "'" +
            ", educationTrade='" + getEducationTrade() + "'" +
            ", carerRelationship='" + getCarerRelationship() + "'" +
            ", diffcultySeeing='" + getDiffcultySeeing() + "'" +
            ", diffcultyHearing='" + getDiffcultyHearing() + "'" +
            ", diffcultyWalking='" + getDiffcultyWalking() + "'" +
            ", diffcultyMemory='" + getDiffcultyMemory() + "'" +
            ", diffcultySelfCare='" + getDiffcultySelfCare() + "'" +
            ", diffcultyCommunicating='" + getDiffcultyCommunicating() + "'" +
            ", causeOfDisability='" + getCauseOfDisability() + "'" +
            ", healthServiceAvailibility='" + getHealthServiceAvailibility() + "'" +
            ", healthServiceAccessibility='" + getHealthServiceAccessibility() + "'" +
            ", educationServiceAvailibility='" + getEducationServiceAvailibility() + "'" +
            ", educationServiceAccessibility='" + getEducationServiceAccessibility() + "'" +
            ", lawServiceAvailibility='" + getLawServiceAvailibility() + "'" +
            ", lawServiceAccessibility='" + getLawServiceAccessibility() + "'" +
            ", welfareServiceAvailibility='" + getWelfareServiceAvailibility() + "'" +
            ", welfareServiceAccessibility='" + getWelfareServiceAccessibility() + "'" +
            ", sportsServiceAvailibility='" + getSportsServiceAvailibility() + "'" +
            ", sportsServiceAccessibility='" + getSportsServiceAccessibility() + "'" +
            ", roadServiceAvailibility='" + getRoadServiceAvailibility() + "'" +
            ", roadServiceAccessibility='" + getRoadServiceAccessibility() + "'" +
            ", marketServiceAvailibility='" + getMarketServiceAvailibility() + "'" +
            ", marketServiceAccessibility='" + getMarketServiceAccessibility() + "'" +
            ", bankingServiceAvailibility='" + getBankingServiceAvailibility() + "'" +
            ", bankingServiceAccessibility='" + getBankingServiceAccessibility() + "'" +
            ", teleCommunicationServiceAvailibility='" + getTeleCommunicationServiceAvailibility() + "'" +
            ", teleCommunicationServiceAccessibility='" + getTeleCommunicationServiceAccessibility() + "'" +
            ", churchServiceAvailibility='" + getChurchServiceAvailibility() + "'" +
            ", churchServiceAccessibility='" + getChurchServiceAccessibility() + "'" +
            ", memberDisabledPersonsOrganisation='" + isMemberDisabledPersonsOrganisation() + "'" +
            ", awarenessAdvocacyProgrammeDetails='" + getAwarenessAdvocacyProgrammeDetails() + "'" +
            ", livelihood='" + getLivelihood() + "'" +
            ", livelihoodDetails='" + getLivelihoodDetails() + "'" +
            ", talents='" + getTalents() + "'" +
            "}";
    }
}
