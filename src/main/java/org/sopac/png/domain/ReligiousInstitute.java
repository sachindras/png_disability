package org.sopac.png.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ReligiousInstitute.
 */
@Entity
@Table(name = "religious_institute")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "religiousinstitute")
public class ReligiousInstitute implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "church_name")
    private String churchName;

    @Column(name = "church_address")
    private String churchAddress;

    @Column(name = "denomination")
    private String denomination;

    @Column(name = "head_office_location")
    private String headOfficeLocation;

    @Column(name = "number_church_membership")
    private Integer numberChurchMembership;

    @OneToMany(mappedBy = "religiousInstitute")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ChurchLeader> churchLeaders = new HashSet<>();

    @OneToMany(mappedBy = "religiousInstitute")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Hospital> churchHospitals = new HashSet<>();

    @OneToMany(mappedBy = "religiousInstitute")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Clinic> churchClinics = new HashSet<>();

    @OneToMany(mappedBy = "religiousInstitute")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OtherHealthService> churchOtherHealthServices = new HashSet<>();

    @OneToMany(mappedBy = "religiousInstitute")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<UniversityCollege> churchUniversityColleges = new HashSet<>();

    @OneToMany(mappedBy = "religiousInstitute")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TechnicalSchool> churchTechnicalSchools = new HashSet<>();

    @OneToMany(mappedBy = "religiousInstitute")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SecondarySchool> churchSecondarySchools = new HashSet<>();

    @OneToMany(mappedBy = "religiousInstitute")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PrimarySchool> churchPrimarySchools = new HashSet<>();

    @OneToMany(mappedBy = "religiousInstitute")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OtherEducationService> churchOtherEducationServices = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChurchName() {
        return churchName;
    }

    public ReligiousInstitute churchName(String churchName) {
        this.churchName = churchName;
        return this;
    }

    public void setChurchName(String churchName) {
        this.churchName = churchName;
    }

    public String getChurchAddress() {
        return churchAddress;
    }

    public ReligiousInstitute churchAddress(String churchAddress) {
        this.churchAddress = churchAddress;
        return this;
    }

    public void setChurchAddress(String churchAddress) {
        this.churchAddress = churchAddress;
    }

    public String getDenomination() {
        return denomination;
    }

    public ReligiousInstitute denomination(String denomination) {
        this.denomination = denomination;
        return this;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getHeadOfficeLocation() {
        return headOfficeLocation;
    }

    public ReligiousInstitute headOfficeLocation(String headOfficeLocation) {
        this.headOfficeLocation = headOfficeLocation;
        return this;
    }

    public void setHeadOfficeLocation(String headOfficeLocation) {
        this.headOfficeLocation = headOfficeLocation;
    }

    public Integer getNumberChurchMembership() {
        return numberChurchMembership;
    }

    public ReligiousInstitute numberChurchMembership(Integer numberChurchMembership) {
        this.numberChurchMembership = numberChurchMembership;
        return this;
    }

    public void setNumberChurchMembership(Integer numberChurchMembership) {
        this.numberChurchMembership = numberChurchMembership;
    }

    public Set<ChurchLeader> getChurchLeaders() {
        return churchLeaders;
    }

    public ReligiousInstitute churchLeaders(Set<ChurchLeader> churchLeaders) {
        this.churchLeaders = churchLeaders;
        return this;
    }

    public ReligiousInstitute addChurchLeaders(ChurchLeader churchLeader) {
        this.churchLeaders.add(churchLeader);
        churchLeader.setReligiousInstitute(this);
        return this;
    }

    public ReligiousInstitute removeChurchLeaders(ChurchLeader churchLeader) {
        this.churchLeaders.remove(churchLeader);
        churchLeader.setReligiousInstitute(null);
        return this;
    }

    public void setChurchLeaders(Set<ChurchLeader> churchLeaders) {
        this.churchLeaders = churchLeaders;
    }

    public Set<Hospital> getChurchHospitals() {
        return churchHospitals;
    }

    public ReligiousInstitute churchHospitals(Set<Hospital> hospitals) {
        this.churchHospitals = hospitals;
        return this;
    }

    public ReligiousInstitute addChurchHospitals(Hospital hospital) {
        this.churchHospitals.add(hospital);
        hospital.setReligiousInstitute(this);
        return this;
    }

    public ReligiousInstitute removeChurchHospitals(Hospital hospital) {
        this.churchHospitals.remove(hospital);
        hospital.setReligiousInstitute(null);
        return this;
    }

    public void setChurchHospitals(Set<Hospital> hospitals) {
        this.churchHospitals = hospitals;
    }

    public Set<Clinic> getChurchClinics() {
        return churchClinics;
    }

    public ReligiousInstitute churchClinics(Set<Clinic> clinics) {
        this.churchClinics = clinics;
        return this;
    }

    public ReligiousInstitute addChurchClinics(Clinic clinic) {
        this.churchClinics.add(clinic);
        clinic.setReligiousInstitute(this);
        return this;
    }

    public ReligiousInstitute removeChurchClinics(Clinic clinic) {
        this.churchClinics.remove(clinic);
        clinic.setReligiousInstitute(null);
        return this;
    }

    public void setChurchClinics(Set<Clinic> clinics) {
        this.churchClinics = clinics;
    }

    public Set<OtherHealthService> getChurchOtherHealthServices() {
        return churchOtherHealthServices;
    }

    public ReligiousInstitute churchOtherHealthServices(Set<OtherHealthService> otherHealthServices) {
        this.churchOtherHealthServices = otherHealthServices;
        return this;
    }

    public ReligiousInstitute addChurchOtherHealthServices(OtherHealthService otherHealthService) {
        this.churchOtherHealthServices.add(otherHealthService);
        otherHealthService.setReligiousInstitute(this);
        return this;
    }

    public ReligiousInstitute removeChurchOtherHealthServices(OtherHealthService otherHealthService) {
        this.churchOtherHealthServices.remove(otherHealthService);
        otherHealthService.setReligiousInstitute(null);
        return this;
    }

    public void setChurchOtherHealthServices(Set<OtherHealthService> otherHealthServices) {
        this.churchOtherHealthServices = otherHealthServices;
    }

    public Set<UniversityCollege> getChurchUniversityColleges() {
        return churchUniversityColleges;
    }

    public ReligiousInstitute churchUniversityColleges(Set<UniversityCollege> universityColleges) {
        this.churchUniversityColleges = universityColleges;
        return this;
    }

    public ReligiousInstitute addChurchUniversityColleges(UniversityCollege universityCollege) {
        this.churchUniversityColleges.add(universityCollege);
        universityCollege.setReligiousInstitute(this);
        return this;
    }

    public ReligiousInstitute removeChurchUniversityColleges(UniversityCollege universityCollege) {
        this.churchUniversityColleges.remove(universityCollege);
        universityCollege.setReligiousInstitute(null);
        return this;
    }

    public void setChurchUniversityColleges(Set<UniversityCollege> universityColleges) {
        this.churchUniversityColleges = universityColleges;
    }

    public Set<TechnicalSchool> getChurchTechnicalSchools() {
        return churchTechnicalSchools;
    }

    public ReligiousInstitute churchTechnicalSchools(Set<TechnicalSchool> technicalSchools) {
        this.churchTechnicalSchools = technicalSchools;
        return this;
    }

    public ReligiousInstitute addChurchTechnicalSchools(TechnicalSchool technicalSchool) {
        this.churchTechnicalSchools.add(technicalSchool);
        technicalSchool.setReligiousInstitute(this);
        return this;
    }

    public ReligiousInstitute removeChurchTechnicalSchools(TechnicalSchool technicalSchool) {
        this.churchTechnicalSchools.remove(technicalSchool);
        technicalSchool.setReligiousInstitute(null);
        return this;
    }

    public void setChurchTechnicalSchools(Set<TechnicalSchool> technicalSchools) {
        this.churchTechnicalSchools = technicalSchools;
    }

    public Set<SecondarySchool> getChurchSecondarySchools() {
        return churchSecondarySchools;
    }

    public ReligiousInstitute churchSecondarySchools(Set<SecondarySchool> secondarySchools) {
        this.churchSecondarySchools = secondarySchools;
        return this;
    }

    public ReligiousInstitute addChurchSecondarySchools(SecondarySchool secondarySchool) {
        this.churchSecondarySchools.add(secondarySchool);
        secondarySchool.setReligiousInstitute(this);
        return this;
    }

    public ReligiousInstitute removeChurchSecondarySchools(SecondarySchool secondarySchool) {
        this.churchSecondarySchools.remove(secondarySchool);
        secondarySchool.setReligiousInstitute(null);
        return this;
    }

    public void setChurchSecondarySchools(Set<SecondarySchool> secondarySchools) {
        this.churchSecondarySchools = secondarySchools;
    }

    public Set<PrimarySchool> getChurchPrimarySchools() {
        return churchPrimarySchools;
    }

    public ReligiousInstitute churchPrimarySchools(Set<PrimarySchool> primarySchools) {
        this.churchPrimarySchools = primarySchools;
        return this;
    }

    public ReligiousInstitute addChurchPrimarySchools(PrimarySchool primarySchool) {
        this.churchPrimarySchools.add(primarySchool);
        primarySchool.setReligiousInstitute(this);
        return this;
    }

    public ReligiousInstitute removeChurchPrimarySchools(PrimarySchool primarySchool) {
        this.churchPrimarySchools.remove(primarySchool);
        primarySchool.setReligiousInstitute(null);
        return this;
    }

    public void setChurchPrimarySchools(Set<PrimarySchool> primarySchools) {
        this.churchPrimarySchools = primarySchools;
    }

    public Set<OtherEducationService> getChurchOtherEducationServices() {
        return churchOtherEducationServices;
    }

    public ReligiousInstitute churchOtherEducationServices(Set<OtherEducationService> otherEducationServices) {
        this.churchOtherEducationServices = otherEducationServices;
        return this;
    }

    public ReligiousInstitute addChurchOtherEducationServices(OtherEducationService otherEducationService) {
        this.churchOtherEducationServices.add(otherEducationService);
        otherEducationService.setReligiousInstitute(this);
        return this;
    }

    public ReligiousInstitute removeChurchOtherEducationServices(OtherEducationService otherEducationService) {
        this.churchOtherEducationServices.remove(otherEducationService);
        otherEducationService.setReligiousInstitute(null);
        return this;
    }

    public void setChurchOtherEducationServices(Set<OtherEducationService> otherEducationServices) {
        this.churchOtherEducationServices = otherEducationServices;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ReligiousInstitute)) {
            return false;
        }
        return id != null && id.equals(((ReligiousInstitute) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ReligiousInstitute{" +
            "id=" + getId() +
            ", churchName='" + getChurchName() + "'" +
            ", churchAddress='" + getChurchAddress() + "'" +
            ", denomination='" + getDenomination() + "'" +
            ", headOfficeLocation='" + getHeadOfficeLocation() + "'" +
            ", numberChurchMembership=" + getNumberChurchMembership() +
            "}";
    }
}
