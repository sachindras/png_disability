package org.sopac.png.web.rest;

import org.sopac.png.domain.PrimarySchool;
import org.sopac.png.repository.PrimarySchoolRepository;
import org.sopac.png.repository.search.PrimarySchoolSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.PrimarySchool}.
 */
@RestController
@RequestMapping("/api")
public class PrimarySchoolResource {

    private final Logger log = LoggerFactory.getLogger(PrimarySchoolResource.class);

    private static final String ENTITY_NAME = "primarySchool";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PrimarySchoolRepository primarySchoolRepository;

    private final PrimarySchoolSearchRepository primarySchoolSearchRepository;

    public PrimarySchoolResource(PrimarySchoolRepository primarySchoolRepository, PrimarySchoolSearchRepository primarySchoolSearchRepository) {
        this.primarySchoolRepository = primarySchoolRepository;
        this.primarySchoolSearchRepository = primarySchoolSearchRepository;
    }

    /**
     * {@code POST  /primary-schools} : Create a new primarySchool.
     *
     * @param primarySchool the primarySchool to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new primarySchool, or with status {@code 400 (Bad Request)} if the primarySchool has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/primary-schools")
    public ResponseEntity<PrimarySchool> createPrimarySchool(@RequestBody PrimarySchool primarySchool) throws URISyntaxException {
        log.debug("REST request to save PrimarySchool : {}", primarySchool);
        if (primarySchool.getId() != null) {
            throw new BadRequestAlertException("A new primarySchool cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PrimarySchool result = primarySchoolRepository.save(primarySchool);
        primarySchoolSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/primary-schools/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /primary-schools} : Updates an existing primarySchool.
     *
     * @param primarySchool the primarySchool to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated primarySchool,
     * or with status {@code 400 (Bad Request)} if the primarySchool is not valid,
     * or with status {@code 500 (Internal Server Error)} if the primarySchool couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/primary-schools")
    public ResponseEntity<PrimarySchool> updatePrimarySchool(@RequestBody PrimarySchool primarySchool) throws URISyntaxException {
        log.debug("REST request to update PrimarySchool : {}", primarySchool);
        if (primarySchool.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PrimarySchool result = primarySchoolRepository.save(primarySchool);
        primarySchoolSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, primarySchool.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /primary-schools} : get all the primarySchools.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of primarySchools in body.
     */
    @GetMapping("/primary-schools")
    public ResponseEntity<List<PrimarySchool>> getAllPrimarySchools(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of PrimarySchools");
        Page<PrimarySchool> page = primarySchoolRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /primary-schools/:id} : get the "id" primarySchool.
     *
     * @param id the id of the primarySchool to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the primarySchool, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/primary-schools/{id}")
    public ResponseEntity<PrimarySchool> getPrimarySchool(@PathVariable Long id) {
        log.debug("REST request to get PrimarySchool : {}", id);
        Optional<PrimarySchool> primarySchool = primarySchoolRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(primarySchool);
    }

    /**
     * {@code DELETE  /primary-schools/:id} : delete the "id" primarySchool.
     *
     * @param id the id of the primarySchool to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/primary-schools/{id}")
    public ResponseEntity<Void> deletePrimarySchool(@PathVariable Long id) {
        log.debug("REST request to delete PrimarySchool : {}", id);
        primarySchoolRepository.deleteById(id);
        primarySchoolSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/primary-schools?query=:query} : search for the primarySchool corresponding
     * to the query.
     *
     * @param query the query of the primarySchool search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/primary-schools")
    public ResponseEntity<List<PrimarySchool>> searchPrimarySchools(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of PrimarySchools for query {}", query);
        Page<PrimarySchool> page = primarySchoolSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
