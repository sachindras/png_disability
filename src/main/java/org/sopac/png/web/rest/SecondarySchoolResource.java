package org.sopac.png.web.rest;

import org.sopac.png.domain.SecondarySchool;
import org.sopac.png.repository.SecondarySchoolRepository;
import org.sopac.png.repository.search.SecondarySchoolSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.SecondarySchool}.
 */
@RestController
@RequestMapping("/api")
public class SecondarySchoolResource {

    private final Logger log = LoggerFactory.getLogger(SecondarySchoolResource.class);

    private static final String ENTITY_NAME = "secondarySchool";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SecondarySchoolRepository secondarySchoolRepository;

    private final SecondarySchoolSearchRepository secondarySchoolSearchRepository;

    public SecondarySchoolResource(SecondarySchoolRepository secondarySchoolRepository, SecondarySchoolSearchRepository secondarySchoolSearchRepository) {
        this.secondarySchoolRepository = secondarySchoolRepository;
        this.secondarySchoolSearchRepository = secondarySchoolSearchRepository;
    }

    /**
     * {@code POST  /secondary-schools} : Create a new secondarySchool.
     *
     * @param secondarySchool the secondarySchool to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new secondarySchool, or with status {@code 400 (Bad Request)} if the secondarySchool has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/secondary-schools")
    public ResponseEntity<SecondarySchool> createSecondarySchool(@RequestBody SecondarySchool secondarySchool) throws URISyntaxException {
        log.debug("REST request to save SecondarySchool : {}", secondarySchool);
        if (secondarySchool.getId() != null) {
            throw new BadRequestAlertException("A new secondarySchool cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SecondarySchool result = secondarySchoolRepository.save(secondarySchool);
        secondarySchoolSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/secondary-schools/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /secondary-schools} : Updates an existing secondarySchool.
     *
     * @param secondarySchool the secondarySchool to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated secondarySchool,
     * or with status {@code 400 (Bad Request)} if the secondarySchool is not valid,
     * or with status {@code 500 (Internal Server Error)} if the secondarySchool couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/secondary-schools")
    public ResponseEntity<SecondarySchool> updateSecondarySchool(@RequestBody SecondarySchool secondarySchool) throws URISyntaxException {
        log.debug("REST request to update SecondarySchool : {}", secondarySchool);
        if (secondarySchool.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SecondarySchool result = secondarySchoolRepository.save(secondarySchool);
        secondarySchoolSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, secondarySchool.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /secondary-schools} : get all the secondarySchools.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of secondarySchools in body.
     */
    @GetMapping("/secondary-schools")
    public ResponseEntity<List<SecondarySchool>> getAllSecondarySchools(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of SecondarySchools");
        Page<SecondarySchool> page = secondarySchoolRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /secondary-schools/:id} : get the "id" secondarySchool.
     *
     * @param id the id of the secondarySchool to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the secondarySchool, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/secondary-schools/{id}")
    public ResponseEntity<SecondarySchool> getSecondarySchool(@PathVariable Long id) {
        log.debug("REST request to get SecondarySchool : {}", id);
        Optional<SecondarySchool> secondarySchool = secondarySchoolRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(secondarySchool);
    }

    /**
     * {@code DELETE  /secondary-schools/:id} : delete the "id" secondarySchool.
     *
     * @param id the id of the secondarySchool to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/secondary-schools/{id}")
    public ResponseEntity<Void> deleteSecondarySchool(@PathVariable Long id) {
        log.debug("REST request to delete SecondarySchool : {}", id);
        secondarySchoolRepository.deleteById(id);
        secondarySchoolSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/secondary-schools?query=:query} : search for the secondarySchool corresponding
     * to the query.
     *
     * @param query the query of the secondarySchool search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/secondary-schools")
    public ResponseEntity<List<SecondarySchool>> searchSecondarySchools(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of SecondarySchools for query {}", query);
        Page<SecondarySchool> page = secondarySchoolSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
