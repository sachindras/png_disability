package org.sopac.png.web.rest;

import org.sopac.png.domain.ReligiousInstitute;
import org.sopac.png.repository.ReligiousInstituteRepository;
import org.sopac.png.repository.search.ReligiousInstituteSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.ReligiousInstitute}.
 */
@RestController
@RequestMapping("/api")
public class ReligiousInstituteResource {

    private final Logger log = LoggerFactory.getLogger(ReligiousInstituteResource.class);

    private static final String ENTITY_NAME = "religiousInstitute";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReligiousInstituteRepository religiousInstituteRepository;

    private final ReligiousInstituteSearchRepository religiousInstituteSearchRepository;

    public ReligiousInstituteResource(ReligiousInstituteRepository religiousInstituteRepository, ReligiousInstituteSearchRepository religiousInstituteSearchRepository) {
        this.religiousInstituteRepository = religiousInstituteRepository;
        this.religiousInstituteSearchRepository = religiousInstituteSearchRepository;
    }

    /**
     * {@code POST  /religious-institutes} : Create a new religiousInstitute.
     *
     * @param religiousInstitute the religiousInstitute to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new religiousInstitute, or with status {@code 400 (Bad Request)} if the religiousInstitute has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/religious-institutes")
    public ResponseEntity<ReligiousInstitute> createReligiousInstitute(@RequestBody ReligiousInstitute religiousInstitute) throws URISyntaxException {
        log.debug("REST request to save ReligiousInstitute : {}", religiousInstitute);
        if (religiousInstitute.getId() != null) {
            throw new BadRequestAlertException("A new religiousInstitute cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReligiousInstitute result = religiousInstituteRepository.save(religiousInstitute);
        religiousInstituteSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/religious-institutes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /religious-institutes} : Updates an existing religiousInstitute.
     *
     * @param religiousInstitute the religiousInstitute to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated religiousInstitute,
     * or with status {@code 400 (Bad Request)} if the religiousInstitute is not valid,
     * or with status {@code 500 (Internal Server Error)} if the religiousInstitute couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/religious-institutes")
    public ResponseEntity<ReligiousInstitute> updateReligiousInstitute(@RequestBody ReligiousInstitute religiousInstitute) throws URISyntaxException {
        log.debug("REST request to update ReligiousInstitute : {}", religiousInstitute);
        if (religiousInstitute.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ReligiousInstitute result = religiousInstituteRepository.save(religiousInstitute);
        religiousInstituteSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, religiousInstitute.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /religious-institutes} : get all the religiousInstitutes.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of religiousInstitutes in body.
     */
    @GetMapping("/religious-institutes")
    public ResponseEntity<List<ReligiousInstitute>> getAllReligiousInstitutes(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of ReligiousInstitutes");
        Page<ReligiousInstitute> page = religiousInstituteRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /religious-institutes/:id} : get the "id" religiousInstitute.
     *
     * @param id the id of the religiousInstitute to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the religiousInstitute, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/religious-institutes/{id}")
    public ResponseEntity<ReligiousInstitute> getReligiousInstitute(@PathVariable Long id) {
        log.debug("REST request to get ReligiousInstitute : {}", id);
        Optional<ReligiousInstitute> religiousInstitute = religiousInstituteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(religiousInstitute);
    }

    /**
     * {@code DELETE  /religious-institutes/:id} : delete the "id" religiousInstitute.
     *
     * @param id the id of the religiousInstitute to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/religious-institutes/{id}")
    public ResponseEntity<Void> deleteReligiousInstitute(@PathVariable Long id) {
        log.debug("REST request to delete ReligiousInstitute : {}", id);
        religiousInstituteRepository.deleteById(id);
        religiousInstituteSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/religious-institutes?query=:query} : search for the religiousInstitute corresponding
     * to the query.
     *
     * @param query the query of the religiousInstitute search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/religious-institutes")
    public ResponseEntity<List<ReligiousInstitute>> searchReligiousInstitutes(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of ReligiousInstitutes for query {}", query);
        Page<ReligiousInstitute> page = religiousInstituteSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
