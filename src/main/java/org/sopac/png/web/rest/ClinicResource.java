package org.sopac.png.web.rest;

import org.sopac.png.domain.Clinic;
import org.sopac.png.repository.ClinicRepository;
import org.sopac.png.repository.search.ClinicSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.Clinic}.
 */
@RestController
@RequestMapping("/api")
public class ClinicResource {

    private final Logger log = LoggerFactory.getLogger(ClinicResource.class);

    private static final String ENTITY_NAME = "clinic";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClinicRepository clinicRepository;

    private final ClinicSearchRepository clinicSearchRepository;

    public ClinicResource(ClinicRepository clinicRepository, ClinicSearchRepository clinicSearchRepository) {
        this.clinicRepository = clinicRepository;
        this.clinicSearchRepository = clinicSearchRepository;
    }

    /**
     * {@code POST  /clinics} : Create a new clinic.
     *
     * @param clinic the clinic to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clinic, or with status {@code 400 (Bad Request)} if the clinic has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/clinics")
    public ResponseEntity<Clinic> createClinic(@RequestBody Clinic clinic) throws URISyntaxException {
        log.debug("REST request to save Clinic : {}", clinic);
        if (clinic.getId() != null) {
            throw new BadRequestAlertException("A new clinic cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Clinic result = clinicRepository.save(clinic);
        clinicSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/clinics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /clinics} : Updates an existing clinic.
     *
     * @param clinic the clinic to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clinic,
     * or with status {@code 400 (Bad Request)} if the clinic is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clinic couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/clinics")
    public ResponseEntity<Clinic> updateClinic(@RequestBody Clinic clinic) throws URISyntaxException {
        log.debug("REST request to update Clinic : {}", clinic);
        if (clinic.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Clinic result = clinicRepository.save(clinic);
        clinicSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, clinic.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /clinics} : get all the clinics.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clinics in body.
     */
    @GetMapping("/clinics")
    public ResponseEntity<List<Clinic>> getAllClinics(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Clinics");
        Page<Clinic> page = clinicRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /clinics/:id} : get the "id" clinic.
     *
     * @param id the id of the clinic to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clinic, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/clinics/{id}")
    public ResponseEntity<Clinic> getClinic(@PathVariable Long id) {
        log.debug("REST request to get Clinic : {}", id);
        Optional<Clinic> clinic = clinicRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(clinic);
    }

    /**
     * {@code DELETE  /clinics/:id} : delete the "id" clinic.
     *
     * @param id the id of the clinic to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/clinics/{id}")
    public ResponseEntity<Void> deleteClinic(@PathVariable Long id) {
        log.debug("REST request to delete Clinic : {}", id);
        clinicRepository.deleteById(id);
        clinicSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/clinics?query=:query} : search for the clinic corresponding
     * to the query.
     *
     * @param query the query of the clinic search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/clinics")
    public ResponseEntity<List<Clinic>> searchClinics(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of Clinics for query {}", query);
        Page<Clinic> page = clinicSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
