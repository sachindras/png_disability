package org.sopac.png.web.rest;

import org.sopac.png.domain.HealthCentre;
import org.sopac.png.repository.HealthCentreRepository;
import org.sopac.png.repository.search.HealthCentreSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.HealthCentre}.
 */
@RestController
@RequestMapping("/api")
public class HealthCentreResource {

    private final Logger log = LoggerFactory.getLogger(HealthCentreResource.class);

    private static final String ENTITY_NAME = "healthCentre";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HealthCentreRepository healthCentreRepository;

    private final HealthCentreSearchRepository healthCentreSearchRepository;

    public HealthCentreResource(HealthCentreRepository healthCentreRepository, HealthCentreSearchRepository healthCentreSearchRepository) {
        this.healthCentreRepository = healthCentreRepository;
        this.healthCentreSearchRepository = healthCentreSearchRepository;
    }

    /**
     * {@code POST  /health-centres} : Create a new healthCentre.
     *
     * @param healthCentre the healthCentre to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new healthCentre, or with status {@code 400 (Bad Request)} if the healthCentre has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/health-centres")
    public ResponseEntity<HealthCentre> createHealthCentre(@RequestBody HealthCentre healthCentre) throws URISyntaxException {
        log.debug("REST request to save HealthCentre : {}", healthCentre);
        if (healthCentre.getId() != null) {
            throw new BadRequestAlertException("A new healthCentre cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HealthCentre result = healthCentreRepository.save(healthCentre);
        healthCentreSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/health-centres/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /health-centres} : Updates an existing healthCentre.
     *
     * @param healthCentre the healthCentre to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated healthCentre,
     * or with status {@code 400 (Bad Request)} if the healthCentre is not valid,
     * or with status {@code 500 (Internal Server Error)} if the healthCentre couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/health-centres")
    public ResponseEntity<HealthCentre> updateHealthCentre(@RequestBody HealthCentre healthCentre) throws URISyntaxException {
        log.debug("REST request to update HealthCentre : {}", healthCentre);
        if (healthCentre.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HealthCentre result = healthCentreRepository.save(healthCentre);
        healthCentreSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, healthCentre.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /health-centres} : get all the healthCentres.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of healthCentres in body.
     */
    @GetMapping("/health-centres")
    public ResponseEntity<List<HealthCentre>> getAllHealthCentres(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of HealthCentres");
        Page<HealthCentre> page = healthCentreRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /health-centres/:id} : get the "id" healthCentre.
     *
     * @param id the id of the healthCentre to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the healthCentre, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/health-centres/{id}")
    public ResponseEntity<HealthCentre> getHealthCentre(@PathVariable Long id) {
        log.debug("REST request to get HealthCentre : {}", id);
        Optional<HealthCentre> healthCentre = healthCentreRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(healthCentre);
    }

    /**
     * {@code DELETE  /health-centres/:id} : delete the "id" healthCentre.
     *
     * @param id the id of the healthCentre to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/health-centres/{id}")
    public ResponseEntity<Void> deleteHealthCentre(@PathVariable Long id) {
        log.debug("REST request to delete HealthCentre : {}", id);
        healthCentreRepository.deleteById(id);
        healthCentreSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/health-centres?query=:query} : search for the healthCentre corresponding
     * to the query.
     *
     * @param query the query of the healthCentre search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/health-centres")
    public ResponseEntity<List<HealthCentre>> searchHealthCentres(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of HealthCentres for query {}", query);
        Page<HealthCentre> page = healthCentreSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
