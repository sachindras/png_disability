package org.sopac.png.web.rest;

import org.sopac.png.domain.ContactDetails;
import org.sopac.png.repository.ContactDetailsRepository;
import org.sopac.png.repository.search.ContactDetailsSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.ContactDetails}.
 */
@RestController
@RequestMapping("/api")
public class ContactDetailsResource {

    private final Logger log = LoggerFactory.getLogger(ContactDetailsResource.class);

    private static final String ENTITY_NAME = "contactDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactDetailsRepository contactDetailsRepository;

    private final ContactDetailsSearchRepository contactDetailsSearchRepository;

    public ContactDetailsResource(ContactDetailsRepository contactDetailsRepository, ContactDetailsSearchRepository contactDetailsSearchRepository) {
        this.contactDetailsRepository = contactDetailsRepository;
        this.contactDetailsSearchRepository = contactDetailsSearchRepository;
    }

    /**
     * {@code POST  /contact-details} : Create a new contactDetails.
     *
     * @param contactDetails the contactDetails to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactDetails, or with status {@code 400 (Bad Request)} if the contactDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-details")
    public ResponseEntity<ContactDetails> createContactDetails(@RequestBody ContactDetails contactDetails) throws URISyntaxException {
        log.debug("REST request to save ContactDetails : {}", contactDetails);
        if (contactDetails.getId() != null) {
            throw new BadRequestAlertException("A new contactDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactDetails result = contactDetailsRepository.save(contactDetails);
        contactDetailsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/contact-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-details} : Updates an existing contactDetails.
     *
     * @param contactDetails the contactDetails to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactDetails,
     * or with status {@code 400 (Bad Request)} if the contactDetails is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactDetails couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-details")
    public ResponseEntity<ContactDetails> updateContactDetails(@RequestBody ContactDetails contactDetails) throws URISyntaxException {
        log.debug("REST request to update ContactDetails : {}", contactDetails);
        if (contactDetails.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactDetails result = contactDetailsRepository.save(contactDetails);
        contactDetailsSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, contactDetails.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-details} : get all the contactDetails.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactDetails in body.
     */
    @GetMapping("/contact-details")
    public ResponseEntity<List<ContactDetails>> getAllContactDetails(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of ContactDetails");
        Page<ContactDetails> page = contactDetailsRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-details/:id} : get the "id" contactDetails.
     *
     * @param id the id of the contactDetails to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactDetails, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-details/{id}")
    public ResponseEntity<ContactDetails> getContactDetails(@PathVariable Long id) {
        log.debug("REST request to get ContactDetails : {}", id);
        Optional<ContactDetails> contactDetails = contactDetailsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(contactDetails);
    }

    /**
     * {@code DELETE  /contact-details/:id} : delete the "id" contactDetails.
     *
     * @param id the id of the contactDetails to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-details/{id}")
    public ResponseEntity<Void> deleteContactDetails(@PathVariable Long id) {
        log.debug("REST request to delete ContactDetails : {}", id);
        contactDetailsRepository.deleteById(id);
        contactDetailsSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/contact-details?query=:query} : search for the contactDetails corresponding
     * to the query.
     *
     * @param query the query of the contactDetails search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/contact-details")
    public ResponseEntity<List<ContactDetails>> searchContactDetails(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of ContactDetails for query {}", query);
        Page<ContactDetails> page = contactDetailsSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
