package org.sopac.png.web.rest;

import org.sopac.png.domain.OtherHealthService;
import org.sopac.png.repository.OtherHealthServiceRepository;
import org.sopac.png.repository.search.OtherHealthServiceSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.OtherHealthService}.
 */
@RestController
@RequestMapping("/api")
public class OtherHealthServiceResource {

    private final Logger log = LoggerFactory.getLogger(OtherHealthServiceResource.class);

    private static final String ENTITY_NAME = "otherHealthService";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OtherHealthServiceRepository otherHealthServiceRepository;

    private final OtherHealthServiceSearchRepository otherHealthServiceSearchRepository;

    public OtherHealthServiceResource(OtherHealthServiceRepository otherHealthServiceRepository, OtherHealthServiceSearchRepository otherHealthServiceSearchRepository) {
        this.otherHealthServiceRepository = otherHealthServiceRepository;
        this.otherHealthServiceSearchRepository = otherHealthServiceSearchRepository;
    }

    /**
     * {@code POST  /other-health-services} : Create a new otherHealthService.
     *
     * @param otherHealthService the otherHealthService to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new otherHealthService, or with status {@code 400 (Bad Request)} if the otherHealthService has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/other-health-services")
    public ResponseEntity<OtherHealthService> createOtherHealthService(@RequestBody OtherHealthService otherHealthService) throws URISyntaxException {
        log.debug("REST request to save OtherHealthService : {}", otherHealthService);
        if (otherHealthService.getId() != null) {
            throw new BadRequestAlertException("A new otherHealthService cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OtherHealthService result = otherHealthServiceRepository.save(otherHealthService);
        otherHealthServiceSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/other-health-services/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /other-health-services} : Updates an existing otherHealthService.
     *
     * @param otherHealthService the otherHealthService to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherHealthService,
     * or with status {@code 400 (Bad Request)} if the otherHealthService is not valid,
     * or with status {@code 500 (Internal Server Error)} if the otherHealthService couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/other-health-services")
    public ResponseEntity<OtherHealthService> updateOtherHealthService(@RequestBody OtherHealthService otherHealthService) throws URISyntaxException {
        log.debug("REST request to update OtherHealthService : {}", otherHealthService);
        if (otherHealthService.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OtherHealthService result = otherHealthServiceRepository.save(otherHealthService);
        otherHealthServiceSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, otherHealthService.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /other-health-services} : get all the otherHealthServices.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of otherHealthServices in body.
     */
    @GetMapping("/other-health-services")
    public ResponseEntity<List<OtherHealthService>> getAllOtherHealthServices(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of OtherHealthServices");
        Page<OtherHealthService> page = otherHealthServiceRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /other-health-services/:id} : get the "id" otherHealthService.
     *
     * @param id the id of the otherHealthService to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otherHealthService, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/other-health-services/{id}")
    public ResponseEntity<OtherHealthService> getOtherHealthService(@PathVariable Long id) {
        log.debug("REST request to get OtherHealthService : {}", id);
        Optional<OtherHealthService> otherHealthService = otherHealthServiceRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(otherHealthService);
    }

    /**
     * {@code DELETE  /other-health-services/:id} : delete the "id" otherHealthService.
     *
     * @param id the id of the otherHealthService to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/other-health-services/{id}")
    public ResponseEntity<Void> deleteOtherHealthService(@PathVariable Long id) {
        log.debug("REST request to delete OtherHealthService : {}", id);
        otherHealthServiceRepository.deleteById(id);
        otherHealthServiceSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/other-health-services?query=:query} : search for the otherHealthService corresponding
     * to the query.
     *
     * @param query the query of the otherHealthService search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/other-health-services")
    public ResponseEntity<List<OtherHealthService>> searchOtherHealthServices(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of OtherHealthServices for query {}", query);
        Page<OtherHealthService> page = otherHealthServiceSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
