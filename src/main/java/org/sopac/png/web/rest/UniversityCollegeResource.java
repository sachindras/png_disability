package org.sopac.png.web.rest;

import org.sopac.png.domain.UniversityCollege;
import org.sopac.png.repository.UniversityCollegeRepository;
import org.sopac.png.repository.search.UniversityCollegeSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.UniversityCollege}.
 */
@RestController
@RequestMapping("/api")
public class UniversityCollegeResource {

    private final Logger log = LoggerFactory.getLogger(UniversityCollegeResource.class);

    private static final String ENTITY_NAME = "universityCollege";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UniversityCollegeRepository universityCollegeRepository;

    private final UniversityCollegeSearchRepository universityCollegeSearchRepository;

    public UniversityCollegeResource(UniversityCollegeRepository universityCollegeRepository, UniversityCollegeSearchRepository universityCollegeSearchRepository) {
        this.universityCollegeRepository = universityCollegeRepository;
        this.universityCollegeSearchRepository = universityCollegeSearchRepository;
    }

    /**
     * {@code POST  /university-colleges} : Create a new universityCollege.
     *
     * @param universityCollege the universityCollege to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new universityCollege, or with status {@code 400 (Bad Request)} if the universityCollege has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/university-colleges")
    public ResponseEntity<UniversityCollege> createUniversityCollege(@RequestBody UniversityCollege universityCollege) throws URISyntaxException {
        log.debug("REST request to save UniversityCollege : {}", universityCollege);
        if (universityCollege.getId() != null) {
            throw new BadRequestAlertException("A new universityCollege cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UniversityCollege result = universityCollegeRepository.save(universityCollege);
        universityCollegeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/university-colleges/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /university-colleges} : Updates an existing universityCollege.
     *
     * @param universityCollege the universityCollege to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated universityCollege,
     * or with status {@code 400 (Bad Request)} if the universityCollege is not valid,
     * or with status {@code 500 (Internal Server Error)} if the universityCollege couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/university-colleges")
    public ResponseEntity<UniversityCollege> updateUniversityCollege(@RequestBody UniversityCollege universityCollege) throws URISyntaxException {
        log.debug("REST request to update UniversityCollege : {}", universityCollege);
        if (universityCollege.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UniversityCollege result = universityCollegeRepository.save(universityCollege);
        universityCollegeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, universityCollege.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /university-colleges} : get all the universityColleges.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of universityColleges in body.
     */
    @GetMapping("/university-colleges")
    public ResponseEntity<List<UniversityCollege>> getAllUniversityColleges(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of UniversityColleges");
        Page<UniversityCollege> page = universityCollegeRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /university-colleges/:id} : get the "id" universityCollege.
     *
     * @param id the id of the universityCollege to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the universityCollege, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/university-colleges/{id}")
    public ResponseEntity<UniversityCollege> getUniversityCollege(@PathVariable Long id) {
        log.debug("REST request to get UniversityCollege : {}", id);
        Optional<UniversityCollege> universityCollege = universityCollegeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(universityCollege);
    }

    /**
     * {@code DELETE  /university-colleges/:id} : delete the "id" universityCollege.
     *
     * @param id the id of the universityCollege to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/university-colleges/{id}")
    public ResponseEntity<Void> deleteUniversityCollege(@PathVariable Long id) {
        log.debug("REST request to delete UniversityCollege : {}", id);
        universityCollegeRepository.deleteById(id);
        universityCollegeSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/university-colleges?query=:query} : search for the universityCollege corresponding
     * to the query.
     *
     * @param query the query of the universityCollege search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/university-colleges")
    public ResponseEntity<List<UniversityCollege>> searchUniversityColleges(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of UniversityColleges for query {}", query);
        Page<UniversityCollege> page = universityCollegeSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
