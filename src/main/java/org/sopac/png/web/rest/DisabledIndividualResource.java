package org.sopac.png.web.rest;

import org.sopac.png.domain.DisabledIndividual;
import org.sopac.png.repository.DisabledIndividualRepository;
import org.sopac.png.repository.search.DisabledIndividualSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.DisabledIndividual}.
 */
@RestController
@RequestMapping("/api")
public class DisabledIndividualResource {

    private final Logger log = LoggerFactory.getLogger(DisabledIndividualResource.class);

    private static final String ENTITY_NAME = "disabledIndividual";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DisabledIndividualRepository disabledIndividualRepository;

    private final DisabledIndividualSearchRepository disabledIndividualSearchRepository;

    public DisabledIndividualResource(DisabledIndividualRepository disabledIndividualRepository, DisabledIndividualSearchRepository disabledIndividualSearchRepository) {
        this.disabledIndividualRepository = disabledIndividualRepository;
        this.disabledIndividualSearchRepository = disabledIndividualSearchRepository;
    }

    /**
     * {@code POST  /disabled-individuals} : Create a new disabledIndividual.
     *
     * @param disabledIndividual the disabledIndividual to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new disabledIndividual, or with status {@code 400 (Bad Request)} if the disabledIndividual has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/disabled-individuals")
    public ResponseEntity<DisabledIndividual> createDisabledIndividual(@RequestBody DisabledIndividual disabledIndividual) throws URISyntaxException {
        log.debug("REST request to save DisabledIndividual : {}", disabledIndividual);
        if (disabledIndividual.getId() != null) {
            throw new BadRequestAlertException("A new disabledIndividual cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DisabledIndividual result = disabledIndividualRepository.save(disabledIndividual);
        disabledIndividualSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/disabled-individuals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /disabled-individuals} : Updates an existing disabledIndividual.
     *
     * @param disabledIndividual the disabledIndividual to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated disabledIndividual,
     * or with status {@code 400 (Bad Request)} if the disabledIndividual is not valid,
     * or with status {@code 500 (Internal Server Error)} if the disabledIndividual couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/disabled-individuals")
    public ResponseEntity<DisabledIndividual> updateDisabledIndividual(@RequestBody DisabledIndividual disabledIndividual) throws URISyntaxException {
        log.debug("REST request to update DisabledIndividual : {}", disabledIndividual);
        if (disabledIndividual.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DisabledIndividual result = disabledIndividualRepository.save(disabledIndividual);
        disabledIndividualSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, disabledIndividual.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /disabled-individuals} : get all the disabledIndividuals.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of disabledIndividuals in body.
     */
    @GetMapping("/disabled-individuals")
    public ResponseEntity<List<DisabledIndividual>> getAllDisabledIndividuals(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of DisabledIndividuals");
        Page<DisabledIndividual> page = disabledIndividualRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /disabled-individuals/:id} : get the "id" disabledIndividual.
     *
     * @param id the id of the disabledIndividual to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the disabledIndividual, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/disabled-individuals/{id}")
    public ResponseEntity<DisabledIndividual> getDisabledIndividual(@PathVariable Long id) {
        log.debug("REST request to get DisabledIndividual : {}", id);
        Optional<DisabledIndividual> disabledIndividual = disabledIndividualRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(disabledIndividual);
    }

    /**
     * {@code DELETE  /disabled-individuals/:id} : delete the "id" disabledIndividual.
     *
     * @param id the id of the disabledIndividual to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/disabled-individuals/{id}")
    public ResponseEntity<Void> deleteDisabledIndividual(@PathVariable Long id) {
        log.debug("REST request to delete DisabledIndividual : {}", id);
        disabledIndividualRepository.deleteById(id);
        disabledIndividualSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/disabled-individuals?query=:query} : search for the disabledIndividual corresponding
     * to the query.
     *
     * @param query the query of the disabledIndividual search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/disabled-individuals")
    public ResponseEntity<List<DisabledIndividual>> searchDisabledIndividuals(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of DisabledIndividuals for query {}", query);
        Page<DisabledIndividual> page = disabledIndividualSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
