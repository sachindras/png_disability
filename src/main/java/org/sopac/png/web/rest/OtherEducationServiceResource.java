package org.sopac.png.web.rest;

import org.sopac.png.domain.OtherEducationService;
import org.sopac.png.repository.OtherEducationServiceRepository;
import org.sopac.png.repository.search.OtherEducationServiceSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.OtherEducationService}.
 */
@RestController
@RequestMapping("/api")
public class OtherEducationServiceResource {

    private final Logger log = LoggerFactory.getLogger(OtherEducationServiceResource.class);

    private static final String ENTITY_NAME = "otherEducationService";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OtherEducationServiceRepository otherEducationServiceRepository;

    private final OtherEducationServiceSearchRepository otherEducationServiceSearchRepository;

    public OtherEducationServiceResource(OtherEducationServiceRepository otherEducationServiceRepository, OtherEducationServiceSearchRepository otherEducationServiceSearchRepository) {
        this.otherEducationServiceRepository = otherEducationServiceRepository;
        this.otherEducationServiceSearchRepository = otherEducationServiceSearchRepository;
    }

    /**
     * {@code POST  /other-education-services} : Create a new otherEducationService.
     *
     * @param otherEducationService the otherEducationService to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new otherEducationService, or with status {@code 400 (Bad Request)} if the otherEducationService has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/other-education-services")
    public ResponseEntity<OtherEducationService> createOtherEducationService(@RequestBody OtherEducationService otherEducationService) throws URISyntaxException {
        log.debug("REST request to save OtherEducationService : {}", otherEducationService);
        if (otherEducationService.getId() != null) {
            throw new BadRequestAlertException("A new otherEducationService cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OtherEducationService result = otherEducationServiceRepository.save(otherEducationService);
        otherEducationServiceSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/other-education-services/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /other-education-services} : Updates an existing otherEducationService.
     *
     * @param otherEducationService the otherEducationService to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherEducationService,
     * or with status {@code 400 (Bad Request)} if the otherEducationService is not valid,
     * or with status {@code 500 (Internal Server Error)} if the otherEducationService couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/other-education-services")
    public ResponseEntity<OtherEducationService> updateOtherEducationService(@RequestBody OtherEducationService otherEducationService) throws URISyntaxException {
        log.debug("REST request to update OtherEducationService : {}", otherEducationService);
        if (otherEducationService.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OtherEducationService result = otherEducationServiceRepository.save(otherEducationService);
        otherEducationServiceSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, otherEducationService.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /other-education-services} : get all the otherEducationServices.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of otherEducationServices in body.
     */
    @GetMapping("/other-education-services")
    public ResponseEntity<List<OtherEducationService>> getAllOtherEducationServices(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of OtherEducationServices");
        Page<OtherEducationService> page = otherEducationServiceRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /other-education-services/:id} : get the "id" otherEducationService.
     *
     * @param id the id of the otherEducationService to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otherEducationService, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/other-education-services/{id}")
    public ResponseEntity<OtherEducationService> getOtherEducationService(@PathVariable Long id) {
        log.debug("REST request to get OtherEducationService : {}", id);
        Optional<OtherEducationService> otherEducationService = otherEducationServiceRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(otherEducationService);
    }

    /**
     * {@code DELETE  /other-education-services/:id} : delete the "id" otherEducationService.
     *
     * @param id the id of the otherEducationService to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/other-education-services/{id}")
    public ResponseEntity<Void> deleteOtherEducationService(@PathVariable Long id) {
        log.debug("REST request to delete OtherEducationService : {}", id);
        otherEducationServiceRepository.deleteById(id);
        otherEducationServiceSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/other-education-services?query=:query} : search for the otherEducationService corresponding
     * to the query.
     *
     * @param query the query of the otherEducationService search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/other-education-services")
    public ResponseEntity<List<OtherEducationService>> searchOtherEducationServices(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of OtherEducationServices for query {}", query);
        Page<OtherEducationService> page = otherEducationServiceSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
