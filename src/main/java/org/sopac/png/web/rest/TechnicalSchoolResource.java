package org.sopac.png.web.rest;

import org.sopac.png.domain.TechnicalSchool;
import org.sopac.png.repository.TechnicalSchoolRepository;
import org.sopac.png.repository.search.TechnicalSchoolSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.TechnicalSchool}.
 */
@RestController
@RequestMapping("/api")
public class TechnicalSchoolResource {

    private final Logger log = LoggerFactory.getLogger(TechnicalSchoolResource.class);

    private static final String ENTITY_NAME = "technicalSchool";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TechnicalSchoolRepository technicalSchoolRepository;

    private final TechnicalSchoolSearchRepository technicalSchoolSearchRepository;

    public TechnicalSchoolResource(TechnicalSchoolRepository technicalSchoolRepository, TechnicalSchoolSearchRepository technicalSchoolSearchRepository) {
        this.technicalSchoolRepository = technicalSchoolRepository;
        this.technicalSchoolSearchRepository = technicalSchoolSearchRepository;
    }

    /**
     * {@code POST  /technical-schools} : Create a new technicalSchool.
     *
     * @param technicalSchool the technicalSchool to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new technicalSchool, or with status {@code 400 (Bad Request)} if the technicalSchool has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/technical-schools")
    public ResponseEntity<TechnicalSchool> createTechnicalSchool(@RequestBody TechnicalSchool technicalSchool) throws URISyntaxException {
        log.debug("REST request to save TechnicalSchool : {}", technicalSchool);
        if (technicalSchool.getId() != null) {
            throw new BadRequestAlertException("A new technicalSchool cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TechnicalSchool result = technicalSchoolRepository.save(technicalSchool);
        technicalSchoolSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/technical-schools/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /technical-schools} : Updates an existing technicalSchool.
     *
     * @param technicalSchool the technicalSchool to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated technicalSchool,
     * or with status {@code 400 (Bad Request)} if the technicalSchool is not valid,
     * or with status {@code 500 (Internal Server Error)} if the technicalSchool couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/technical-schools")
    public ResponseEntity<TechnicalSchool> updateTechnicalSchool(@RequestBody TechnicalSchool technicalSchool) throws URISyntaxException {
        log.debug("REST request to update TechnicalSchool : {}", technicalSchool);
        if (technicalSchool.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TechnicalSchool result = technicalSchoolRepository.save(technicalSchool);
        technicalSchoolSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, technicalSchool.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /technical-schools} : get all the technicalSchools.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of technicalSchools in body.
     */
    @GetMapping("/technical-schools")
    public ResponseEntity<List<TechnicalSchool>> getAllTechnicalSchools(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of TechnicalSchools");
        Page<TechnicalSchool> page = technicalSchoolRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /technical-schools/:id} : get the "id" technicalSchool.
     *
     * @param id the id of the technicalSchool to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the technicalSchool, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/technical-schools/{id}")
    public ResponseEntity<TechnicalSchool> getTechnicalSchool(@PathVariable Long id) {
        log.debug("REST request to get TechnicalSchool : {}", id);
        Optional<TechnicalSchool> technicalSchool = technicalSchoolRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(technicalSchool);
    }

    /**
     * {@code DELETE  /technical-schools/:id} : delete the "id" technicalSchool.
     *
     * @param id the id of the technicalSchool to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/technical-schools/{id}")
    public ResponseEntity<Void> deleteTechnicalSchool(@PathVariable Long id) {
        log.debug("REST request to delete TechnicalSchool : {}", id);
        technicalSchoolRepository.deleteById(id);
        technicalSchoolSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/technical-schools?query=:query} : search for the technicalSchool corresponding
     * to the query.
     *
     * @param query the query of the technicalSchool search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/technical-schools")
    public ResponseEntity<List<TechnicalSchool>> searchTechnicalSchools(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of TechnicalSchools for query {}", query);
        Page<TechnicalSchool> page = technicalSchoolSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
