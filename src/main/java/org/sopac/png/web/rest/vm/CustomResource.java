package org.sopac.png.web.rest.vm;

import org.sopac.png.domain.DisabledIndividual;
import org.sopac.png.domain.ReligiousInstitute;
import org.sopac.png.repository.DisabledIndividualRepository;
import org.sopac.png.repository.ReligiousInstituteRepository;
import org.sopac.png.repository.search.DisabledIndividualSearchRepository;
import org.sopac.png.repository.search.ReligiousInstituteSearchRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/custom")
public class CustomResource {

    private final DisabledIndividualRepository disabledIndividual;
    private final DisabledIndividualSearchRepository disabledIndividualSearchRepository;
    private final ReligiousInstituteRepository religiousInstitute;
    private final ReligiousInstituteSearchRepository religiousInstituteSearchRepository;

    public CustomResource(DisabledIndividualRepository disabledIndividual, ReligiousInstituteRepository religiousInstitute, DisabledIndividualSearchRepository disabledIndividualSearchRepository, ReligiousInstituteSearchRepository religiousInstituteSearchRepository) {
        this.disabledIndividual = disabledIndividual;
        this.religiousInstitute = religiousInstitute;
        this.disabledIndividualSearchRepository = disabledIndividualSearchRepository;
        this.religiousInstituteSearchRepository = religiousInstituteSearchRepository;
    }

    @GetMapping("/reindex")
    public boolean reIndex() {
        disabledIndividualSearchRepository.deleteAll();
        disabledIndividual.findAll().forEach(i -> disabledIndividualSearchRepository.index(i));
        religiousInstituteSearchRepository.deleteAll();
        religiousInstitute.findAll().forEach(i -> religiousInstituteSearchRepository.index(i));
        return true;
    }

    @GetMapping("/test")
    public String test() {
        return "Hello @ " + new Date().toString();
    }

}
