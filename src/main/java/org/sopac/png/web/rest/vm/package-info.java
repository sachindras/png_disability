/**
 * View Models used by Spring MVC REST controllers.
 */
package org.sopac.png.web.rest.vm;
