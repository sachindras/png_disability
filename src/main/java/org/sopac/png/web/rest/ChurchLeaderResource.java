package org.sopac.png.web.rest;

import org.sopac.png.domain.ChurchLeader;
import org.sopac.png.repository.ChurchLeaderRepository;
import org.sopac.png.repository.search.ChurchLeaderSearchRepository;
import org.sopac.png.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link org.sopac.png.domain.ChurchLeader}.
 */
@RestController
@RequestMapping("/api")
public class ChurchLeaderResource {

    private final Logger log = LoggerFactory.getLogger(ChurchLeaderResource.class);

    private static final String ENTITY_NAME = "churchLeader";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChurchLeaderRepository churchLeaderRepository;

    private final ChurchLeaderSearchRepository churchLeaderSearchRepository;

    public ChurchLeaderResource(ChurchLeaderRepository churchLeaderRepository, ChurchLeaderSearchRepository churchLeaderSearchRepository) {
        this.churchLeaderRepository = churchLeaderRepository;
        this.churchLeaderSearchRepository = churchLeaderSearchRepository;
    }

    /**
     * {@code POST  /church-leaders} : Create a new churchLeader.
     *
     * @param churchLeader the churchLeader to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new churchLeader, or with status {@code 400 (Bad Request)} if the churchLeader has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/church-leaders")
    public ResponseEntity<ChurchLeader> createChurchLeader(@RequestBody ChurchLeader churchLeader) throws URISyntaxException {
        log.debug("REST request to save ChurchLeader : {}", churchLeader);
        if (churchLeader.getId() != null) {
            throw new BadRequestAlertException("A new churchLeader cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChurchLeader result = churchLeaderRepository.save(churchLeader);
        churchLeaderSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/church-leaders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /church-leaders} : Updates an existing churchLeader.
     *
     * @param churchLeader the churchLeader to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated churchLeader,
     * or with status {@code 400 (Bad Request)} if the churchLeader is not valid,
     * or with status {@code 500 (Internal Server Error)} if the churchLeader couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/church-leaders")
    public ResponseEntity<ChurchLeader> updateChurchLeader(@RequestBody ChurchLeader churchLeader) throws URISyntaxException {
        log.debug("REST request to update ChurchLeader : {}", churchLeader);
        if (churchLeader.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChurchLeader result = churchLeaderRepository.save(churchLeader);
        churchLeaderSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, churchLeader.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /church-leaders} : get all the churchLeaders.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of churchLeaders in body.
     */
    @GetMapping("/church-leaders")
    public ResponseEntity<List<ChurchLeader>> getAllChurchLeaders(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of ChurchLeaders");
        Page<ChurchLeader> page = churchLeaderRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /church-leaders/:id} : get the "id" churchLeader.
     *
     * @param id the id of the churchLeader to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the churchLeader, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/church-leaders/{id}")
    public ResponseEntity<ChurchLeader> getChurchLeader(@PathVariable Long id) {
        log.debug("REST request to get ChurchLeader : {}", id);
        Optional<ChurchLeader> churchLeader = churchLeaderRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(churchLeader);
    }

    /**
     * {@code DELETE  /church-leaders/:id} : delete the "id" churchLeader.
     *
     * @param id the id of the churchLeader to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/church-leaders/{id}")
    public ResponseEntity<Void> deleteChurchLeader(@PathVariable Long id) {
        log.debug("REST request to delete ChurchLeader : {}", id);
        churchLeaderRepository.deleteById(id);
        churchLeaderSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/church-leaders?query=:query} : search for the churchLeader corresponding
     * to the query.
     *
     * @param query the query of the churchLeader search.
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the result of the search.
     */
    @GetMapping("/_search/church-leaders")
    public ResponseEntity<List<ChurchLeader>> searchChurchLeaders(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of ChurchLeaders for query {}", query);
        Page<ChurchLeader> page = churchLeaderSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
