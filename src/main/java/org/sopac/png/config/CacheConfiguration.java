package org.sopac.png.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, org.sopac.png.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, org.sopac.png.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, org.sopac.png.domain.User.class.getName());
            createCache(cm, org.sopac.png.domain.Authority.class.getName());
            createCache(cm, org.sopac.png.domain.User.class.getName() + ".authorities");
            createCache(cm, org.sopac.png.domain.ReligiousInstitute.class.getName());
            createCache(cm, org.sopac.png.domain.ReligiousInstitute.class.getName() + ".churchLeaders");
            createCache(cm, org.sopac.png.domain.ReligiousInstitute.class.getName() + ".churchHospitals");
            createCache(cm, org.sopac.png.domain.ReligiousInstitute.class.getName() + ".churchClinics");
            createCache(cm, org.sopac.png.domain.ReligiousInstitute.class.getName() + ".churchOtherHealthServices");
            createCache(cm, org.sopac.png.domain.ReligiousInstitute.class.getName() + ".churchUniversityColleges");
            createCache(cm, org.sopac.png.domain.ReligiousInstitute.class.getName() + ".churchTechnicalSchools");
            createCache(cm, org.sopac.png.domain.ReligiousInstitute.class.getName() + ".churchSecondarySchools");
            createCache(cm, org.sopac.png.domain.ReligiousInstitute.class.getName() + ".churchPrimarySchools");
            createCache(cm, org.sopac.png.domain.ReligiousInstitute.class.getName() + ".churchOtherEducationServices");
            createCache(cm, org.sopac.png.domain.ChurchLeader.class.getName());
            createCache(cm, org.sopac.png.domain.Hospital.class.getName());
            createCache(cm, org.sopac.png.domain.HealthCentre.class.getName());
            createCache(cm, org.sopac.png.domain.Clinic.class.getName());
            createCache(cm, org.sopac.png.domain.OtherHealthService.class.getName());
            createCache(cm, org.sopac.png.domain.UniversityCollege.class.getName());
            createCache(cm, org.sopac.png.domain.TechnicalSchool.class.getName());
            createCache(cm, org.sopac.png.domain.SecondarySchool.class.getName());
            createCache(cm, org.sopac.png.domain.PrimarySchool.class.getName());
            createCache(cm, org.sopac.png.domain.OtherEducationService.class.getName());
            createCache(cm, org.sopac.png.domain.DisabledIndividual.class.getName());
            createCache(cm, org.sopac.png.domain.ContactDetails.class.getName());
            createCache(cm, org.sopac.png.domain.Location.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cm.destroyCache(cacheName);
        }
        cm.createCache(cacheName, jcacheConfiguration);
    }
}
