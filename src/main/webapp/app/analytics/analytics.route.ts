import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core';
import { AnalyticsComponent } from './analytics.component';

export const ANALYTICS_ROUTE: Route = {
  path: 'analytics',
  component: AnalyticsComponent,
  data: {
    authorities: [],
    pageTitle: 'PNG Disability Analytics'
  },
  canActivate: [UserRouteAccessService]
};
