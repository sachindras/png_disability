import { Component, OnInit } from '@angular/core';
import { SingleSeries } from '@swimlane/ngx-charts';
import { formatLabel } from '@swimlane/ngx-charts';

@Component({
  selector: 'jhi-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {
  single1: SingleSeries = [
    {
      name: 'Health',
      value: 40632,
      extra: {
        code: 'de'
      }
    },
    {
      name: 'Education',
      value: 50000,
      extra: {
        code: 'us'
      }
    },
    {
      name: 'Law and Justice',
      value: 36745,
      extra: {
        code: 'fr'
      }
    },
    {
      name: 'Welfare and Counselling',
      value: 36240,
      extra: {
        code: 'uk'
      }
    },
    {
      name: 'Sports',
      value: 33000,
      extra: {
        code: 'es'
      }
    },
    {
      name: 'Road and Transport',
      value: 35800,
      extra: {
        code: 'rt'
      }
    },
    {
      name: 'Market and Necessities',
      value: 45800,
      extra: {
        code: 'mn'
      }
    },
    {
      name: 'Banking and Finance',
      value: 15800,
      extra: {
        code: 'bf'
      }
    },
    {
      name: 'Telecommunications and Assistive Technologies',
      value: 12800,
      extra: {
        code: 'ta'
      }
    }
  ];

  single: SingleSeries = [
    {
      name: 'Seeing Difficulty',
      value: 40632,
      extra: {
        code: 'de'
      }
    },
    {
      name: 'Hearing Difficulty',
      value: 50000,
      extra: {
        code: 'us'
      }
    },
    {
      name: 'Walking Difficulty',
      value: 36745,
      extra: {
        code: 'fr'
      }
    },
    {
      name: 'Memory Difficulty',
      value: 36240,
      extra: {
        code: 'uk'
      }
    },
    {
      name: 'Self-Care Difficulty',
      value: 33000,
      extra: {
        code: 'es'
      }
    },
    {
      name: 'Communications Difficulty',
      value: 35800,
      extra: {
        code: 'it'
      }
    }
  ];

  message: string;

  theme = 'dark';
  chartType: string;
  chartGroups: any[];
  chart: any;
  realTimeData: boolean = false;
  countries: any[];
  //single: any[];
  multi: any[];
  fiscalYearReport: any[];
  dateData: any[];
  dateDataWithRange: any[];
  calendarData: any[];
  statusData: any[];
  sparklineData: any[];
  timelineFilterBarData: any[];
  graph: { links: any[]; nodes: any[] };
  bubble: any;
  linearScale: boolean = false;
  range: boolean = false;

  view: any[];
  width: number = 700;
  height: number = 300;
  fitContainer: boolean = false;

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  legendTitle = 'Legend';
  legendPosition = 'right';
  showXAxisLabel = true;
  tooltipDisabled = false;
  showText = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'GDP Per Capita';
  showGridLines = true;
  innerPadding = '10%';
  barPadding = 8;
  groupPadding = 16;
  roundDomains = false;
  maxRadius = 10;
  minRadius = 3;
  showSeriesOnHover = true;
  roundEdges: boolean = true;
  animations: boolean = true;
  xScaleMin: any;
  xScaleMax: any;
  yScaleMin: number;
  yScaleMax: number;
  showDataLabel = false;
  noBarWhenZero = true;
  trimXAxisTicks = true;
  trimYAxisTicks = true;
  rotateXAxisTicks = true;
  maxXAxisTickLength = 16;
  maxYAxisTickLength = 16;

  colorSets: any;
  //colorScheme: any;
  schemeType: string = 'ordinal';
  selectedColorScheme: string;
  rangeFillOpacity: number = 0.15;

  // pie
  showLabels = true;
  explodeSlices = false;
  doughnut = false;
  arcWidth = 0.25;

  // line, area
  autoScale = true;
  timeline = false;

  // margin
  margin: boolean = false;
  marginTop: number = 40;
  marginRight: number = 40;
  marginBottom: number = 40;
  marginLeft: number = 40;

  // gauge
  gaugeMin: number = 0;
  gaugeMax: number = 100;
  gaugeLargeSegments: number = 10;
  gaugeSmallSegments: number = 5;
  gaugeTextValue: string = '';
  gaugeUnits: string = 'alerts';
  gaugeAngleSpan: number = 240;
  gaugeStartAngle: number = -120;
  gaugeShowAxis: boolean = true;
  gaugeValue: number = 50; // linear gauge value
  gaugePreviousValue: number = 70;

  // heatmap
  heatmapMin: number = 0;
  heatmapMax: number = 50000;

  constructor() {
    this.message = 'Still In Development';
  }

  ngOnInit() {}

  pieTooltipText({ data }) {
    const label = formatLabel(data.name);
    const val = formatLabel(data.value);

    return `
      <span class="tooltip-label">${label}</span>
      <span class="tooltip-val">${val}</span>
    `;
  }

  public colorScheme = {
    domain: ['#52adc8', '#5AA454', '#A10A28', '#C7B42C', '#F65D22', '#09569d', '#62c462'] // or whatever colors you want
  };
}
