import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { PngDisabilityReligionSharedModule } from '../shared';

import { ANALYTICS_ROUTE, AnalyticsComponent } from './';

@NgModule({
  imports: [PngDisabilityReligionSharedModule, NgxChartsModule, RouterModule.forRoot([ANALYTICS_ROUTE], { useHash: true })],
  declarations: [AnalyticsComponent],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionAppAnalyticsModule {}
