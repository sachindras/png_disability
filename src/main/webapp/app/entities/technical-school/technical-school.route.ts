import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TechnicalSchool } from 'app/shared/model/technical-school.model';
import { TechnicalSchoolService } from './technical-school.service';
import { TechnicalSchoolComponent } from './technical-school.component';
import { TechnicalSchoolDetailComponent } from './technical-school-detail.component';
import { TechnicalSchoolUpdateComponent } from './technical-school-update.component';
import { TechnicalSchoolDeletePopupComponent } from './technical-school-delete-dialog.component';
import { ITechnicalSchool } from 'app/shared/model/technical-school.model';

@Injectable({ providedIn: 'root' })
export class TechnicalSchoolResolve implements Resolve<ITechnicalSchool> {
  constructor(private service: TechnicalSchoolService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITechnicalSchool> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TechnicalSchool>) => response.ok),
        map((technicalSchool: HttpResponse<TechnicalSchool>) => technicalSchool.body)
      );
    }
    return of(new TechnicalSchool());
  }
}

export const technicalSchoolRoute: Routes = [
  {
    path: '',
    component: TechnicalSchoolComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TechnicalSchools'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TechnicalSchoolDetailComponent,
    resolve: {
      technicalSchool: TechnicalSchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TechnicalSchools'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TechnicalSchoolUpdateComponent,
    resolve: {
      technicalSchool: TechnicalSchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TechnicalSchools'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TechnicalSchoolUpdateComponent,
    resolve: {
      technicalSchool: TechnicalSchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TechnicalSchools'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const technicalSchoolPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TechnicalSchoolDeletePopupComponent,
    resolve: {
      technicalSchool: TechnicalSchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TechnicalSchools'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
