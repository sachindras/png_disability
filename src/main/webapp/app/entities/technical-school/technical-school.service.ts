import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITechnicalSchool } from 'app/shared/model/technical-school.model';

type EntityResponseType = HttpResponse<ITechnicalSchool>;
type EntityArrayResponseType = HttpResponse<ITechnicalSchool[]>;

@Injectable({ providedIn: 'root' })
export class TechnicalSchoolService {
  public resourceUrl = SERVER_API_URL + 'api/technical-schools';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/technical-schools';

  constructor(protected http: HttpClient) {}

  create(technicalSchool: ITechnicalSchool): Observable<EntityResponseType> {
    return this.http.post<ITechnicalSchool>(this.resourceUrl, technicalSchool, { observe: 'response' });
  }

  update(technicalSchool: ITechnicalSchool): Observable<EntityResponseType> {
    return this.http.put<ITechnicalSchool>(this.resourceUrl, technicalSchool, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITechnicalSchool>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITechnicalSchool[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITechnicalSchool[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
