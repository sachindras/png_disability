export * from './technical-school.service';
export * from './technical-school-update.component';
export * from './technical-school-delete-dialog.component';
export * from './technical-school-detail.component';
export * from './technical-school.component';
export * from './technical-school.route';
