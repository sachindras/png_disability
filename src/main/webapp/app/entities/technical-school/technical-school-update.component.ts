import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ITechnicalSchool, TechnicalSchool } from 'app/shared/model/technical-school.model';
import { TechnicalSchoolService } from './technical-school.service';
import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';
import { ReligiousInstituteService } from 'app/entities/religious-institute';

@Component({
  selector: 'jhi-technical-school-update',
  templateUrl: './technical-school-update.component.html'
})
export class TechnicalSchoolUpdateComponent implements OnInit {
  isSaving: boolean;

  religiousinstitutes: IReligiousInstitute[];

  editForm = this.fb.group({
    id: [],
    name: [],
    province: [],
    district: [],
    ward: [],
    active: [],
    religiousInstitute: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected technicalSchoolService: TechnicalSchoolService,
    protected religiousInstituteService: ReligiousInstituteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ technicalSchool }) => {
      this.updateForm(technicalSchool);
    });
    this.religiousInstituteService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IReligiousInstitute[]>) => mayBeOk.ok),
        map((response: HttpResponse<IReligiousInstitute[]>) => response.body)
      )
      .subscribe((res: IReligiousInstitute[]) => (this.religiousinstitutes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(technicalSchool: ITechnicalSchool) {
    this.editForm.patchValue({
      id: technicalSchool.id,
      name: technicalSchool.name,
      province: technicalSchool.province,
      district: technicalSchool.district,
      ward: technicalSchool.ward,
      active: technicalSchool.active,
      religiousInstitute: technicalSchool.religiousInstitute
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const technicalSchool = this.createFromForm();
    if (technicalSchool.id !== undefined) {
      this.subscribeToSaveResponse(this.technicalSchoolService.update(technicalSchool));
    } else {
      this.subscribeToSaveResponse(this.technicalSchoolService.create(technicalSchool));
    }
  }

  private createFromForm(): ITechnicalSchool {
    return {
      ...new TechnicalSchool(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      province: this.editForm.get(['province']).value,
      district: this.editForm.get(['district']).value,
      ward: this.editForm.get(['ward']).value,
      active: this.editForm.get(['active']).value,
      religiousInstitute: this.editForm.get(['religiousInstitute']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITechnicalSchool>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackReligiousInstituteById(index: number, item: IReligiousInstitute) {
    return item.id;
  }
}
