import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  TechnicalSchoolComponent,
  TechnicalSchoolDetailComponent,
  TechnicalSchoolUpdateComponent,
  TechnicalSchoolDeletePopupComponent,
  TechnicalSchoolDeleteDialogComponent,
  technicalSchoolRoute,
  technicalSchoolPopupRoute
} from './';

const ENTITY_STATES = [...technicalSchoolRoute, ...technicalSchoolPopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TechnicalSchoolComponent,
    TechnicalSchoolDetailComponent,
    TechnicalSchoolUpdateComponent,
    TechnicalSchoolDeleteDialogComponent,
    TechnicalSchoolDeletePopupComponent
  ],
  entryComponents: [
    TechnicalSchoolComponent,
    TechnicalSchoolUpdateComponent,
    TechnicalSchoolDeleteDialogComponent,
    TechnicalSchoolDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionTechnicalSchoolModule {}
