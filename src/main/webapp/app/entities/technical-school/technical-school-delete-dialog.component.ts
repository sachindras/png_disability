import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITechnicalSchool } from 'app/shared/model/technical-school.model';
import { TechnicalSchoolService } from './technical-school.service';

@Component({
  selector: 'jhi-technical-school-delete-dialog',
  templateUrl: './technical-school-delete-dialog.component.html'
})
export class TechnicalSchoolDeleteDialogComponent {
  technicalSchool: ITechnicalSchool;

  constructor(
    protected technicalSchoolService: TechnicalSchoolService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.technicalSchoolService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'technicalSchoolListModification',
        content: 'Deleted an technicalSchool'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-technical-school-delete-popup',
  template: ''
})
export class TechnicalSchoolDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ technicalSchool }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TechnicalSchoolDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.technicalSchool = technicalSchool;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/technical-school', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/technical-school', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
