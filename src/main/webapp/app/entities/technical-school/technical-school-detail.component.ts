import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITechnicalSchool } from 'app/shared/model/technical-school.model';

@Component({
  selector: 'jhi-technical-school-detail',
  templateUrl: './technical-school-detail.component.html'
})
export class TechnicalSchoolDetailComponent implements OnInit {
  technicalSchool: ITechnicalSchool;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ technicalSchool }) => {
      this.technicalSchool = technicalSchool;
    });
  }

  previousState() {
    window.history.back();
  }
}
