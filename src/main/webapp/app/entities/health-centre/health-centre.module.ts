import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  HealthCentreComponent,
  HealthCentreDetailComponent,
  HealthCentreUpdateComponent,
  HealthCentreDeletePopupComponent,
  HealthCentreDeleteDialogComponent,
  healthCentreRoute,
  healthCentrePopupRoute
} from './';

const ENTITY_STATES = [...healthCentreRoute, ...healthCentrePopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    HealthCentreComponent,
    HealthCentreDetailComponent,
    HealthCentreUpdateComponent,
    HealthCentreDeleteDialogComponent,
    HealthCentreDeletePopupComponent
  ],
  entryComponents: [
    HealthCentreComponent,
    HealthCentreUpdateComponent,
    HealthCentreDeleteDialogComponent,
    HealthCentreDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionHealthCentreModule {}
