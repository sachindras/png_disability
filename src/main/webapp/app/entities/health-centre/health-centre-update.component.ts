import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IHealthCentre, HealthCentre } from 'app/shared/model/health-centre.model';
import { HealthCentreService } from './health-centre.service';

@Component({
  selector: 'jhi-health-centre-update',
  templateUrl: './health-centre-update.component.html'
})
export class HealthCentreUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    centreName: [],
    province: [],
    district: [],
    ward: [],
    active: []
  });

  constructor(protected healthCentreService: HealthCentreService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ healthCentre }) => {
      this.updateForm(healthCentre);
    });
  }

  updateForm(healthCentre: IHealthCentre) {
    this.editForm.patchValue({
      id: healthCentre.id,
      centreName: healthCentre.centreName,
      province: healthCentre.province,
      district: healthCentre.district,
      ward: healthCentre.ward,
      active: healthCentre.active
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const healthCentre = this.createFromForm();
    if (healthCentre.id !== undefined) {
      this.subscribeToSaveResponse(this.healthCentreService.update(healthCentre));
    } else {
      this.subscribeToSaveResponse(this.healthCentreService.create(healthCentre));
    }
  }

  private createFromForm(): IHealthCentre {
    return {
      ...new HealthCentre(),
      id: this.editForm.get(['id']).value,
      centreName: this.editForm.get(['centreName']).value,
      province: this.editForm.get(['province']).value,
      district: this.editForm.get(['district']).value,
      ward: this.editForm.get(['ward']).value,
      active: this.editForm.get(['active']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHealthCentre>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
