import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHealthCentre } from 'app/shared/model/health-centre.model';
import { HealthCentreService } from './health-centre.service';

@Component({
  selector: 'jhi-health-centre-delete-dialog',
  templateUrl: './health-centre-delete-dialog.component.html'
})
export class HealthCentreDeleteDialogComponent {
  healthCentre: IHealthCentre;

  constructor(
    protected healthCentreService: HealthCentreService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.healthCentreService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'healthCentreListModification',
        content: 'Deleted an healthCentre'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-health-centre-delete-popup',
  template: ''
})
export class HealthCentreDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ healthCentre }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(HealthCentreDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.healthCentre = healthCentre;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/health-centre', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/health-centre', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
