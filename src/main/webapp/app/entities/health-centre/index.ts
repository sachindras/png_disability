export * from './health-centre.service';
export * from './health-centre-update.component';
export * from './health-centre-delete-dialog.component';
export * from './health-centre-detail.component';
export * from './health-centre.component';
export * from './health-centre.route';
