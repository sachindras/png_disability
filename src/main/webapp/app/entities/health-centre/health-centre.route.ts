import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HealthCentre } from 'app/shared/model/health-centre.model';
import { HealthCentreService } from './health-centre.service';
import { HealthCentreComponent } from './health-centre.component';
import { HealthCentreDetailComponent } from './health-centre-detail.component';
import { HealthCentreUpdateComponent } from './health-centre-update.component';
import { HealthCentreDeletePopupComponent } from './health-centre-delete-dialog.component';
import { IHealthCentre } from 'app/shared/model/health-centre.model';

@Injectable({ providedIn: 'root' })
export class HealthCentreResolve implements Resolve<IHealthCentre> {
  constructor(private service: HealthCentreService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IHealthCentre> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<HealthCentre>) => response.ok),
        map((healthCentre: HttpResponse<HealthCentre>) => healthCentre.body)
      );
    }
    return of(new HealthCentre());
  }
}

export const healthCentreRoute: Routes = [
  {
    path: '',
    component: HealthCentreComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HealthCentres'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: HealthCentreDetailComponent,
    resolve: {
      healthCentre: HealthCentreResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HealthCentres'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HealthCentreUpdateComponent,
    resolve: {
      healthCentre: HealthCentreResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HealthCentres'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HealthCentreUpdateComponent,
    resolve: {
      healthCentre: HealthCentreResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HealthCentres'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const healthCentrePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: HealthCentreDeletePopupComponent,
    resolve: {
      healthCentre: HealthCentreResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HealthCentres'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
