import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IChurchLeader, ChurchLeader } from 'app/shared/model/church-leader.model';
import { ChurchLeaderService } from './church-leader.service';
import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';
import { ReligiousInstituteService } from 'app/entities/religious-institute';

@Component({
  selector: 'jhi-church-leader-update',
  templateUrl: './church-leader-update.component.html'
})
export class ChurchLeaderUpdateComponent implements OnInit {
  isSaving: boolean;

  religiousinstitutes: IReligiousInstitute[];

  editForm = this.fb.group({
    id: [],
    name: [],
    designation: [],
    phoneNumber: [],
    email: [],
    religiousInstitute: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected churchLeaderService: ChurchLeaderService,
    protected religiousInstituteService: ReligiousInstituteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ churchLeader }) => {
      this.updateForm(churchLeader);
    });
    this.religiousInstituteService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IReligiousInstitute[]>) => mayBeOk.ok),
        map((response: HttpResponse<IReligiousInstitute[]>) => response.body)
      )
      .subscribe((res: IReligiousInstitute[]) => (this.religiousinstitutes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(churchLeader: IChurchLeader) {
    this.editForm.patchValue({
      id: churchLeader.id,
      name: churchLeader.name,
      designation: churchLeader.designation,
      phoneNumber: churchLeader.phoneNumber,
      email: churchLeader.email,
      religiousInstitute: churchLeader.religiousInstitute
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const churchLeader = this.createFromForm();
    if (churchLeader.id !== undefined) {
      this.subscribeToSaveResponse(this.churchLeaderService.update(churchLeader));
    } else {
      this.subscribeToSaveResponse(this.churchLeaderService.create(churchLeader));
    }
  }

  private createFromForm(): IChurchLeader {
    return {
      ...new ChurchLeader(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      designation: this.editForm.get(['designation']).value,
      phoneNumber: this.editForm.get(['phoneNumber']).value,
      email: this.editForm.get(['email']).value,
      religiousInstitute: this.editForm.get(['religiousInstitute']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IChurchLeader>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackReligiousInstituteById(index: number, item: IReligiousInstitute) {
    return item.id;
  }
}
