import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  ChurchLeaderComponent,
  ChurchLeaderDetailComponent,
  ChurchLeaderUpdateComponent,
  ChurchLeaderDeletePopupComponent,
  ChurchLeaderDeleteDialogComponent,
  churchLeaderRoute,
  churchLeaderPopupRoute
} from './';

const ENTITY_STATES = [...churchLeaderRoute, ...churchLeaderPopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ChurchLeaderComponent,
    ChurchLeaderDetailComponent,
    ChurchLeaderUpdateComponent,
    ChurchLeaderDeleteDialogComponent,
    ChurchLeaderDeletePopupComponent
  ],
  entryComponents: [
    ChurchLeaderComponent,
    ChurchLeaderUpdateComponent,
    ChurchLeaderDeleteDialogComponent,
    ChurchLeaderDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionChurchLeaderModule {}
