import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IChurchLeader } from 'app/shared/model/church-leader.model';

@Component({
  selector: 'jhi-church-leader-detail',
  templateUrl: './church-leader-detail.component.html'
})
export class ChurchLeaderDetailComponent implements OnInit {
  churchLeader: IChurchLeader;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ churchLeader }) => {
      this.churchLeader = churchLeader;
    });
  }

  previousState() {
    window.history.back();
  }
}
