import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IChurchLeader } from 'app/shared/model/church-leader.model';
import { ChurchLeaderService } from './church-leader.service';

@Component({
  selector: 'jhi-church-leader-delete-dialog',
  templateUrl: './church-leader-delete-dialog.component.html'
})
export class ChurchLeaderDeleteDialogComponent {
  churchLeader: IChurchLeader;

  constructor(
    protected churchLeaderService: ChurchLeaderService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.churchLeaderService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'churchLeaderListModification',
        content: 'Deleted an churchLeader'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-church-leader-delete-popup',
  template: ''
})
export class ChurchLeaderDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ churchLeader }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ChurchLeaderDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.churchLeader = churchLeader;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/church-leader', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/church-leader', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
