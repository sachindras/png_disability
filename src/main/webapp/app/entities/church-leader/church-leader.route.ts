import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ChurchLeader } from 'app/shared/model/church-leader.model';
import { ChurchLeaderService } from './church-leader.service';
import { ChurchLeaderComponent } from './church-leader.component';
import { ChurchLeaderDetailComponent } from './church-leader-detail.component';
import { ChurchLeaderUpdateComponent } from './church-leader-update.component';
import { ChurchLeaderDeletePopupComponent } from './church-leader-delete-dialog.component';
import { IChurchLeader } from 'app/shared/model/church-leader.model';

@Injectable({ providedIn: 'root' })
export class ChurchLeaderResolve implements Resolve<IChurchLeader> {
  constructor(private service: ChurchLeaderService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IChurchLeader> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ChurchLeader>) => response.ok),
        map((churchLeader: HttpResponse<ChurchLeader>) => churchLeader.body)
      );
    }
    return of(new ChurchLeader());
  }
}

export const churchLeaderRoute: Routes = [
  {
    path: '',
    component: ChurchLeaderComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ChurchLeaders'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ChurchLeaderDetailComponent,
    resolve: {
      churchLeader: ChurchLeaderResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ChurchLeaders'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ChurchLeaderUpdateComponent,
    resolve: {
      churchLeader: ChurchLeaderResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ChurchLeaders'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ChurchLeaderUpdateComponent,
    resolve: {
      churchLeader: ChurchLeaderResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ChurchLeaders'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const churchLeaderPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ChurchLeaderDeletePopupComponent,
    resolve: {
      churchLeader: ChurchLeaderResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ChurchLeaders'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
