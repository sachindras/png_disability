import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IChurchLeader } from 'app/shared/model/church-leader.model';

type EntityResponseType = HttpResponse<IChurchLeader>;
type EntityArrayResponseType = HttpResponse<IChurchLeader[]>;

@Injectable({ providedIn: 'root' })
export class ChurchLeaderService {
  public resourceUrl = SERVER_API_URL + 'api/church-leaders';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/church-leaders';

  constructor(protected http: HttpClient) {}

  create(churchLeader: IChurchLeader): Observable<EntityResponseType> {
    return this.http.post<IChurchLeader>(this.resourceUrl, churchLeader, { observe: 'response' });
  }

  update(churchLeader: IChurchLeader): Observable<EntityResponseType> {
    return this.http.put<IChurchLeader>(this.resourceUrl, churchLeader, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IChurchLeader>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IChurchLeader[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IChurchLeader[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
