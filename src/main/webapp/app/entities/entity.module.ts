import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'religious-institute',
        loadChildren: './religious-institute/religious-institute.module#PngDisabilityReligionReligiousInstituteModule'
      },
      {
        path: 'church-leader',
        loadChildren: './church-leader/church-leader.module#PngDisabilityReligionChurchLeaderModule'
      },
      {
        path: 'hospital',
        loadChildren: './hospital/hospital.module#PngDisabilityReligionHospitalModule'
      },
      {
        path: 'health-centre',
        loadChildren: './health-centre/health-centre.module#PngDisabilityReligionHealthCentreModule'
      },
      {
        path: 'clinic',
        loadChildren: './clinic/clinic.module#PngDisabilityReligionClinicModule'
      },
      {
        path: 'other-health-service',
        loadChildren: './other-health-service/other-health-service.module#PngDisabilityReligionOtherHealthServiceModule'
      },
      {
        path: 'university-college',
        loadChildren: './university-college/university-college.module#PngDisabilityReligionUniversityCollegeModule'
      },
      {
        path: 'technical-school',
        loadChildren: './technical-school/technical-school.module#PngDisabilityReligionTechnicalSchoolModule'
      },
      {
        path: 'secondary-school',
        loadChildren: './secondary-school/secondary-school.module#PngDisabilityReligionSecondarySchoolModule'
      },
      {
        path: 'primary-school',
        loadChildren: './primary-school/primary-school.module#PngDisabilityReligionPrimarySchoolModule'
      },
      {
        path: 'other-education-service',
        loadChildren: './other-education-service/other-education-service.module#PngDisabilityReligionOtherEducationServiceModule'
      },
      {
        path: 'disabled-individual',
        loadChildren: './disabled-individual/disabled-individual.module#PngDisabilityReligionDisabledIndividualModule'
      },
      {
        path: 'contact-details',
        loadChildren: './contact-details/contact-details.module#PngDisabilityReligionContactDetailsModule'
      },
      {
        path: 'location',
        loadChildren: './location/location.module#PngDisabilityReligionLocationModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionEntityModule {}
