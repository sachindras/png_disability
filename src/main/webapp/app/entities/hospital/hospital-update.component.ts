import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IHospital, Hospital } from 'app/shared/model/hospital.model';
import { HospitalService } from './hospital.service';
import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';
import { ReligiousInstituteService } from 'app/entities/religious-institute';

@Component({
  selector: 'jhi-hospital-update',
  templateUrl: './hospital-update.component.html'
})
export class HospitalUpdateComponent implements OnInit {
  isSaving: boolean;

  religiousinstitutes: IReligiousInstitute[];

  editForm = this.fb.group({
    id: [],
    hospitalName: [],
    province: [],
    district: [],
    ward: [],
    active: [],
    religiousInstitute: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected hospitalService: HospitalService,
    protected religiousInstituteService: ReligiousInstituteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ hospital }) => {
      this.updateForm(hospital);
    });
    this.religiousInstituteService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IReligiousInstitute[]>) => mayBeOk.ok),
        map((response: HttpResponse<IReligiousInstitute[]>) => response.body)
      )
      .subscribe((res: IReligiousInstitute[]) => (this.religiousinstitutes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(hospital: IHospital) {
    this.editForm.patchValue({
      id: hospital.id,
      hospitalName: hospital.hospitalName,
      province: hospital.province,
      district: hospital.district,
      ward: hospital.ward,
      active: hospital.active,
      religiousInstitute: hospital.religiousInstitute
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const hospital = this.createFromForm();
    if (hospital.id !== undefined) {
      this.subscribeToSaveResponse(this.hospitalService.update(hospital));
    } else {
      this.subscribeToSaveResponse(this.hospitalService.create(hospital));
    }
  }

  private createFromForm(): IHospital {
    return {
      ...new Hospital(),
      id: this.editForm.get(['id']).value,
      hospitalName: this.editForm.get(['hospitalName']).value,
      province: this.editForm.get(['province']).value,
      district: this.editForm.get(['district']).value,
      ward: this.editForm.get(['ward']).value,
      active: this.editForm.get(['active']).value,
      religiousInstitute: this.editForm.get(['religiousInstitute']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHospital>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackReligiousInstituteById(index: number, item: IReligiousInstitute) {
    return item.id;
  }
}
