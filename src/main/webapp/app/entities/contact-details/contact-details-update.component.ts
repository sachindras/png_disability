import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IContactDetails, ContactDetails } from 'app/shared/model/contact-details.model';
import { ContactDetailsService } from './contact-details.service';

@Component({
  selector: 'jhi-contact-details-update',
  templateUrl: './contact-details-update.component.html'
})
export class ContactDetailsUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    postalAddress: [],
    phoneNumber: [],
    email: []
  });

  constructor(protected contactDetailsService: ContactDetailsService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ contactDetails }) => {
      this.updateForm(contactDetails);
    });
  }

  updateForm(contactDetails: IContactDetails) {
    this.editForm.patchValue({
      id: contactDetails.id,
      postalAddress: contactDetails.postalAddress,
      phoneNumber: contactDetails.phoneNumber,
      email: contactDetails.email
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const contactDetails = this.createFromForm();
    if (contactDetails.id !== undefined) {
      this.subscribeToSaveResponse(this.contactDetailsService.update(contactDetails));
    } else {
      this.subscribeToSaveResponse(this.contactDetailsService.create(contactDetails));
    }
  }

  private createFromForm(): IContactDetails {
    return {
      ...new ContactDetails(),
      id: this.editForm.get(['id']).value,
      postalAddress: this.editForm.get(['postalAddress']).value,
      phoneNumber: this.editForm.get(['phoneNumber']).value,
      email: this.editForm.get(['email']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContactDetails>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
