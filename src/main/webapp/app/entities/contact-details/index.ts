export * from './contact-details.service';
export * from './contact-details-update.component';
export * from './contact-details-delete-dialog.component';
export * from './contact-details-detail.component';
export * from './contact-details.component';
export * from './contact-details.route';
