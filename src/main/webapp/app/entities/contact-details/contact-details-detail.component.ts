import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IContactDetails } from 'app/shared/model/contact-details.model';

@Component({
  selector: 'jhi-contact-details-detail',
  templateUrl: './contact-details-detail.component.html'
})
export class ContactDetailsDetailComponent implements OnInit {
  contactDetails: IContactDetails;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ contactDetails }) => {
      this.contactDetails = contactDetails;
    });
  }

  previousState() {
    window.history.back();
  }
}
