import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IContactDetails } from 'app/shared/model/contact-details.model';
import { ContactDetailsService } from './contact-details.service';

@Component({
  selector: 'jhi-contact-details-delete-dialog',
  templateUrl: './contact-details-delete-dialog.component.html'
})
export class ContactDetailsDeleteDialogComponent {
  contactDetails: IContactDetails;

  constructor(
    protected contactDetailsService: ContactDetailsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.contactDetailsService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'contactDetailsListModification',
        content: 'Deleted an contactDetails'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-contact-details-delete-popup',
  template: ''
})
export class ContactDetailsDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ contactDetails }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ContactDetailsDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.contactDetails = contactDetails;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/contact-details', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/contact-details', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
