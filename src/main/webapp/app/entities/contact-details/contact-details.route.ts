import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ContactDetails } from 'app/shared/model/contact-details.model';
import { ContactDetailsService } from './contact-details.service';
import { ContactDetailsComponent } from './contact-details.component';
import { ContactDetailsDetailComponent } from './contact-details-detail.component';
import { ContactDetailsUpdateComponent } from './contact-details-update.component';
import { ContactDetailsDeletePopupComponent } from './contact-details-delete-dialog.component';
import { IContactDetails } from 'app/shared/model/contact-details.model';

@Injectable({ providedIn: 'root' })
export class ContactDetailsResolve implements Resolve<IContactDetails> {
  constructor(private service: ContactDetailsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IContactDetails> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ContactDetails>) => response.ok),
        map((contactDetails: HttpResponse<ContactDetails>) => contactDetails.body)
      );
    }
    return of(new ContactDetails());
  }
}

export const contactDetailsRoute: Routes = [
  {
    path: '',
    component: ContactDetailsComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ContactDetails'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ContactDetailsDetailComponent,
    resolve: {
      contactDetails: ContactDetailsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ContactDetails'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ContactDetailsUpdateComponent,
    resolve: {
      contactDetails: ContactDetailsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ContactDetails'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ContactDetailsUpdateComponent,
    resolve: {
      contactDetails: ContactDetailsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ContactDetails'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const contactDetailsPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ContactDetailsDeletePopupComponent,
    resolve: {
      contactDetails: ContactDetailsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ContactDetails'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
