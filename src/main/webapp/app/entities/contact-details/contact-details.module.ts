import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  ContactDetailsComponent,
  ContactDetailsDetailComponent,
  ContactDetailsUpdateComponent,
  ContactDetailsDeletePopupComponent,
  ContactDetailsDeleteDialogComponent,
  contactDetailsRoute,
  contactDetailsPopupRoute
} from './';

const ENTITY_STATES = [...contactDetailsRoute, ...contactDetailsPopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ContactDetailsComponent,
    ContactDetailsDetailComponent,
    ContactDetailsUpdateComponent,
    ContactDetailsDeleteDialogComponent,
    ContactDetailsDeletePopupComponent
  ],
  entryComponents: [
    ContactDetailsComponent,
    ContactDetailsUpdateComponent,
    ContactDetailsDeleteDialogComponent,
    ContactDetailsDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionContactDetailsModule {}
