import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IUniversityCollege, UniversityCollege } from 'app/shared/model/university-college.model';
import { UniversityCollegeService } from './university-college.service';
import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';
import { ReligiousInstituteService } from 'app/entities/religious-institute';

@Component({
  selector: 'jhi-university-college-update',
  templateUrl: './university-college-update.component.html'
})
export class UniversityCollegeUpdateComponent implements OnInit {
  isSaving: boolean;

  religiousinstitutes: IReligiousInstitute[];

  editForm = this.fb.group({
    id: [],
    name: [],
    province: [],
    district: [],
    ward: [],
    active: [],
    religiousInstitute: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected universityCollegeService: UniversityCollegeService,
    protected religiousInstituteService: ReligiousInstituteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ universityCollege }) => {
      this.updateForm(universityCollege);
    });
    this.religiousInstituteService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IReligiousInstitute[]>) => mayBeOk.ok),
        map((response: HttpResponse<IReligiousInstitute[]>) => response.body)
      )
      .subscribe((res: IReligiousInstitute[]) => (this.religiousinstitutes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(universityCollege: IUniversityCollege) {
    this.editForm.patchValue({
      id: universityCollege.id,
      name: universityCollege.name,
      province: universityCollege.province,
      district: universityCollege.district,
      ward: universityCollege.ward,
      active: universityCollege.active,
      religiousInstitute: universityCollege.religiousInstitute
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const universityCollege = this.createFromForm();
    if (universityCollege.id !== undefined) {
      this.subscribeToSaveResponse(this.universityCollegeService.update(universityCollege));
    } else {
      this.subscribeToSaveResponse(this.universityCollegeService.create(universityCollege));
    }
  }

  private createFromForm(): IUniversityCollege {
    return {
      ...new UniversityCollege(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      province: this.editForm.get(['province']).value,
      district: this.editForm.get(['district']).value,
      ward: this.editForm.get(['ward']).value,
      active: this.editForm.get(['active']).value,
      religiousInstitute: this.editForm.get(['religiousInstitute']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUniversityCollege>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackReligiousInstituteById(index: number, item: IReligiousInstitute) {
    return item.id;
  }
}
