export * from './university-college.service';
export * from './university-college-update.component';
export * from './university-college-delete-dialog.component';
export * from './university-college-detail.component';
export * from './university-college.component';
export * from './university-college.route';
