import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IUniversityCollege } from 'app/shared/model/university-college.model';

type EntityResponseType = HttpResponse<IUniversityCollege>;
type EntityArrayResponseType = HttpResponse<IUniversityCollege[]>;

@Injectable({ providedIn: 'root' })
export class UniversityCollegeService {
  public resourceUrl = SERVER_API_URL + 'api/university-colleges';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/university-colleges';

  constructor(protected http: HttpClient) {}

  create(universityCollege: IUniversityCollege): Observable<EntityResponseType> {
    return this.http.post<IUniversityCollege>(this.resourceUrl, universityCollege, { observe: 'response' });
  }

  update(universityCollege: IUniversityCollege): Observable<EntityResponseType> {
    return this.http.put<IUniversityCollege>(this.resourceUrl, universityCollege, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUniversityCollege>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUniversityCollege[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUniversityCollege[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
