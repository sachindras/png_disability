import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UniversityCollege } from 'app/shared/model/university-college.model';
import { UniversityCollegeService } from './university-college.service';
import { UniversityCollegeComponent } from './university-college.component';
import { UniversityCollegeDetailComponent } from './university-college-detail.component';
import { UniversityCollegeUpdateComponent } from './university-college-update.component';
import { UniversityCollegeDeletePopupComponent } from './university-college-delete-dialog.component';
import { IUniversityCollege } from 'app/shared/model/university-college.model';

@Injectable({ providedIn: 'root' })
export class UniversityCollegeResolve implements Resolve<IUniversityCollege> {
  constructor(private service: UniversityCollegeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUniversityCollege> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<UniversityCollege>) => response.ok),
        map((universityCollege: HttpResponse<UniversityCollege>) => universityCollege.body)
      );
    }
    return of(new UniversityCollege());
  }
}

export const universityCollegeRoute: Routes = [
  {
    path: '',
    component: UniversityCollegeComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UniversityColleges'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UniversityCollegeDetailComponent,
    resolve: {
      universityCollege: UniversityCollegeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UniversityColleges'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UniversityCollegeUpdateComponent,
    resolve: {
      universityCollege: UniversityCollegeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UniversityColleges'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UniversityCollegeUpdateComponent,
    resolve: {
      universityCollege: UniversityCollegeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UniversityColleges'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const universityCollegePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: UniversityCollegeDeletePopupComponent,
    resolve: {
      universityCollege: UniversityCollegeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UniversityColleges'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
