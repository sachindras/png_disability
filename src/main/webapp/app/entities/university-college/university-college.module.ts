import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  UniversityCollegeComponent,
  UniversityCollegeDetailComponent,
  UniversityCollegeUpdateComponent,
  UniversityCollegeDeletePopupComponent,
  UniversityCollegeDeleteDialogComponent,
  universityCollegeRoute,
  universityCollegePopupRoute
} from './';

const ENTITY_STATES = [...universityCollegeRoute, ...universityCollegePopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    UniversityCollegeComponent,
    UniversityCollegeDetailComponent,
    UniversityCollegeUpdateComponent,
    UniversityCollegeDeleteDialogComponent,
    UniversityCollegeDeletePopupComponent
  ],
  entryComponents: [
    UniversityCollegeComponent,
    UniversityCollegeUpdateComponent,
    UniversityCollegeDeleteDialogComponent,
    UniversityCollegeDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionUniversityCollegeModule {}
