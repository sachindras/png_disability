import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUniversityCollege } from 'app/shared/model/university-college.model';
import { UniversityCollegeService } from './university-college.service';

@Component({
  selector: 'jhi-university-college-delete-dialog',
  templateUrl: './university-college-delete-dialog.component.html'
})
export class UniversityCollegeDeleteDialogComponent {
  universityCollege: IUniversityCollege;

  constructor(
    protected universityCollegeService: UniversityCollegeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.universityCollegeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'universityCollegeListModification',
        content: 'Deleted an universityCollege'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-university-college-delete-popup',
  template: ''
})
export class UniversityCollegeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ universityCollege }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(UniversityCollegeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.universityCollege = universityCollege;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/university-college', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/university-college', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
