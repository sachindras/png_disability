import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUniversityCollege } from 'app/shared/model/university-college.model';

@Component({
  selector: 'jhi-university-college-detail',
  templateUrl: './university-college-detail.component.html'
})
export class UniversityCollegeDetailComponent implements OnInit {
  universityCollege: IUniversityCollege;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ universityCollege }) => {
      this.universityCollege = universityCollege;
    });
  }

  previousState() {
    window.history.back();
  }
}
