import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  PrimarySchoolComponent,
  PrimarySchoolDetailComponent,
  PrimarySchoolUpdateComponent,
  PrimarySchoolDeletePopupComponent,
  PrimarySchoolDeleteDialogComponent,
  primarySchoolRoute,
  primarySchoolPopupRoute
} from './';

const ENTITY_STATES = [...primarySchoolRoute, ...primarySchoolPopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PrimarySchoolComponent,
    PrimarySchoolDetailComponent,
    PrimarySchoolUpdateComponent,
    PrimarySchoolDeleteDialogComponent,
    PrimarySchoolDeletePopupComponent
  ],
  entryComponents: [
    PrimarySchoolComponent,
    PrimarySchoolUpdateComponent,
    PrimarySchoolDeleteDialogComponent,
    PrimarySchoolDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionPrimarySchoolModule {}
