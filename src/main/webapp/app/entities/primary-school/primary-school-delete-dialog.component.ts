import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPrimarySchool } from 'app/shared/model/primary-school.model';
import { PrimarySchoolService } from './primary-school.service';

@Component({
  selector: 'jhi-primary-school-delete-dialog',
  templateUrl: './primary-school-delete-dialog.component.html'
})
export class PrimarySchoolDeleteDialogComponent {
  primarySchool: IPrimarySchool;

  constructor(
    protected primarySchoolService: PrimarySchoolService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.primarySchoolService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'primarySchoolListModification',
        content: 'Deleted an primarySchool'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-primary-school-delete-popup',
  template: ''
})
export class PrimarySchoolDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ primarySchool }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PrimarySchoolDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.primarySchool = primarySchool;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/primary-school', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/primary-school', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
