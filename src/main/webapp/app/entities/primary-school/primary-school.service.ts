import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPrimarySchool } from 'app/shared/model/primary-school.model';

type EntityResponseType = HttpResponse<IPrimarySchool>;
type EntityArrayResponseType = HttpResponse<IPrimarySchool[]>;

@Injectable({ providedIn: 'root' })
export class PrimarySchoolService {
  public resourceUrl = SERVER_API_URL + 'api/primary-schools';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/primary-schools';

  constructor(protected http: HttpClient) {}

  create(primarySchool: IPrimarySchool): Observable<EntityResponseType> {
    return this.http.post<IPrimarySchool>(this.resourceUrl, primarySchool, { observe: 'response' });
  }

  update(primarySchool: IPrimarySchool): Observable<EntityResponseType> {
    return this.http.put<IPrimarySchool>(this.resourceUrl, primarySchool, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPrimarySchool>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPrimarySchool[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPrimarySchool[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
