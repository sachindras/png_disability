import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IPrimarySchool, PrimarySchool } from 'app/shared/model/primary-school.model';
import { PrimarySchoolService } from './primary-school.service';
import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';
import { ReligiousInstituteService } from 'app/entities/religious-institute';

@Component({
  selector: 'jhi-primary-school-update',
  templateUrl: './primary-school-update.component.html'
})
export class PrimarySchoolUpdateComponent implements OnInit {
  isSaving: boolean;

  religiousinstitutes: IReligiousInstitute[];

  editForm = this.fb.group({
    id: [],
    name: [],
    province: [],
    district: [],
    ward: [],
    active: [],
    religiousInstitute: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected primarySchoolService: PrimarySchoolService,
    protected religiousInstituteService: ReligiousInstituteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ primarySchool }) => {
      this.updateForm(primarySchool);
    });
    this.religiousInstituteService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IReligiousInstitute[]>) => mayBeOk.ok),
        map((response: HttpResponse<IReligiousInstitute[]>) => response.body)
      )
      .subscribe((res: IReligiousInstitute[]) => (this.religiousinstitutes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(primarySchool: IPrimarySchool) {
    this.editForm.patchValue({
      id: primarySchool.id,
      name: primarySchool.name,
      province: primarySchool.province,
      district: primarySchool.district,
      ward: primarySchool.ward,
      active: primarySchool.active,
      religiousInstitute: primarySchool.religiousInstitute
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const primarySchool = this.createFromForm();
    if (primarySchool.id !== undefined) {
      this.subscribeToSaveResponse(this.primarySchoolService.update(primarySchool));
    } else {
      this.subscribeToSaveResponse(this.primarySchoolService.create(primarySchool));
    }
  }

  private createFromForm(): IPrimarySchool {
    return {
      ...new PrimarySchool(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      province: this.editForm.get(['province']).value,
      district: this.editForm.get(['district']).value,
      ward: this.editForm.get(['ward']).value,
      active: this.editForm.get(['active']).value,
      religiousInstitute: this.editForm.get(['religiousInstitute']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPrimarySchool>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackReligiousInstituteById(index: number, item: IReligiousInstitute) {
    return item.id;
  }
}
