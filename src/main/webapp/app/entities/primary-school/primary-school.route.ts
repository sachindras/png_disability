import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { PrimarySchool } from 'app/shared/model/primary-school.model';
import { PrimarySchoolService } from './primary-school.service';
import { PrimarySchoolComponent } from './primary-school.component';
import { PrimarySchoolDetailComponent } from './primary-school-detail.component';
import { PrimarySchoolUpdateComponent } from './primary-school-update.component';
import { PrimarySchoolDeletePopupComponent } from './primary-school-delete-dialog.component';
import { IPrimarySchool } from 'app/shared/model/primary-school.model';

@Injectable({ providedIn: 'root' })
export class PrimarySchoolResolve implements Resolve<IPrimarySchool> {
  constructor(private service: PrimarySchoolService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPrimarySchool> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<PrimarySchool>) => response.ok),
        map((primarySchool: HttpResponse<PrimarySchool>) => primarySchool.body)
      );
    }
    return of(new PrimarySchool());
  }
}

export const primarySchoolRoute: Routes = [
  {
    path: '',
    component: PrimarySchoolComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PrimarySchools'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PrimarySchoolDetailComponent,
    resolve: {
      primarySchool: PrimarySchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PrimarySchools'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PrimarySchoolUpdateComponent,
    resolve: {
      primarySchool: PrimarySchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PrimarySchools'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PrimarySchoolUpdateComponent,
    resolve: {
      primarySchool: PrimarySchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PrimarySchools'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const primarySchoolPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PrimarySchoolDeletePopupComponent,
    resolve: {
      primarySchool: PrimarySchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PrimarySchools'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
