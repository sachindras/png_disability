export * from './primary-school.service';
export * from './primary-school-update.component';
export * from './primary-school-delete-dialog.component';
export * from './primary-school-detail.component';
export * from './primary-school.component';
export * from './primary-school.route';
