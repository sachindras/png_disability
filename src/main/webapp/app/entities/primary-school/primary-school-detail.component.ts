import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPrimarySchool } from 'app/shared/model/primary-school.model';

@Component({
  selector: 'jhi-primary-school-detail',
  templateUrl: './primary-school-detail.component.html'
})
export class PrimarySchoolDetailComponent implements OnInit {
  primarySchool: IPrimarySchool;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ primarySchool }) => {
      this.primarySchool = primarySchool;
    });
  }

  previousState() {
    window.history.back();
  }
}
