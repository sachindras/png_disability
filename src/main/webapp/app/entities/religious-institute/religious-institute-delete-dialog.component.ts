import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';
import { ReligiousInstituteService } from './religious-institute.service';

@Component({
  selector: 'jhi-religious-institute-delete-dialog',
  templateUrl: './religious-institute-delete-dialog.component.html'
})
export class ReligiousInstituteDeleteDialogComponent {
  religiousInstitute: IReligiousInstitute;

  constructor(
    protected religiousInstituteService: ReligiousInstituteService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.religiousInstituteService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'religiousInstituteListModification',
        content: 'Deleted an religiousInstitute'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-religious-institute-delete-popup',
  template: ''
})
export class ReligiousInstituteDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ religiousInstitute }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ReligiousInstituteDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.religiousInstitute = religiousInstitute;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/religious-institute', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/religious-institute', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
