import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';

@Component({
  selector: 'jhi-religious-institute-detail',
  templateUrl: './religious-institute-detail.component.html'
})
export class ReligiousInstituteDetailComponent implements OnInit {
  religiousInstitute: IReligiousInstitute;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ religiousInstitute }) => {
      this.religiousInstitute = religiousInstitute;
    });
  }

  previousState() {
    window.history.back();
  }
}
