export * from './religious-institute.service';
export * from './religious-institute-update.component';
export * from './religious-institute-delete-dialog.component';
export * from './religious-institute-detail.component';
export * from './religious-institute.component';
export * from './religious-institute.route';
