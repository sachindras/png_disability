import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  ReligiousInstituteComponent,
  ReligiousInstituteDetailComponent,
  ReligiousInstituteUpdateComponent,
  ReligiousInstituteDeletePopupComponent,
  ReligiousInstituteDeleteDialogComponent,
  religiousInstituteRoute,
  religiousInstitutePopupRoute
} from './';

const ENTITY_STATES = [...religiousInstituteRoute, ...religiousInstitutePopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ReligiousInstituteComponent,
    ReligiousInstituteDetailComponent,
    ReligiousInstituteUpdateComponent,
    ReligiousInstituteDeleteDialogComponent,
    ReligiousInstituteDeletePopupComponent
  ],
  entryComponents: [
    ReligiousInstituteComponent,
    ReligiousInstituteUpdateComponent,
    ReligiousInstituteDeleteDialogComponent,
    ReligiousInstituteDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionReligiousInstituteModule {}
