import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';

type EntityResponseType = HttpResponse<IReligiousInstitute>;
type EntityArrayResponseType = HttpResponse<IReligiousInstitute[]>;

@Injectable({ providedIn: 'root' })
export class ReligiousInstituteService {
  public resourceUrl = SERVER_API_URL + 'api/religious-institutes';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/religious-institutes';

  constructor(protected http: HttpClient) {}

  create(religiousInstitute: IReligiousInstitute): Observable<EntityResponseType> {
    return this.http.post<IReligiousInstitute>(this.resourceUrl, religiousInstitute, { observe: 'response' });
  }

  update(religiousInstitute: IReligiousInstitute): Observable<EntityResponseType> {
    return this.http.put<IReligiousInstitute>(this.resourceUrl, religiousInstitute, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IReligiousInstitute>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IReligiousInstitute[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IReligiousInstitute[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
