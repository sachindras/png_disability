import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IReligiousInstitute, ReligiousInstitute } from 'app/shared/model/religious-institute.model';
import { ReligiousInstituteService } from './religious-institute.service';

@Component({
  selector: 'jhi-religious-institute-update',
  templateUrl: './religious-institute-update.component.html'
})
export class ReligiousInstituteUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    churchName: [],
    churchAddress: [],
    denomination: [],
    headOfficeLocation: [],
    numberChurchMembership: []
  });

  constructor(
    protected religiousInstituteService: ReligiousInstituteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ religiousInstitute }) => {
      this.updateForm(religiousInstitute);
    });
  }

  updateForm(religiousInstitute: IReligiousInstitute) {
    this.editForm.patchValue({
      id: religiousInstitute.id,
      churchName: religiousInstitute.churchName,
      churchAddress: religiousInstitute.churchAddress,
      denomination: religiousInstitute.denomination,
      headOfficeLocation: religiousInstitute.headOfficeLocation,
      numberChurchMembership: religiousInstitute.numberChurchMembership
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const religiousInstitute = this.createFromForm();
    if (religiousInstitute.id !== undefined) {
      this.subscribeToSaveResponse(this.religiousInstituteService.update(religiousInstitute));
    } else {
      this.subscribeToSaveResponse(this.religiousInstituteService.create(religiousInstitute));
    }
  }

  private createFromForm(): IReligiousInstitute {
    return {
      ...new ReligiousInstitute(),
      id: this.editForm.get(['id']).value,
      churchName: this.editForm.get(['churchName']).value,
      churchAddress: this.editForm.get(['churchAddress']).value,
      denomination: this.editForm.get(['denomination']).value,
      headOfficeLocation: this.editForm.get(['headOfficeLocation']).value,
      numberChurchMembership: this.editForm.get(['numberChurchMembership']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReligiousInstitute>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
