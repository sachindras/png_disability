import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ReligiousInstitute } from 'app/shared/model/religious-institute.model';
import { ReligiousInstituteService } from './religious-institute.service';
import { ReligiousInstituteComponent } from './religious-institute.component';
import { ReligiousInstituteDetailComponent } from './religious-institute-detail.component';
import { ReligiousInstituteUpdateComponent } from './religious-institute-update.component';
import { ReligiousInstituteDeletePopupComponent } from './religious-institute-delete-dialog.component';
import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';

@Injectable({ providedIn: 'root' })
export class ReligiousInstituteResolve implements Resolve<IReligiousInstitute> {
  constructor(private service: ReligiousInstituteService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IReligiousInstitute> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ReligiousInstitute>) => response.ok),
        map((religiousInstitute: HttpResponse<ReligiousInstitute>) => religiousInstitute.body)
      );
    }
    return of(new ReligiousInstitute());
  }
}

export const religiousInstituteRoute: Routes = [
  {
    path: '',
    component: ReligiousInstituteComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ReligiousInstitutes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ReligiousInstituteDetailComponent,
    resolve: {
      religiousInstitute: ReligiousInstituteResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ReligiousInstitutes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ReligiousInstituteUpdateComponent,
    resolve: {
      religiousInstitute: ReligiousInstituteResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ReligiousInstitutes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ReligiousInstituteUpdateComponent,
    resolve: {
      religiousInstitute: ReligiousInstituteResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ReligiousInstitutes'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const religiousInstitutePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ReligiousInstituteDeletePopupComponent,
    resolve: {
      religiousInstitute: ReligiousInstituteResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ReligiousInstitutes'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
