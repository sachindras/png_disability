import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOtherEducationService } from 'app/shared/model/other-education-service.model';
import { OtherEducationServiceService } from './other-education-service.service';

@Component({
  selector: 'jhi-other-education-service-delete-dialog',
  templateUrl: './other-education-service-delete-dialog.component.html'
})
export class OtherEducationServiceDeleteDialogComponent {
  otherEducationService: IOtherEducationService;

  constructor(
    protected otherEducationServiceService: OtherEducationServiceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.otherEducationServiceService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'otherEducationServiceListModification',
        content: 'Deleted an otherEducationService'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-other-education-service-delete-popup',
  template: ''
})
export class OtherEducationServiceDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ otherEducationService }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(OtherEducationServiceDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.otherEducationService = otherEducationService;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/other-education-service', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/other-education-service', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
