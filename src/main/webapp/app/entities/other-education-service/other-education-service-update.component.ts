import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IOtherEducationService, OtherEducationService } from 'app/shared/model/other-education-service.model';
import { OtherEducationServiceService } from './other-education-service.service';
import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';
import { ReligiousInstituteService } from 'app/entities/religious-institute';

@Component({
  selector: 'jhi-other-education-service-update',
  templateUrl: './other-education-service-update.component.html'
})
export class OtherEducationServiceUpdateComponent implements OnInit {
  isSaving: boolean;

  religiousinstitutes: IReligiousInstitute[];

  editForm = this.fb.group({
    id: [],
    name: [],
    province: [],
    district: [],
    ward: [],
    active: [],
    religiousInstitute: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected otherEducationServiceService: OtherEducationServiceService,
    protected religiousInstituteService: ReligiousInstituteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ otherEducationService }) => {
      this.updateForm(otherEducationService);
    });
    this.religiousInstituteService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IReligiousInstitute[]>) => mayBeOk.ok),
        map((response: HttpResponse<IReligiousInstitute[]>) => response.body)
      )
      .subscribe((res: IReligiousInstitute[]) => (this.religiousinstitutes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(otherEducationService: IOtherEducationService) {
    this.editForm.patchValue({
      id: otherEducationService.id,
      name: otherEducationService.name,
      province: otherEducationService.province,
      district: otherEducationService.district,
      ward: otherEducationService.ward,
      active: otherEducationService.active,
      religiousInstitute: otherEducationService.religiousInstitute
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const otherEducationService = this.createFromForm();
    if (otherEducationService.id !== undefined) {
      this.subscribeToSaveResponse(this.otherEducationServiceService.update(otherEducationService));
    } else {
      this.subscribeToSaveResponse(this.otherEducationServiceService.create(otherEducationService));
    }
  }

  private createFromForm(): IOtherEducationService {
    return {
      ...new OtherEducationService(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      province: this.editForm.get(['province']).value,
      district: this.editForm.get(['district']).value,
      ward: this.editForm.get(['ward']).value,
      active: this.editForm.get(['active']).value,
      religiousInstitute: this.editForm.get(['religiousInstitute']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOtherEducationService>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackReligiousInstituteById(index: number, item: IReligiousInstitute) {
    return item.id;
  }
}
