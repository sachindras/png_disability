import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOtherEducationService } from 'app/shared/model/other-education-service.model';

@Component({
  selector: 'jhi-other-education-service-detail',
  templateUrl: './other-education-service-detail.component.html'
})
export class OtherEducationServiceDetailComponent implements OnInit {
  otherEducationService: IOtherEducationService;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ otherEducationService }) => {
      this.otherEducationService = otherEducationService;
    });
  }

  previousState() {
    window.history.back();
  }
}
