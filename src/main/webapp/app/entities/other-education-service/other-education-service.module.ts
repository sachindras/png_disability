import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  OtherEducationServiceComponent,
  OtherEducationServiceDetailComponent,
  OtherEducationServiceUpdateComponent,
  OtherEducationServiceDeletePopupComponent,
  OtherEducationServiceDeleteDialogComponent,
  otherEducationServiceRoute,
  otherEducationServicePopupRoute
} from './';

const ENTITY_STATES = [...otherEducationServiceRoute, ...otherEducationServicePopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OtherEducationServiceComponent,
    OtherEducationServiceDetailComponent,
    OtherEducationServiceUpdateComponent,
    OtherEducationServiceDeleteDialogComponent,
    OtherEducationServiceDeletePopupComponent
  ],
  entryComponents: [
    OtherEducationServiceComponent,
    OtherEducationServiceUpdateComponent,
    OtherEducationServiceDeleteDialogComponent,
    OtherEducationServiceDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionOtherEducationServiceModule {}
