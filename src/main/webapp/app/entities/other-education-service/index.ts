export * from './other-education-service.service';
export * from './other-education-service-update.component';
export * from './other-education-service-delete-dialog.component';
export * from './other-education-service-detail.component';
export * from './other-education-service.component';
export * from './other-education-service.route';
