import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { OtherEducationService } from 'app/shared/model/other-education-service.model';
import { OtherEducationServiceService } from './other-education-service.service';
import { OtherEducationServiceComponent } from './other-education-service.component';
import { OtherEducationServiceDetailComponent } from './other-education-service-detail.component';
import { OtherEducationServiceUpdateComponent } from './other-education-service-update.component';
import { OtherEducationServiceDeletePopupComponent } from './other-education-service-delete-dialog.component';
import { IOtherEducationService } from 'app/shared/model/other-education-service.model';

@Injectable({ providedIn: 'root' })
export class OtherEducationServiceResolve implements Resolve<IOtherEducationService> {
  constructor(private service: OtherEducationServiceService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IOtherEducationService> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<OtherEducationService>) => response.ok),
        map((otherEducationService: HttpResponse<OtherEducationService>) => otherEducationService.body)
      );
    }
    return of(new OtherEducationService());
  }
}

export const otherEducationServiceRoute: Routes = [
  {
    path: '',
    component: OtherEducationServiceComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OtherEducationServices'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OtherEducationServiceDetailComponent,
    resolve: {
      otherEducationService: OtherEducationServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OtherEducationServices'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OtherEducationServiceUpdateComponent,
    resolve: {
      otherEducationService: OtherEducationServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OtherEducationServices'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OtherEducationServiceUpdateComponent,
    resolve: {
      otherEducationService: OtherEducationServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OtherEducationServices'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const otherEducationServicePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: OtherEducationServiceDeletePopupComponent,
    resolve: {
      otherEducationService: OtherEducationServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OtherEducationServices'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
