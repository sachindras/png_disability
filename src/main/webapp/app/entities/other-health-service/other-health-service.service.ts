import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOtherHealthService } from 'app/shared/model/other-health-service.model';

type EntityResponseType = HttpResponse<IOtherHealthService>;
type EntityArrayResponseType = HttpResponse<IOtherHealthService[]>;

@Injectable({ providedIn: 'root' })
export class OtherHealthServiceService {
  public resourceUrl = SERVER_API_URL + 'api/other-health-services';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/other-health-services';

  constructor(protected http: HttpClient) {}

  create(otherHealthService: IOtherHealthService): Observable<EntityResponseType> {
    return this.http.post<IOtherHealthService>(this.resourceUrl, otherHealthService, { observe: 'response' });
  }

  update(otherHealthService: IOtherHealthService): Observable<EntityResponseType> {
    return this.http.put<IOtherHealthService>(this.resourceUrl, otherHealthService, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOtherHealthService>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOtherHealthService[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOtherHealthService[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
