import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOtherHealthService } from 'app/shared/model/other-health-service.model';
import { OtherHealthServiceService } from './other-health-service.service';

@Component({
  selector: 'jhi-other-health-service-delete-dialog',
  templateUrl: './other-health-service-delete-dialog.component.html'
})
export class OtherHealthServiceDeleteDialogComponent {
  otherHealthService: IOtherHealthService;

  constructor(
    protected otherHealthServiceService: OtherHealthServiceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.otherHealthServiceService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'otherHealthServiceListModification',
        content: 'Deleted an otherHealthService'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-other-health-service-delete-popup',
  template: ''
})
export class OtherHealthServiceDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ otherHealthService }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(OtherHealthServiceDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.otherHealthService = otherHealthService;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/other-health-service', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/other-health-service', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
