import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOtherHealthService } from 'app/shared/model/other-health-service.model';

@Component({
  selector: 'jhi-other-health-service-detail',
  templateUrl: './other-health-service-detail.component.html'
})
export class OtherHealthServiceDetailComponent implements OnInit {
  otherHealthService: IOtherHealthService;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ otherHealthService }) => {
      this.otherHealthService = otherHealthService;
    });
  }

  previousState() {
    window.history.back();
  }
}
