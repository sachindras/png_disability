import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IOtherHealthService, OtherHealthService } from 'app/shared/model/other-health-service.model';
import { OtherHealthServiceService } from './other-health-service.service';
import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';
import { ReligiousInstituteService } from 'app/entities/religious-institute';

@Component({
  selector: 'jhi-other-health-service-update',
  templateUrl: './other-health-service-update.component.html'
})
export class OtherHealthServiceUpdateComponent implements OnInit {
  isSaving: boolean;

  religiousinstitutes: IReligiousInstitute[];

  editForm = this.fb.group({
    id: [],
    name: [],
    province: [],
    district: [],
    ward: [],
    active: [],
    religiousInstitute: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected otherHealthServiceService: OtherHealthServiceService,
    protected religiousInstituteService: ReligiousInstituteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ otherHealthService }) => {
      this.updateForm(otherHealthService);
    });
    this.religiousInstituteService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IReligiousInstitute[]>) => mayBeOk.ok),
        map((response: HttpResponse<IReligiousInstitute[]>) => response.body)
      )
      .subscribe((res: IReligiousInstitute[]) => (this.religiousinstitutes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(otherHealthService: IOtherHealthService) {
    this.editForm.patchValue({
      id: otherHealthService.id,
      name: otherHealthService.name,
      province: otherHealthService.province,
      district: otherHealthService.district,
      ward: otherHealthService.ward,
      active: otherHealthService.active,
      religiousInstitute: otherHealthService.religiousInstitute
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const otherHealthService = this.createFromForm();
    if (otherHealthService.id !== undefined) {
      this.subscribeToSaveResponse(this.otherHealthServiceService.update(otherHealthService));
    } else {
      this.subscribeToSaveResponse(this.otherHealthServiceService.create(otherHealthService));
    }
  }

  private createFromForm(): IOtherHealthService {
    return {
      ...new OtherHealthService(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      province: this.editForm.get(['province']).value,
      district: this.editForm.get(['district']).value,
      ward: this.editForm.get(['ward']).value,
      active: this.editForm.get(['active']).value,
      religiousInstitute: this.editForm.get(['religiousInstitute']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOtherHealthService>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackReligiousInstituteById(index: number, item: IReligiousInstitute) {
    return item.id;
  }
}
