export * from './other-health-service.service';
export * from './other-health-service-update.component';
export * from './other-health-service-delete-dialog.component';
export * from './other-health-service-detail.component';
export * from './other-health-service.component';
export * from './other-health-service.route';
