import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  OtherHealthServiceComponent,
  OtherHealthServiceDetailComponent,
  OtherHealthServiceUpdateComponent,
  OtherHealthServiceDeletePopupComponent,
  OtherHealthServiceDeleteDialogComponent,
  otherHealthServiceRoute,
  otherHealthServicePopupRoute
} from './';

const ENTITY_STATES = [...otherHealthServiceRoute, ...otherHealthServicePopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OtherHealthServiceComponent,
    OtherHealthServiceDetailComponent,
    OtherHealthServiceUpdateComponent,
    OtherHealthServiceDeleteDialogComponent,
    OtherHealthServiceDeletePopupComponent
  ],
  entryComponents: [
    OtherHealthServiceComponent,
    OtherHealthServiceUpdateComponent,
    OtherHealthServiceDeleteDialogComponent,
    OtherHealthServiceDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionOtherHealthServiceModule {}
