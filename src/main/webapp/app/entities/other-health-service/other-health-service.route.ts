import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { OtherHealthService } from 'app/shared/model/other-health-service.model';
import { OtherHealthServiceService } from './other-health-service.service';
import { OtherHealthServiceComponent } from './other-health-service.component';
import { OtherHealthServiceDetailComponent } from './other-health-service-detail.component';
import { OtherHealthServiceUpdateComponent } from './other-health-service-update.component';
import { OtherHealthServiceDeletePopupComponent } from './other-health-service-delete-dialog.component';
import { IOtherHealthService } from 'app/shared/model/other-health-service.model';

@Injectable({ providedIn: 'root' })
export class OtherHealthServiceResolve implements Resolve<IOtherHealthService> {
  constructor(private service: OtherHealthServiceService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IOtherHealthService> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<OtherHealthService>) => response.ok),
        map((otherHealthService: HttpResponse<OtherHealthService>) => otherHealthService.body)
      );
    }
    return of(new OtherHealthService());
  }
}

export const otherHealthServiceRoute: Routes = [
  {
    path: '',
    component: OtherHealthServiceComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OtherHealthServices'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OtherHealthServiceDetailComponent,
    resolve: {
      otherHealthService: OtherHealthServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OtherHealthServices'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OtherHealthServiceUpdateComponent,
    resolve: {
      otherHealthService: OtherHealthServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OtherHealthServices'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OtherHealthServiceUpdateComponent,
    resolve: {
      otherHealthService: OtherHealthServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OtherHealthServices'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const otherHealthServicePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: OtherHealthServiceDeletePopupComponent,
    resolve: {
      otherHealthService: OtherHealthServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OtherHealthServices'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
