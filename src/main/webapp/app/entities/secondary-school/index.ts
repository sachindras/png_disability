export * from './secondary-school.service';
export * from './secondary-school-update.component';
export * from './secondary-school-delete-dialog.component';
export * from './secondary-school-detail.component';
export * from './secondary-school.component';
export * from './secondary-school.route';
