import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISecondarySchool } from 'app/shared/model/secondary-school.model';

type EntityResponseType = HttpResponse<ISecondarySchool>;
type EntityArrayResponseType = HttpResponse<ISecondarySchool[]>;

@Injectable({ providedIn: 'root' })
export class SecondarySchoolService {
  public resourceUrl = SERVER_API_URL + 'api/secondary-schools';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/secondary-schools';

  constructor(protected http: HttpClient) {}

  create(secondarySchool: ISecondarySchool): Observable<EntityResponseType> {
    return this.http.post<ISecondarySchool>(this.resourceUrl, secondarySchool, { observe: 'response' });
  }

  update(secondarySchool: ISecondarySchool): Observable<EntityResponseType> {
    return this.http.put<ISecondarySchool>(this.resourceUrl, secondarySchool, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISecondarySchool>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISecondarySchool[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISecondarySchool[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
