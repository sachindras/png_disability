import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISecondarySchool } from 'app/shared/model/secondary-school.model';

@Component({
  selector: 'jhi-secondary-school-detail',
  templateUrl: './secondary-school-detail.component.html'
})
export class SecondarySchoolDetailComponent implements OnInit {
  secondarySchool: ISecondarySchool;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ secondarySchool }) => {
      this.secondarySchool = secondarySchool;
    });
  }

  previousState() {
    window.history.back();
  }
}
