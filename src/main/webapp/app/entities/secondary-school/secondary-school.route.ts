import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SecondarySchool } from 'app/shared/model/secondary-school.model';
import { SecondarySchoolService } from './secondary-school.service';
import { SecondarySchoolComponent } from './secondary-school.component';
import { SecondarySchoolDetailComponent } from './secondary-school-detail.component';
import { SecondarySchoolUpdateComponent } from './secondary-school-update.component';
import { SecondarySchoolDeletePopupComponent } from './secondary-school-delete-dialog.component';
import { ISecondarySchool } from 'app/shared/model/secondary-school.model';

@Injectable({ providedIn: 'root' })
export class SecondarySchoolResolve implements Resolve<ISecondarySchool> {
  constructor(private service: SecondarySchoolService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISecondarySchool> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SecondarySchool>) => response.ok),
        map((secondarySchool: HttpResponse<SecondarySchool>) => secondarySchool.body)
      );
    }
    return of(new SecondarySchool());
  }
}

export const secondarySchoolRoute: Routes = [
  {
    path: '',
    component: SecondarySchoolComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SecondarySchools'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SecondarySchoolDetailComponent,
    resolve: {
      secondarySchool: SecondarySchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SecondarySchools'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SecondarySchoolUpdateComponent,
    resolve: {
      secondarySchool: SecondarySchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SecondarySchools'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SecondarySchoolUpdateComponent,
    resolve: {
      secondarySchool: SecondarySchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SecondarySchools'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const secondarySchoolPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SecondarySchoolDeletePopupComponent,
    resolve: {
      secondarySchool: SecondarySchoolResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SecondarySchools'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
