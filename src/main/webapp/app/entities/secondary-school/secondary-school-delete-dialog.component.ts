import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISecondarySchool } from 'app/shared/model/secondary-school.model';
import { SecondarySchoolService } from './secondary-school.service';

@Component({
  selector: 'jhi-secondary-school-delete-dialog',
  templateUrl: './secondary-school-delete-dialog.component.html'
})
export class SecondarySchoolDeleteDialogComponent {
  secondarySchool: ISecondarySchool;

  constructor(
    protected secondarySchoolService: SecondarySchoolService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.secondarySchoolService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'secondarySchoolListModification',
        content: 'Deleted an secondarySchool'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-secondary-school-delete-popup',
  template: ''
})
export class SecondarySchoolDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ secondarySchool }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SecondarySchoolDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.secondarySchool = secondarySchool;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/secondary-school', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/secondary-school', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
