import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  SecondarySchoolComponent,
  SecondarySchoolDetailComponent,
  SecondarySchoolUpdateComponent,
  SecondarySchoolDeletePopupComponent,
  SecondarySchoolDeleteDialogComponent,
  secondarySchoolRoute,
  secondarySchoolPopupRoute
} from './';

const ENTITY_STATES = [...secondarySchoolRoute, ...secondarySchoolPopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SecondarySchoolComponent,
    SecondarySchoolDetailComponent,
    SecondarySchoolUpdateComponent,
    SecondarySchoolDeleteDialogComponent,
    SecondarySchoolDeletePopupComponent
  ],
  entryComponents: [
    SecondarySchoolComponent,
    SecondarySchoolUpdateComponent,
    SecondarySchoolDeleteDialogComponent,
    SecondarySchoolDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionSecondarySchoolModule {}
