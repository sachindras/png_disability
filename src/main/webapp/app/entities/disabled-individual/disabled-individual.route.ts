import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DisabledIndividual } from 'app/shared/model/disabled-individual.model';
import { DisabledIndividualService } from './disabled-individual.service';
import { DisabledIndividualComponent } from './disabled-individual.component';
import { DisabledIndividualDetailComponent } from './disabled-individual-detail.component';
import { DisabledIndividualUpdateComponent } from './disabled-individual-update.component';
import { DisabledIndividualDeletePopupComponent } from './disabled-individual-delete-dialog.component';
import { IDisabledIndividual } from 'app/shared/model/disabled-individual.model';

@Injectable({ providedIn: 'root' })
export class DisabledIndividualResolve implements Resolve<IDisabledIndividual> {
  constructor(private service: DisabledIndividualService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDisabledIndividual> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<DisabledIndividual>) => response.ok),
        map((disabledIndividual: HttpResponse<DisabledIndividual>) => disabledIndividual.body)
      );
    }
    return of(new DisabledIndividual());
  }
}

export const disabledIndividualRoute: Routes = [
  {
    path: '',
    component: DisabledIndividualComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DisabledIndividuals'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DisabledIndividualDetailComponent,
    resolve: {
      disabledIndividual: DisabledIndividualResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DisabledIndividuals'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DisabledIndividualUpdateComponent,
    resolve: {
      disabledIndividual: DisabledIndividualResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DisabledIndividuals'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DisabledIndividualUpdateComponent,
    resolve: {
      disabledIndividual: DisabledIndividualResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DisabledIndividuals'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const disabledIndividualPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DisabledIndividualDeletePopupComponent,
    resolve: {
      disabledIndividual: DisabledIndividualResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DisabledIndividuals'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
