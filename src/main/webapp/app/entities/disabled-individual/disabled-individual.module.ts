import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PngDisabilityReligionSharedModule } from 'app/shared';
import {
  DisabledIndividualComponent,
  DisabledIndividualDetailComponent,
  DisabledIndividualUpdateComponent,
  DisabledIndividualDeletePopupComponent,
  DisabledIndividualDeleteDialogComponent,
  disabledIndividualRoute,
  disabledIndividualPopupRoute
} from './';

const ENTITY_STATES = [...disabledIndividualRoute, ...disabledIndividualPopupRoute];

@NgModule({
  imports: [PngDisabilityReligionSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    DisabledIndividualComponent,
    DisabledIndividualDetailComponent,
    DisabledIndividualUpdateComponent,
    DisabledIndividualDeleteDialogComponent,
    DisabledIndividualDeletePopupComponent
  ],
  entryComponents: [
    DisabledIndividualComponent,
    DisabledIndividualUpdateComponent,
    DisabledIndividualDeleteDialogComponent,
    DisabledIndividualDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionDisabledIndividualModule {}
