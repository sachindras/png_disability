import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IDisabledIndividual, DisabledIndividual } from 'app/shared/model/disabled-individual.model';
import { DisabledIndividualService } from './disabled-individual.service';
import { IContactDetails } from 'app/shared/model/contact-details.model';
import { ContactDetailsService } from 'app/entities/contact-details';
import { ILocation } from 'app/shared/model/location.model';
import { LocationService } from 'app/entities/location';

@Component({
  selector: 'jhi-disabled-individual-update',
  templateUrl: './disabled-individual-update.component.html'
})
export class DisabledIndividualUpdateComponent implements OnInit {
  isSaving: boolean;

  individualcontactdetails: IContactDetails[];

  carercontactdetails: IContactDetails[];

  locations: ILocation[];

  editForm = this.fb.group({
    id: [],
    civilRegistrationNo: [],
    name: [],
    gender: [],
    religion: [],
    occupation: [],
    employer: [],
    dateOfBirth: [],
    estimatedAge: [],
    martialStatus: [],
    noOfChildrenAlive: [],
    noOfChildrenDeceased: [],
    noOfChildrenCongenital: [],
    educationFormal: [],
    educationTrade: [],
    carerRelationship: [],
    diffcultySeeing: [],
    diffcultyHearing: [],
    diffcultyWalking: [],
    diffcultyMemory: [],
    diffcultySelfCare: [],
    diffcultyCommunicating: [],
    causeOfDisability: [],
    healthServiceAvailibility: [],
    healthServiceAccessibility: [],
    educationServiceAvailibility: [],
    educationServiceAccessibility: [],
    lawServiceAvailibility: [],
    lawServiceAccessibility: [],
    welfareServiceAvailibility: [],
    welfareServiceAccessibility: [],
    sportsServiceAvailibility: [],
    sportsServiceAccessibility: [],
    roadServiceAvailibility: [],
    roadServiceAccessibility: [],
    marketServiceAvailibility: [],
    marketServiceAccessibility: [],
    bankingServiceAvailibility: [],
    bankingServiceAccessibility: [],
    teleCommunicationServiceAvailibility: [],
    teleCommunicationServiceAccessibility: [],
    churchServiceAvailibility: [],
    churchServiceAccessibility: [],
    memberDisabledPersonsOrganisation: [],
    awarenessAdvocacyProgrammeDetails: [],
    livelihood: [],
    livelihoodDetails: [],
    talents: [],
    individualContactDetails: [],
    carerContactDetails: [],
    individualLocation: [],
    carerLocation: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected disabledIndividualService: DisabledIndividualService,
    protected contactDetailsService: ContactDetailsService,
    protected locationService: LocationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ disabledIndividual }) => {
      this.updateForm(disabledIndividual);
    });
    this.contactDetailsService
      .query({ filter: 'disabledindividual-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IContactDetails[]>) => mayBeOk.ok),
        map((response: HttpResponse<IContactDetails[]>) => response.body)
      )
      .subscribe(
        (res: IContactDetails[]) => {
          if (!this.editForm.get('individualContactDetails').value || !this.editForm.get('individualContactDetails').value.id) {
            this.individualcontactdetails = res;
          } else {
            this.contactDetailsService
              .find(this.editForm.get('individualContactDetails').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IContactDetails>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IContactDetails>) => subResponse.body)
              )
              .subscribe(
                (subRes: IContactDetails) => (this.individualcontactdetails = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.contactDetailsService
      .query({ filter: 'disabledindividual-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IContactDetails[]>) => mayBeOk.ok),
        map((response: HttpResponse<IContactDetails[]>) => response.body)
      )
      .subscribe(
        (res: IContactDetails[]) => {
          if (!this.editForm.get('carerContactDetails').value || !this.editForm.get('carerContactDetails').value.id) {
            this.carercontactdetails = res;
          } else {
            this.contactDetailsService
              .find(this.editForm.get('carerContactDetails').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IContactDetails>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IContactDetails>) => subResponse.body)
              )
              .subscribe(
                (subRes: IContactDetails) => (this.carercontactdetails = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.locationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ILocation[]>) => mayBeOk.ok),
        map((response: HttpResponse<ILocation[]>) => response.body)
      )
      .subscribe((res: ILocation[]) => (this.locations = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(disabledIndividual: IDisabledIndividual) {
    this.editForm.patchValue({
      id: disabledIndividual.id,
      civilRegistrationNo: disabledIndividual.civilRegistrationNo,
      name: disabledIndividual.name,
      gender: disabledIndividual.gender,
      religion: disabledIndividual.religion,
      occupation: disabledIndividual.occupation,
      employer: disabledIndividual.employer,
      dateOfBirth: disabledIndividual.dateOfBirth != null ? disabledIndividual.dateOfBirth.format(DATE_TIME_FORMAT) : null,
      estimatedAge: disabledIndividual.estimatedAge,
      martialStatus: disabledIndividual.martialStatus,
      noOfChildrenAlive: disabledIndividual.noOfChildrenAlive,
      noOfChildrenDeceased: disabledIndividual.noOfChildrenDeceased,
      noOfChildrenCongenital: disabledIndividual.noOfChildrenCongenital,
      educationFormal: disabledIndividual.educationFormal,
      educationTrade: disabledIndividual.educationTrade,
      carerRelationship: disabledIndividual.carerRelationship,
      diffcultySeeing: disabledIndividual.diffcultySeeing,
      diffcultyHearing: disabledIndividual.diffcultyHearing,
      diffcultyWalking: disabledIndividual.diffcultyWalking,
      diffcultyMemory: disabledIndividual.diffcultyMemory,
      diffcultySelfCare: disabledIndividual.diffcultySelfCare,
      diffcultyCommunicating: disabledIndividual.diffcultyCommunicating,
      causeOfDisability: disabledIndividual.causeOfDisability,
      healthServiceAvailibility: disabledIndividual.healthServiceAvailibility,
      healthServiceAccessibility: disabledIndividual.healthServiceAccessibility,
      educationServiceAvailibility: disabledIndividual.educationServiceAvailibility,
      educationServiceAccessibility: disabledIndividual.educationServiceAccessibility,
      lawServiceAvailibility: disabledIndividual.lawServiceAvailibility,
      lawServiceAccessibility: disabledIndividual.lawServiceAccessibility,
      welfareServiceAvailibility: disabledIndividual.welfareServiceAvailibility,
      welfareServiceAccessibility: disabledIndividual.welfareServiceAccessibility,
      sportsServiceAvailibility: disabledIndividual.sportsServiceAvailibility,
      sportsServiceAccessibility: disabledIndividual.sportsServiceAccessibility,
      roadServiceAvailibility: disabledIndividual.roadServiceAvailibility,
      roadServiceAccessibility: disabledIndividual.roadServiceAccessibility,
      marketServiceAvailibility: disabledIndividual.marketServiceAvailibility,
      marketServiceAccessibility: disabledIndividual.marketServiceAccessibility,
      bankingServiceAvailibility: disabledIndividual.bankingServiceAvailibility,
      bankingServiceAccessibility: disabledIndividual.bankingServiceAccessibility,
      teleCommunicationServiceAvailibility: disabledIndividual.teleCommunicationServiceAvailibility,
      teleCommunicationServiceAccessibility: disabledIndividual.teleCommunicationServiceAccessibility,
      churchServiceAvailibility: disabledIndividual.churchServiceAvailibility,
      churchServiceAccessibility: disabledIndividual.churchServiceAccessibility,
      memberDisabledPersonsOrganisation: disabledIndividual.memberDisabledPersonsOrganisation,
      awarenessAdvocacyProgrammeDetails: disabledIndividual.awarenessAdvocacyProgrammeDetails,
      livelihood: disabledIndividual.livelihood,
      livelihoodDetails: disabledIndividual.livelihoodDetails,
      talents: disabledIndividual.talents,
      individualContactDetails: disabledIndividual.individualContactDetails,
      carerContactDetails: disabledIndividual.carerContactDetails,
      individualLocation: disabledIndividual.individualLocation,
      carerLocation: disabledIndividual.carerLocation
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const disabledIndividual = this.createFromForm();
    if (disabledIndividual.id !== undefined) {
      this.subscribeToSaveResponse(this.disabledIndividualService.update(disabledIndividual));
    } else {
      this.subscribeToSaveResponse(this.disabledIndividualService.create(disabledIndividual));
    }
  }

  private createFromForm(): IDisabledIndividual {
    return {
      ...new DisabledIndividual(),
      id: this.editForm.get(['id']).value,
      civilRegistrationNo: this.editForm.get(['civilRegistrationNo']).value,
      name: this.editForm.get(['name']).value,
      gender: this.editForm.get(['gender']).value,
      religion: this.editForm.get(['religion']).value,
      occupation: this.editForm.get(['occupation']).value,
      employer: this.editForm.get(['employer']).value,
      dateOfBirth:
        this.editForm.get(['dateOfBirth']).value != null ? moment(this.editForm.get(['dateOfBirth']).value, DATE_TIME_FORMAT) : undefined,
      estimatedAge: this.editForm.get(['estimatedAge']).value,
      martialStatus: this.editForm.get(['martialStatus']).value,
      noOfChildrenAlive: this.editForm.get(['noOfChildrenAlive']).value,
      noOfChildrenDeceased: this.editForm.get(['noOfChildrenDeceased']).value,
      noOfChildrenCongenital: this.editForm.get(['noOfChildrenCongenital']).value,
      educationFormal: this.editForm.get(['educationFormal']).value,
      educationTrade: this.editForm.get(['educationTrade']).value,
      carerRelationship: this.editForm.get(['carerRelationship']).value,
      diffcultySeeing: this.editForm.get(['diffcultySeeing']).value,
      diffcultyHearing: this.editForm.get(['diffcultyHearing']).value,
      diffcultyWalking: this.editForm.get(['diffcultyWalking']).value,
      diffcultyMemory: this.editForm.get(['diffcultyMemory']).value,
      diffcultySelfCare: this.editForm.get(['diffcultySelfCare']).value,
      diffcultyCommunicating: this.editForm.get(['diffcultyCommunicating']).value,
      causeOfDisability: this.editForm.get(['causeOfDisability']).value,
      healthServiceAvailibility: this.editForm.get(['healthServiceAvailibility']).value,
      healthServiceAccessibility: this.editForm.get(['healthServiceAccessibility']).value,
      educationServiceAvailibility: this.editForm.get(['educationServiceAvailibility']).value,
      educationServiceAccessibility: this.editForm.get(['educationServiceAccessibility']).value,
      lawServiceAvailibility: this.editForm.get(['lawServiceAvailibility']).value,
      lawServiceAccessibility: this.editForm.get(['lawServiceAccessibility']).value,
      welfareServiceAvailibility: this.editForm.get(['welfareServiceAvailibility']).value,
      welfareServiceAccessibility: this.editForm.get(['welfareServiceAccessibility']).value,
      sportsServiceAvailibility: this.editForm.get(['sportsServiceAvailibility']).value,
      sportsServiceAccessibility: this.editForm.get(['sportsServiceAccessibility']).value,
      roadServiceAvailibility: this.editForm.get(['roadServiceAvailibility']).value,
      roadServiceAccessibility: this.editForm.get(['roadServiceAccessibility']).value,
      marketServiceAvailibility: this.editForm.get(['marketServiceAvailibility']).value,
      marketServiceAccessibility: this.editForm.get(['marketServiceAccessibility']).value,
      bankingServiceAvailibility: this.editForm.get(['bankingServiceAvailibility']).value,
      bankingServiceAccessibility: this.editForm.get(['bankingServiceAccessibility']).value,
      teleCommunicationServiceAvailibility: this.editForm.get(['teleCommunicationServiceAvailibility']).value,
      teleCommunicationServiceAccessibility: this.editForm.get(['teleCommunicationServiceAccessibility']).value,
      churchServiceAvailibility: this.editForm.get(['churchServiceAvailibility']).value,
      churchServiceAccessibility: this.editForm.get(['churchServiceAccessibility']).value,
      memberDisabledPersonsOrganisation: this.editForm.get(['memberDisabledPersonsOrganisation']).value,
      awarenessAdvocacyProgrammeDetails: this.editForm.get(['awarenessAdvocacyProgrammeDetails']).value,
      livelihood: this.editForm.get(['livelihood']).value,
      livelihoodDetails: this.editForm.get(['livelihoodDetails']).value,
      talents: this.editForm.get(['talents']).value,
      individualContactDetails: this.editForm.get(['individualContactDetails']).value,
      carerContactDetails: this.editForm.get(['carerContactDetails']).value,
      individualLocation: this.editForm.get(['individualLocation']).value,
      carerLocation: this.editForm.get(['carerLocation']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDisabledIndividual>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackContactDetailsById(index: number, item: IContactDetails) {
    return item.id;
  }

  trackLocationById(index: number, item: ILocation) {
    return item.id;
  }
}
