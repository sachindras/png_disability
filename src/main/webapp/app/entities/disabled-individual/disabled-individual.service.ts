import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDisabledIndividual } from 'app/shared/model/disabled-individual.model';

type EntityResponseType = HttpResponse<IDisabledIndividual>;
type EntityArrayResponseType = HttpResponse<IDisabledIndividual[]>;

@Injectable({ providedIn: 'root' })
export class DisabledIndividualService {
  public resourceUrl = SERVER_API_URL + 'api/disabled-individuals';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/disabled-individuals';

  constructor(protected http: HttpClient) {}

  create(disabledIndividual: IDisabledIndividual): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(disabledIndividual);
    return this.http
      .post<IDisabledIndividual>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(disabledIndividual: IDisabledIndividual): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(disabledIndividual);
    return this.http
      .put<IDisabledIndividual>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDisabledIndividual>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDisabledIndividual[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDisabledIndividual[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(disabledIndividual: IDisabledIndividual): IDisabledIndividual {
    const copy: IDisabledIndividual = Object.assign({}, disabledIndividual, {
      dateOfBirth:
        disabledIndividual.dateOfBirth != null && disabledIndividual.dateOfBirth.isValid() ? disabledIndividual.dateOfBirth.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateOfBirth = res.body.dateOfBirth != null ? moment(res.body.dateOfBirth) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((disabledIndividual: IDisabledIndividual) => {
        disabledIndividual.dateOfBirth = disabledIndividual.dateOfBirth != null ? moment(disabledIndividual.dateOfBirth) : null;
      });
    }
    return res;
  }
}
