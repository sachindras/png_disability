import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDisabledIndividual } from 'app/shared/model/disabled-individual.model';

@Component({
  selector: 'jhi-disabled-individual-detail',
  templateUrl: './disabled-individual-detail.component.html'
})
export class DisabledIndividualDetailComponent implements OnInit {
  disabledIndividual: IDisabledIndividual;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ disabledIndividual }) => {
      this.disabledIndividual = disabledIndividual;
    });
  }

  previousState() {
    window.history.back();
  }
}
