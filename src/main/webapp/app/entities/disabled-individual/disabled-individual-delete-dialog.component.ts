import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDisabledIndividual } from 'app/shared/model/disabled-individual.model';
import { DisabledIndividualService } from './disabled-individual.service';

@Component({
  selector: 'jhi-disabled-individual-delete-dialog',
  templateUrl: './disabled-individual-delete-dialog.component.html'
})
export class DisabledIndividualDeleteDialogComponent {
  disabledIndividual: IDisabledIndividual;

  constructor(
    protected disabledIndividualService: DisabledIndividualService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.disabledIndividualService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'disabledIndividualListModification',
        content: 'Deleted an disabledIndividual'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-disabled-individual-delete-popup',
  template: ''
})
export class DisabledIndividualDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ disabledIndividual }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DisabledIndividualDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.disabledIndividual = disabledIndividual;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/disabled-individual', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/disabled-individual', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
