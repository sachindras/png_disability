export * from './disabled-individual.service';
export * from './disabled-individual-update.component';
export * from './disabled-individual-delete-dialog.component';
export * from './disabled-individual-detail.component';
export * from './disabled-individual.component';
export * from './disabled-individual.route';
