import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Clinic } from 'app/shared/model/clinic.model';
import { ClinicService } from './clinic.service';
import { ClinicComponent } from './clinic.component';
import { ClinicDetailComponent } from './clinic-detail.component';
import { ClinicUpdateComponent } from './clinic-update.component';
import { ClinicDeletePopupComponent } from './clinic-delete-dialog.component';
import { IClinic } from 'app/shared/model/clinic.model';

@Injectable({ providedIn: 'root' })
export class ClinicResolve implements Resolve<IClinic> {
  constructor(private service: ClinicService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IClinic> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Clinic>) => response.ok),
        map((clinic: HttpResponse<Clinic>) => clinic.body)
      );
    }
    return of(new Clinic());
  }
}

export const clinicRoute: Routes = [
  {
    path: '',
    component: ClinicComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Clinics'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ClinicDetailComponent,
    resolve: {
      clinic: ClinicResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Clinics'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ClinicUpdateComponent,
    resolve: {
      clinic: ClinicResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Clinics'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ClinicUpdateComponent,
    resolve: {
      clinic: ClinicResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Clinics'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const clinicPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ClinicDeletePopupComponent,
    resolve: {
      clinic: ClinicResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Clinics'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
