import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PngDisabilityReligionSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [PngDisabilityReligionSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [PngDisabilityReligionSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PngDisabilityReligionSharedModule {
  static forRoot() {
    return {
      ngModule: PngDisabilityReligionSharedModule
    };
  }
}
