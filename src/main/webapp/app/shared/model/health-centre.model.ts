export interface IHealthCentre {
  id?: number;
  centreName?: string;
  province?: string;
  district?: string;
  ward?: string;
  active?: boolean;
}

export class HealthCentre implements IHealthCentre {
  constructor(
    public id?: number,
    public centreName?: string,
    public province?: string,
    public district?: string,
    public ward?: string,
    public active?: boolean
  ) {
    this.active = this.active || false;
  }
}
