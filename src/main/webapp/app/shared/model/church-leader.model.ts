import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';

export interface IChurchLeader {
  id?: number;
  name?: string;
  designation?: string;
  phoneNumber?: string;
  email?: string;
  religiousInstitute?: IReligiousInstitute;
}

export class ChurchLeader implements IChurchLeader {
  constructor(
    public id?: number,
    public name?: string,
    public designation?: string,
    public phoneNumber?: string,
    public email?: string,
    public religiousInstitute?: IReligiousInstitute
  ) {}
}
