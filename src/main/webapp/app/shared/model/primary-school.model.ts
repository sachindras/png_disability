import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';

export interface IPrimarySchool {
  id?: number;
  name?: string;
  province?: string;
  district?: string;
  ward?: string;
  active?: boolean;
  religiousInstitute?: IReligiousInstitute;
}

export class PrimarySchool implements IPrimarySchool {
  constructor(
    public id?: number,
    public name?: string,
    public province?: string,
    public district?: string,
    public ward?: string,
    public active?: boolean,
    public religiousInstitute?: IReligiousInstitute
  ) {
    this.active = this.active || false;
  }
}
