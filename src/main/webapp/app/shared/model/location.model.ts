export interface ILocation {
  id?: number;
  villageName?: string;
  ward?: string;
  localLevelGovernment?: string;
  district?: string;
  province?: string;
}

export class Location implements ILocation {
  constructor(
    public id?: number,
    public villageName?: string,
    public ward?: string,
    public localLevelGovernment?: string,
    public district?: string,
    public province?: string
  ) {}
}
