import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';

export interface IClinic {
  id?: number;
  clinicName?: string;
  province?: string;
  district?: string;
  ward?: string;
  active?: boolean;
  religiousInstitute?: IReligiousInstitute;
}

export class Clinic implements IClinic {
  constructor(
    public id?: number,
    public clinicName?: string,
    public province?: string,
    public district?: string,
    public ward?: string,
    public active?: boolean,
    public religiousInstitute?: IReligiousInstitute
  ) {
    this.active = this.active || false;
  }
}
