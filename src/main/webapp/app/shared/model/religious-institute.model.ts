import { IChurchLeader } from 'app/shared/model/church-leader.model';
import { IHospital } from 'app/shared/model/hospital.model';
import { IClinic } from 'app/shared/model/clinic.model';
import { IOtherHealthService } from 'app/shared/model/other-health-service.model';
import { IUniversityCollege } from 'app/shared/model/university-college.model';
import { ITechnicalSchool } from 'app/shared/model/technical-school.model';
import { ISecondarySchool } from 'app/shared/model/secondary-school.model';
import { IPrimarySchool } from 'app/shared/model/primary-school.model';
import { IOtherEducationService } from 'app/shared/model/other-education-service.model';

export interface IReligiousInstitute {
  id?: number;
  churchName?: string;
  churchAddress?: string;
  denomination?: string;
  headOfficeLocation?: string;
  numberChurchMembership?: number;
  churchLeaders?: IChurchLeader[];
  churchHospitals?: IHospital[];
  churchClinics?: IClinic[];
  churchOtherHealthServices?: IOtherHealthService[];
  churchUniversityColleges?: IUniversityCollege[];
  churchTechnicalSchools?: ITechnicalSchool[];
  churchSecondarySchools?: ISecondarySchool[];
  churchPrimarySchools?: IPrimarySchool[];
  churchOtherEducationServices?: IOtherEducationService[];
}

export class ReligiousInstitute implements IReligiousInstitute {
  constructor(
    public id?: number,
    public churchName?: string,
    public churchAddress?: string,
    public denomination?: string,
    public headOfficeLocation?: string,
    public numberChurchMembership?: number,
    public churchLeaders?: IChurchLeader[],
    public churchHospitals?: IHospital[],
    public churchClinics?: IClinic[],
    public churchOtherHealthServices?: IOtherHealthService[],
    public churchUniversityColleges?: IUniversityCollege[],
    public churchTechnicalSchools?: ITechnicalSchool[],
    public churchSecondarySchools?: ISecondarySchool[],
    public churchPrimarySchools?: IPrimarySchool[],
    public churchOtherEducationServices?: IOtherEducationService[]
  ) {}
}
