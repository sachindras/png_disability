import { IReligiousInstitute } from 'app/shared/model/religious-institute.model';

export interface IHospital {
  id?: number;
  hospitalName?: string;
  province?: string;
  district?: string;
  ward?: string;
  active?: boolean;
  religiousInstitute?: IReligiousInstitute;
}

export class Hospital implements IHospital {
  constructor(
    public id?: number,
    public hospitalName?: string,
    public province?: string,
    public district?: string,
    public ward?: string,
    public active?: boolean,
    public religiousInstitute?: IReligiousInstitute
  ) {
    this.active = this.active || false;
  }
}
