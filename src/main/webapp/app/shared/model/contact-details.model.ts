export interface IContactDetails {
  id?: number;
  postalAddress?: string;
  phoneNumber?: string;
  email?: string;
}

export class ContactDetails implements IContactDetails {
  constructor(public id?: number, public postalAddress?: string, public phoneNumber?: string, public email?: string) {}
}
