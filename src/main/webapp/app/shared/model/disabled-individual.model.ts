import { Moment } from 'moment';
import { IContactDetails } from 'app/shared/model/contact-details.model';
import { ILocation } from 'app/shared/model/location.model';

export const enum Gender {
  MALE = 'MALE',
  FEMALE = 'FEMALE'
}

export const enum EstimateAge {
  LESS_THAN_18 = 'LESS_THAN_18',
  BETWEEN_18_AND_29 = 'BETWEEN_18_AND_29',
  BETWEEN_30_AND_39 = 'BETWEEN_30_AND_39',
  BETWEEN_40_AND_49 = 'BETWEEN_40_AND_49',
  BETWEEN_50_AND_59 = 'BETWEEN_50_AND_59',
  BETWEEN_60_AND_64 = 'BETWEEN_60_AND_64',
  ABOVE_65 = 'ABOVE_65'
}

export const enum MartialStatus {
  MARRIED = 'MARRIED',
  WIDOW_WIDOWER = 'WIDOW_WIDOWER',
  DEFACTO = 'DEFACTO',
  DIVORCED = 'DIVORCED',
  SEPARATED = 'SEPARATED',
  SINGLE = 'SINGLE'
}

export const enum CarerRelationship {
  FAMILY_MEMBER = 'FAMILY_MEMBER',
  NON_FAMILY_MEMBER = 'NON_FAMILY_MEMBER',
  FRIEND = 'FRIEND'
}

export const enum Difficulty {
  NO = 'NO',
  SOME = 'SOME',
  LOT = 'LOT',
  ENTIRELY = 'ENTIRELY'
}

export const enum CauseOfDisability {
  DISEASE = 'DISEASE',
  ACCIDENT = 'ACCIDENT',
  SUBSTANCE_ABUSE = 'SUBSTANCE_ABUSE',
  PHYSICAL_ABUSE = 'PHYSICAL_ABUSE',
  OTHERS = 'OTHERS'
}

export const enum ServiceAvailibility {
  YES_AVAILABLE = 'YES_AVAILABLE',
  NOT_AVAILABLE = 'NOT_AVAILABLE'
}

export const enum ServiceAccessibility {
  YES_ACCESSIBLE = 'YES_ACCESSIBLE',
  NOT_ACCESSIBLE = 'NOT_ACCESSIBLE'
}

export const enum Livelihood {
  EMPLOYED_FORM = 'EMPLOYED_FORM',
  EMPLOYED_INFORMAL = 'EMPLOYED_INFORMAL',
  SELF_EMPLOYED = 'SELF_EMPLOYED',
  SUBSISTENCE_FARMING = 'SUBSISTENCE_FARMING',
  CHARITY = 'CHARITY',
  BEGGING = 'BEGGING',
  DEPEDNDENT = 'DEPEDNDENT'
}

export const enum Talents {
  LEADERSHIP = 'LEADERSHIP',
  SPORTS = 'SPORTS',
  MUSIC = 'MUSIC',
  SIGN_LANGUAGE = 'SIGN_LANGUAGE',
  HAND_CRAFTING = 'HAND_CRAFTING',
  SCREEN_PAINTING = 'SCREEN_PAINTING',
  TECHNICAL_SKILLS = 'TECHNICAL_SKILLS',
  MECHANICAL_SKILLS = 'MECHANICAL_SKILLS'
}

export interface IDisabledIndividual {
  id?: number;
  civilRegistrationNo?: string;
  name?: string;
  gender?: Gender;
  religion?: string;
  occupation?: string;
  employer?: string;
  dateOfBirth?: Moment;
  estimatedAge?: EstimateAge;
  martialStatus?: MartialStatus;
  noOfChildrenAlive?: number;
  noOfChildrenDeceased?: number;
  noOfChildrenCongenital?: number;
  educationFormal?: string;
  educationTrade?: string;
  carerRelationship?: CarerRelationship;
  diffcultySeeing?: Difficulty;
  diffcultyHearing?: Difficulty;
  diffcultyWalking?: Difficulty;
  diffcultyMemory?: Difficulty;
  diffcultySelfCare?: Difficulty;
  diffcultyCommunicating?: Difficulty;
  causeOfDisability?: CauseOfDisability;
  healthServiceAvailibility?: ServiceAvailibility;
  healthServiceAccessibility?: ServiceAccessibility;
  educationServiceAvailibility?: ServiceAvailibility;
  educationServiceAccessibility?: ServiceAccessibility;
  lawServiceAvailibility?: ServiceAvailibility;
  lawServiceAccessibility?: ServiceAccessibility;
  welfareServiceAvailibility?: ServiceAvailibility;
  welfareServiceAccessibility?: ServiceAccessibility;
  sportsServiceAvailibility?: ServiceAvailibility;
  sportsServiceAccessibility?: ServiceAccessibility;
  roadServiceAvailibility?: ServiceAvailibility;
  roadServiceAccessibility?: ServiceAccessibility;
  marketServiceAvailibility?: ServiceAvailibility;
  marketServiceAccessibility?: ServiceAccessibility;
  bankingServiceAvailibility?: ServiceAvailibility;
  bankingServiceAccessibility?: ServiceAccessibility;
  teleCommunicationServiceAvailibility?: ServiceAvailibility;
  teleCommunicationServiceAccessibility?: ServiceAccessibility;
  churchServiceAvailibility?: ServiceAvailibility;
  churchServiceAccessibility?: ServiceAccessibility;
  memberDisabledPersonsOrganisation?: boolean;
  awarenessAdvocacyProgrammeDetails?: string;
  livelihood?: Livelihood;
  livelihoodDetails?: string;
  talents?: Talents;
  individualContactDetails?: IContactDetails;
  carerContactDetails?: IContactDetails;
  individualLocation?: ILocation;
  carerLocation?: ILocation;
}

export class DisabledIndividual implements IDisabledIndividual {
  constructor(
    public id?: number,
    public civilRegistrationNo?: string,
    public name?: string,
    public gender?: Gender,
    public religion?: string,
    public occupation?: string,
    public employer?: string,
    public dateOfBirth?: Moment,
    public estimatedAge?: EstimateAge,
    public martialStatus?: MartialStatus,
    public noOfChildrenAlive?: number,
    public noOfChildrenDeceased?: number,
    public noOfChildrenCongenital?: number,
    public educationFormal?: string,
    public educationTrade?: string,
    public carerRelationship?: CarerRelationship,
    public diffcultySeeing?: Difficulty,
    public diffcultyHearing?: Difficulty,
    public diffcultyWalking?: Difficulty,
    public diffcultyMemory?: Difficulty,
    public diffcultySelfCare?: Difficulty,
    public diffcultyCommunicating?: Difficulty,
    public causeOfDisability?: CauseOfDisability,
    public healthServiceAvailibility?: ServiceAvailibility,
    public healthServiceAccessibility?: ServiceAccessibility,
    public educationServiceAvailibility?: ServiceAvailibility,
    public educationServiceAccessibility?: ServiceAccessibility,
    public lawServiceAvailibility?: ServiceAvailibility,
    public lawServiceAccessibility?: ServiceAccessibility,
    public welfareServiceAvailibility?: ServiceAvailibility,
    public welfareServiceAccessibility?: ServiceAccessibility,
    public sportsServiceAvailibility?: ServiceAvailibility,
    public sportsServiceAccessibility?: ServiceAccessibility,
    public roadServiceAvailibility?: ServiceAvailibility,
    public roadServiceAccessibility?: ServiceAccessibility,
    public marketServiceAvailibility?: ServiceAvailibility,
    public marketServiceAccessibility?: ServiceAccessibility,
    public bankingServiceAvailibility?: ServiceAvailibility,
    public bankingServiceAccessibility?: ServiceAccessibility,
    public teleCommunicationServiceAvailibility?: ServiceAvailibility,
    public teleCommunicationServiceAccessibility?: ServiceAccessibility,
    public churchServiceAvailibility?: ServiceAvailibility,
    public churchServiceAccessibility?: ServiceAccessibility,
    public memberDisabledPersonsOrganisation?: boolean,
    public awarenessAdvocacyProgrammeDetails?: string,
    public livelihood?: Livelihood,
    public livelihoodDetails?: string,
    public talents?: Talents,
    public individualContactDetails?: IContactDetails,
    public carerContactDetails?: IContactDetails,
    public individualLocation?: ILocation,
    public carerLocation?: ILocation
  ) {
    this.memberDisabledPersonsOrganisation = this.memberDisabledPersonsOrganisation || false;
  }
}
