import { NgModule } from '@angular/core';

import { PngDisabilityReligionSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [PngDisabilityReligionSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [PngDisabilityReligionSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class PngDisabilityReligionSharedCommonModule {}
